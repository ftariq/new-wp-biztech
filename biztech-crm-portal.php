<?php

/**
 * Plugin Name: Customer Portal
 * Description: This plug-in use for dynamic portal. It manages different modules and set dynamic portal layout. 
 * Author: biztechc
 * Author URI: https://www.biztechcs.com
 * Version: 1.0.1 
 */
global $sugar_crm_version;

$sugar_crm_version = get_option('biztech_scp_sugar_crm_version');
if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
    include( plugin_dir_path(__FILE__) . 'class/scp-class-6.php');
    include( plugin_dir_path(__FILE__) . 'actions/bcp-action-v6.php');
} else if ($sugar_crm_version == 7) {
    include( plugin_dir_path(__FILE__) . 'class/scp-class-7.php');
    include( plugin_dir_path(__FILE__) . 'actions/bcp-action-v7.php');
} else {
    include( plugin_dir_path(__FILE__) . 'class/scp-class-6.php');
    include( plugin_dir_path(__FILE__) . 'actions/bcp-action-v6.php');
}

//Template part 30-jul-2016
//
//

include_once( plugin_dir_path(__FILE__) . 'templates/' . 'scp-class-page-template.php' ); //Include template file for manage-page
add_action('init', array('Scp_Page_Template_Plugin', 'get_instance'));

//For include all settings of admin side
include( plugin_dir_path(__FILE__) . 'actions/bcp_common-action.php'); // include common actions for 6/7 versions
include( plugin_dir_path(__FILE__) . 'admin/settings.php'); //For include all settings of admin side
include( plugin_dir_path(__FILE__) . 'shortcodes/bcp-shortcodes.php'); //For include all shortcodes
// Define a constant path to our single template folder
define('IMAGES_URL', plugin_dir_url(__FILE__) . 'assets/images/');
define('JS_PATH', plugin_dir_path(__FILE__) . 'assets/js/');
define('TEMPLATE_PATH', plugin_dir_path(__FILE__) . 'templates/');
define('ACTIONS_PATH', plugin_dir_path(__FILE__) . 'actions/');


//include( plugin_dir_path(__FILE__) . '/templates/'); 
//on activate plugin
register_activation_hook(__FILE__, "bcp_create_page");

function bcp_create_page() {
    global $wpdb, $user_ID;

    $page_array = array('bcp-sign-up' => 'Portal Sign Up', 'bcp-login' => 'Portal Login', 'bcp-profile' => 'Portal Profile', 'bcp-forgot-password' => 'Portal Forgot Password', 'bcp-manage-page' => 'Portal Manage Page', 'bcp-dashboard-gui' => 'Portal Dashboard GUI');

    foreach ($page_array as $sc => $page_name) {
//echo $file."--".$page_name;
        $page = get_page_by_title($page_name);
        if (empty($page)) {
            $new_post = array(
                'post_title' => $page_name,
                'post_content' => '[' . $sc . ']',
                'post_status' => 'publish',
                'post_date' => date('Y-m-d H:i:s'),
                'post_type' => 'page',
                'post_author' => $user_ID
            );
            $post_id = wp_insert_post($new_post);
            update_post_meta($post_id, '_wp_page_template', 'page-crm-standalone.php'); //For add stand alone template
        }
    }
//Added by BC on 17-jun-2016 Create table for storing module layout
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'module_layout';
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `version` tinyint(4) NOT NULL,
                    `module_name` varchar(50) NOT NULL,
                    `layout_view` varchar(50) NOT NULL,
                    `json_data` longtext NOT NULL,
                    `date` varchar(255) DEFAULT NULL,
                    `contact_group` VARCHAR(50) NOT NULL,
                    PRIMARY KEY ( `id` )) $charset_collate;";

    $wpdb->query($sql);
}

//Added by BC on 06-jun-2016 for CURL exist CHECKING
function _isCurl() {
//return function_exists('curl_version');
    $conn_err = 1;
    if (!(version_compare(phpversion(), '5.0.0', '>='))) {
        $conn_err = 'Plugin requires minimum PHP 5.0.0 to function properly. Please upgrade PHP or deactivate Plugin';
        return $conn_err;
    }
    if (!version_compare(phpversion(), '7.0.0', '<')) {
        $conn_err = "Plugin doesn't PHP 7.0.0 yet. Please downgrade PHP or deactivate Plugin";
        return $conn_err;
    }
    if (!function_exists('curl_version')) {
        $conn_err = 'Please enable PHP CURL extension to make this plugin work.';
        return $conn_err;
    }
    if (!function_exists('json_decode')) {
        $conn_err = 'Please enable PHP JSON extension to make this plugin work.';
        return $conn_err;
    }
    if ($conn_err == 1) {
        return 'yes';
    }
}

// general  function for connection with sugercrm
function bcp_suger_connection() {
    
    global $sugar_crm_version;
    global $objSCP;
    $check_var = _isCurl();
    if (($check_var == 'yes')) {//Added by BC on 03-jun-2016 for CURL CHECKING
        if (class_exists('SugarRestApiCall')) {
            $scp_sugar_rest_url = get_option('biztech_scp_rest_url');
            $scp_sugar_username = get_option('biztech_scp_username');
            $scp_sugar_password = get_option('biztech_scp_password');
            if (isset($scp_sugar_rest_url) && !empty($scp_sugar_rest_url) && isset($scp_sugar_username) && !empty($scp_sugar_username) && isset($scp_sugar_password) && !empty($scp_sugar_password)) {
                $objSCP = new SugarRestApiCall($scp_sugar_rest_url, $scp_sugar_username, $scp_sugar_password);
//if ($objSCP->login() != NULL && $objSCP->login()->name != "Invalid Login") {
                if ((($sugar_crm_version == 6 || $sugar_crm_version == 5) && $objSCP->session_id != '') || ($sugar_crm_version == 7 && $objSCP->access_token != '')) {//Updated on 15-jun-2016 for checking token
                    
                     
                    return $objSCP;
                } else {
                    $objSCP = 0;
                    $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
                    setcookie('bcp_connection_error', $conn_err, time() + 3600, '/');
                    return 0;
                }
            }
        } else {
            $objSCP = 0;
            $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
            setcookie('bcp_connection_error', $conn_err, time() + 3600, '/');
            return 0;
        }
    } else {//else CURL CHECKING
        $objSCP = 0;
//$conn_err = "CURL NOT WORKING";
        setcookie('bcp_connection_error', $check_var, time() + 3600, '/');
        return 0;
    }
    return false;
}

add_action('init', 'bcp_suger_connection');

//without login can't redirect to other page
function bcp_check_login() {
    if (!is_admin()) {
        if (isset($_SESSION['scp_user_id']) != true) {
            global $post;
            $curent_page = $post->ID;
            if (get_option('biztech_redirect_profile') != NULL) {
                $profile_page = get_option('biztech_redirect_profile');
            } else {
                $page = get_page_by_path('bcp-profile');
                $profile_page = $page->ID;
            }
            if (get_option('biztech_redirect_manange') != NULL) {
                $manage_page = get_option('biztech_redirect_manange');
            } else {
                $page = get_page_by_path('bcp-manage-page');
                $manage_page = $page->ID;
            }
            if (get_option('biztech_redirect_dashboard_gui') != NULL) {
                $dashboard_gui_page = get_option('biztech_redirect_dashboard_gui');
            } else {
                $dash_page = get_page_by_path('bcp-dashboard-gui');
                $dashboard_gui_page = $dash_page->ID;
            }
            if ($profile_page == $curent_page || $manage_page == $curent_page || $dashboard_gui_page == $curent_page) {
                $biztech_redirect_login = get_page_link(get_option('biztech_redirect_login'));
                if (isset($biztech_redirect_login) && !empty($biztech_redirect_login)) {

                    $redirect_url = $biztech_redirect_login;
                } else {
                    $redirect_url = home_url() . '/portal-login';
                }

                ob_clean();
                wp_redirect($redirect_url);
                die();
            }
        } else {
            global $post;
            $curent_page = $post->ID;
            if (get_option('biztech_redirect_login') != NULL) {
                $login = get_option('biztech_redirect_login');
            } else {
                $page = get_page_by_path('bcp-login');
                $login = $page->ID;
            }
            if (get_option('biztech_redirect_forgotpwd') != NULL) {
                $forgotpwd = get_option('biztech_redirect_forgotpwd');
            } else {
                $page = get_page_by_path('bcp-forgot-password');
                $forgotpwd = $page->ID;
            }
            if (get_option('biztech_redirect_signup') != NULL) {
                $signup = get_option('biztech_redirect_signup');
            } else {
                $dash_page = get_page_by_path('bcp_sign_up');
                $signup = $dash_page->ID;
            }
            if (!empty($_SESSION['scp_user_id'])) {
                if ($login == $curent_page || $forgotpwd == $curent_page || $signup == $curent_page) {
                    $biztech_redirect_dashboard_gui = get_page_link(get_option('biztech_redirect_dashboard_gui'));
                    if (isset($biztech_redirect_dashboard_gui) && !empty($biztech_redirect_dashboard_gui)) {

                        $redirect_url = $biztech_redirect_dashboard_gui;
                    } else {
                        $redirect_url = home_url() . '/portal-dashboard-gui';
                    }

                    ob_clean();
                    wp_redirect($redirect_url);
                    die();
                }
            }
        }
    }
}

add_action('wp', 'bcp_check_login', 099);

// general  function for connection with sugercrm
function bcp_check_sign_up() {

    if (isset($_REQUEST) && !empty($_REQUEST)) {

        if (isset($_REQUEST['sugar_side_portal_email']) && $_REQUEST['sugar_side_portal_email'] != '') {

            $username_c = stripslashes_deep($_REQUEST['user_login']);
            //$username_c = sanitize_user($username_c, true); //Added by BC on 19-nov-2015
            $password_c = stripslashes_deep($_REQUEST['user_pass']);
            $email = stripslashes_deep($_REQUEST['user_email']);
            $first_name = stripslashes_deep($_REQUEST['first_name']);
            $last_name = stripslashes_deep($_REQUEST['last_name']);
            $biztech_scp_key = stripslashes_deep($_REQUEST['biztech_scp_key']);
            $sugar_side_portal_email = (!empty($_REQUEST['sugar_side_portal_email'])) ? $_REQUEST['sugar_side_portal_email'] : 0; //Sugar side portal email

            if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) { //Added by BC on 08-aug-2015 
                $user = get_user_by('login', $username_c);
                $user_ID = $user->ID;
                $email_exists = email_exists($email);

                $error_msg = '';
                if ($email_exists) {
                    $error_msg = 'Email Address already exists.';
                    $add_data = array(
                        'ID' => $user_ID,
                        'first_name' => $first_name,
                        'last_name' => $last_name
                    );
                    $user_id = wp_update_user($add_data);
                } else {
                    $add_data = array(
                        'ID' => $user_ID,
                        //'user_login'        => $username_c,
                        //'user_pass' => $passw
                        'user_email' => $email,
                        'first_name' => $first_name,
                        'last_name' => $last_name
                    );
                    $user_id = wp_update_user($add_data);
                    if ($user_id != NULL) {
                        $error_msg = 'Your data successfully updated.';
                    } else {
                        $error_msg = 'User data not updated.';
                    }
                }
            } else {

                $username_exists = username_exists($username_c);
                $email_exists = email_exists($email);

                $error_msg = '';
                if ($username_exists && $email_exists) {
                    $error_msg = 'Username and Email Address already exists.';
                } else if ($username_exists) {
                    $error_msg = 'Username already exists.';
                } else if ($email_exists) {
                    $error_msg = 'Email Address already exists.';
                } else {
                    $add_data = array(
                        'user_login' => $username_c,
                        'user_pass' => $password_c,
                        'user_email' => $email,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'biztech_scp_key' => $biztech_scp_key
                    );
                    if (get_option('biztech_scp_key') == $biztech_scp_key) {
                        $user_id = wp_insert_user($add_data);
                    }
                    if ($user_id != NULL) {
                        if ($sugar_side_portal_email == 1) {
                            mail_user($username_c, $password_c, $first_name, $last_name, $email); //Added by BC on 23-sep-2015 send user mail
                        }
                        $error_msg = 'You are successfully sign up.';
                    } else {
                        $error_msg = 'You are not authenticate to register user.';
                    }
                }
            }
            echo $error_msg;
            exit();
        }
        if (isset($_REQUEST['pass_stored_scp_key']) && $_REQUEST['pass_stored_scp_key'] != '') {
            echo get_option('biztech_scp_key');
            exit();
        }
    }
}

add_action('init', 'bcp_check_sign_up');

register_uninstall_hook(__FILE__, 'sugar_crm_portal_uninstall'); // uninstall plug-in 

function sugar_crm_portal_uninstall() {
    global $wpdb;
    delete_option('biztech_scp_name');
    delete_option('biztech_scp_rest_url');
    delete_option('biztech_scp_username');
    delete_option('biztech_scp_password');
    delete_option('biztech_scp_case_per_page');
    delete_option('biztech_scp_tab_active');
    delete_option('biztech_scp_tab_inactive');
    delete_option('biztech_scp_upload_image');
    delete_option('biztech_scp_calendar_calls_color');
    delete_option('biztech_scp_calendar_meetings_color');
    delete_option('biztech_scp_calendar_tasks_color');
    delete_option('biztech_scp_portal_menu_title');
    delete_option('biztech_scp_custom_css');
    delete_option('biztech_scp_key');
    delete_option('biztech_scp_mail_subject');
    delete_option('biztech_scp_mail_body');
    delete_option('biztech_scp_sugar_crm_version');
    delete_option('biztech_redirect_login');
    delete_option('biztech_redirect_signup');
    delete_option('biztech_redirect_profile');
    delete_option('biztech_redirect_forgotpwd');
    delete_option('biztech_redirect_manange');
    delete_option('biztech_redirect_dashboard_gui'); //Added by BC on 30-may-2016
    delete_option('biztech_scp_sidebar_position'); //Added by BC on 04-jun-2016
    delete_option('biztech_scp_theme_color');
    delete_option('biztech_scp_recent_activity');
    delete_option('biztech_portal_template');
//Delete pages

    $wpdb->query("DELETE FROM {$wpdb->posts} WHERE post_name IN ( 'portal-sign-up', 'portal-login','portal-profile','portal-forgot-password','portal-manage-page','portal-dashboard-gui' ) AND post_type = 'page'");
}

//on deactivate plugin
register_deactivation_hook(__FILE__, 'sugar_crm_portal_deactivate');

function sugar_crm_portal_deactivate() {
//Delete pages
    global $wpdb;
//    $wpdb->query("DELETE FROM {$wpdb->posts} WHERE post_name IN ( 'portal-sign-up', 'portal-login', 'portal-forgot-password','portal-profile','portal-manage-page','portal-dashboard-gui') AND post_type = 'page'");
//Added by BC on 17-jun-2016 delete module layout table
    $dropTable = $wpdb->prepare("DROP TABLE IF EXISTS " . $wpdb->prefix . "module_layout", 1);
    $wpdb->query($dropTable);
}

add_action('wp_enqueue_scripts', 'sugar_crm_portal_style_and_script', 15);  // add custom style and script

function sugar_crm_portal_style_and_script() {

//Font css adde by BC on 31-may-2016
    wp_enqueue_style('font-awesome', plugins_url('assets/css/font-awesome.css', __FILE__));
    wp_enqueue_style('font-awesome.min', plugins_url('assets/css/font-awesome.min.css', __FILE__));
//bootstrp,calendar css
    wp_enqueue_style('fullcalendar', plugins_url('assets/css/fullcalendar.css', __FILE__));
    //wp_enqueue_style('jquery-ui-timepicker-addon', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css');
    wp_enqueue_style('jquery-ui-timepicker-addon', plugins_url('assets/css/jquery-ui.min.css', __FILE__));
    wp_enqueue_style('scp-style', plugins_url('assets/css/scp-style.css', __FILE__));
// js
    wp_enqueue_script('scp-js', plugins_url('assets/js/scp-js.js', __FILE__), array('jquery'));
//bootstrp,calendar js
    wp_enqueue_script('bootstrap.min', plugins_url('assets/js/bootstrap.min.js', __FILE__), array('jquery'));
    wp_enqueue_script('jquery.cookie.min', plugins_url('assets/js/jquery.cookie.min.js', __FILE__), array('jquery'));
    wp_enqueue_script('fullcalendar.min', plugins_url('assets/js/fullcalendar.min.js', __FILE__), array('jquery'));
    wp_enqueue_script('index', plugins_url('assets/js/index.js', __FILE__), array('jquery'));
    wp_enqueue_script('app', plugins_url('assets/js/app.js', __FILE__), array('jquery'));
    wp_enqueue_script('jquery.blockui.min', plugins_url('assets/js/jquery.blockui.min.js', __FILE__), array('jquery'));
// Date time picker
    wp_enqueue_script('jquery-ui-timepicker-addon', plugins_url('assets/js/jquery-ui-timepicker-addon.js', __FILE__));
//Validate jquery in all browsers
    wp_enqueue_script('jquery.validate', plugins_url('assets/js/jquery.validate/jquery.validate.js', __FILE__), array('jquery'));
//////////Added by BC on 30-jul-2016/////////////
    wp_enqueue_script('scp-manage-page-js', plugins_url('assets/js/scp-manage-page-js.js', __FILE__), array('jquery'));
}

//Making Changes on jquery version
function bcp_modify_jquery() {
    global $post;
    $profile_page = $manage_page = $dashboard_gui_page = $forgotpwd = $signup = $login = '';
    $curent_page = $post->ID;
    if (!empty($curent_page)) {

        if (get_option('biztech_redirect_profile') != NULL) {
            $profile_page = get_option('biztech_redirect_profile');
        }
        if (get_option('biztech_redirect_manange') != NULL) {
            $manage_page = get_option('biztech_redirect_manange');
        }
        if (get_option('biztech_redirect_dashboard_gui') != NULL) {
            $dashboard_gui_page = get_option('biztech_redirect_dashboard_gui');
        }
        if (get_option('biztech_redirect_forgotpwd') != NULL) {
            $forgotpwd = get_option('biztech_redirect_forgotpwd');
        }
        if (get_option('biztech_redirect_signup') != NULL) {
            $signup = get_option('biztech_redirect_signup');
        }
        if (get_option('biztech_redirect_login') != NULL) {
            $login = get_option('biztech_redirect_login');
        }
    }
    if ($curent_page == $profile_page || $curent_page == $manage_page || $curent_page == $dashboard_gui_page || $curent_page == $forgotpwd || $curent_page == $signup || $curent_page == $login) {
        //if (!is_admin()) {
        if (function_exists('wp_deregister_script')) {
            wp_deregister_script('jquery');
        }
//change by default version 1.11.1 to 1.10.2 for set calendar
        wp_register_script('jquery', plugins_url('assets/js/jquery-1.10.2.min.js', __FILE__), false, '1.10.2');
        wp_enqueue_script('jquery');
//jquery ui
        wp_enqueue_script('jquery-ui', plugins_url('assets/js/jquery-ui/jquery-ui-1.10.3.custom.min.js', __FILE__), false, '1.10.3');
        //}
    }
}

add_action('wp_enqueue_scripts', 'bcp_modify_jquery');

// you can hook your js variables at wp_head, that way it would be available to all js files
function bcp_js_variables() {
    $get_current_url = explode('?', $_SERVER['REQUEST_URI'], 2);
    $current_url = $get_current_url[0];
    $get_page_parameter = '';
    if (!isset($get_current_url[1])) {
        $get_current_url[1] = null;
    }
    if ($get_current_url[1]) {
        $get_page_parameter = explode('&', $get_current_url[1]);
        $get_page_parameter = $get_page_parameter[0];
    }
    if ($get_page_parameter != NULL) {
        $current_url .= "?" . $get_page_parameter;
    } else {
        $current_url .= "?scp-page=list-accounts";
    }
    ?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url("admin-ajax.php"); ?>';
        var image_url = '<?php echo IMAGES_URL; ?>';
        var pathId = '<?php
    if (isset($_REQUEST['scp-page'])) {
        echo $_REQUEST['scp-page'];
    }
    ?>';
        //General function
        function form_submit(doc_id) {
            jQuery('#scp_id').val(doc_id);
            if (confirm('Are you sure you want to delete the record?')) {
                document.getElementById("actionform").submit();
                return false;
            } else {
                return false;
            }
        }

    </script>
    <style type="text/css">
    <?php
    if (get_option('biztech_scp_theme_color') != NULL && get_option('biztech_scp_theme_color') != '') {
        ?> 
            .scp-Accounts,
            input[type="submit"].scp-Accounts,
            .scp-KBDocuments,
            .scp-Leads,
            input[type="submit"].scp-Leads,
            .scp-Cases,
            input[type="submit"].scp-Cases,
            .scp-Cases-update,
            .scp-Opportunities,
            input[type="submit"].scp-Opportunities,
            .scp-Documents,
            input[type="submit"].scp-Documents,
            .scp-Calls,
            input[type="submit"].scp-Calls,
            .scp-Meetings,
            input[type="submit"].scp-Meetings,
            .scp-Tasks,
            input[type="submit"].scp-Tasks,
            .scp-Notes,
            input[type="submit"].scp-Notes,
            .scp-Calendar,
            input[type="submit"].scp-Calendar,
            .scp-Quotes,
            input[type="submit"].scp-Quotes,
            .scp-default-bg,
            .module-box, .crm_standalone .scp-login-form.scp-form .scp-send.last span>input,
            .crm_fullwidth .scp-login-form.scp-form .scp-send.last span>input,.scp-default-bg-btn, .scp-default-background{
        <?php
        echo 'background:' . get_option('biztech_scp_theme_color') . ' !important;border: 1px solid ' . get_option('biztech_scp_theme_color') . ' !important;';
        ?>
            }
            .scp-Cases-font a,
            .scp-Cases-font,
            .crm_fullwidth a.kb-attachment.scp-Cases-font i,
            .crm_standalone a.kb-attachment.scp-Cases-font i,
            .crm_fullwidth .container .scp-page-list-view .scp-table-responsive table td a.scp-Cases-font,
            .crm_standalone .container .scp-page-list-view .scp-table-responsive table td a.scp-Cases-font
            .crm_fullwidth .entry-content a.scp-Cases-font,
            .crm_standalone .entry-content a.scp-Cases-font,
            .scp-default-font,
            .scp-Account-font a,
            .scp-Accounts-font, 
            .crm_fullwidth a.kb-attachment.scp-Accounts-font i,
            .crm_standalone a.kb-attachment.scp-Accounts-font i,
            .crm_fullwidth .container .scp-page-list-view .scp-table-responsive table td a.scp-Account-font,
            .crm_standalone .container .scp-page-list-view .scp-table-responsive table td a.scp-Account-font,
            .crm_fullwidth .entry-content a.scp-Account-font,
            .crm_standalone .entry-content a.scp-Account-font,
            .scp-Calls-font a,
            .scp-Calls-font,
            .crm_fullwidth a.kb-attachment.scp-Calls-font i,
            .crm_standalone a.kb-attachment.scp-Calls-font i,
            .crm_fullwidth .container .scp-page-list-view .scp-table-responsive table td a.scp-Calls-font,
            .crm_standalone .container .scp-page-list-view .scp-table-responsive table td a.scp-Calls-font,
            .crm_fullwidth .entry-content a.scp-Calls-font,
            .crm_standalone .entry-content a.scp-Calls-font,
            .scp-Documents-font a,
            .scp-Documents-font,
            .crm_fullwidth a.kb-attachment.scp-Documents-font i,
            .crm_standalone a.kb-attachment.scp-Documents-font i,
            .crm_fullwidth .container .scp-page-list-view .scp-table-responsive table td a.scp-Documents-font,
            .crm_standalone .container .scp-page-list-view .scp-table-responsive table td a.scp-Documents-font,
            .crm_fullwidth .entry-content a.scp-Documents-font,
            .crm_standalone .entry-content a.scp-Documents-font,
            .scp-Leads-font a,
            .scp-Leads-font,
            .crm_fullwidth a.kb-attachment.scp-Leads-font i,
            .crm_standalone a.kb-attachment.scp-Leads-font i,
            .crm_fullwidth .container .scp-page-list-view .scp-table-responsive table td a.scp-Leads-font,
            .crm_standalone .container .scp-page-list-view .scp-table-responsive table td a.scp-Leads-font,
            .crm_fullwidth .entry-content a.scp-Leads-font,
            .crm_standalone .entry-content a.scp-Leads-font,
            .scp-Meetings-font a,
            .scp-Meetings-font,
            .crm_fullwidth a.kb-attachment.scp-Meetings-font i,
            .crm_standalone a.kb-attachment.scp-Meetings-font i,
            .crm_fullwidth .container .scp-page-list-view .scp-table-responsive table td a.scp-Meetings-font,
            .crm_standalone .container .scp-page-list-view .scp-table-responsive table td a.scp-Meetings-font,
            .crm_fullwidth .entry-content a.scp-Meetings-font,
            .crm_standalone .entry-content a.scp-Meetings-font,
            .scp-Opportunities-font a,
            .scp-Opportunities-font,
            .crm_fullwidth a.kb-attachment.scp-Opportunities-font i,
            .crm_standalone a.kb-attachment.scp-Opportunities-font i,
            .crm_fullwidth .container .scp-page-list-view .scp-table-responsive table td a.scp-Opportunities-font,
            .crm_standalone .container .scp-page-list-view .scp-table-responsive table td a.scp-Opportunities-font,
            .crm_fullwidth .entry-content a.scp-Opportunities-font,
            .crm_standalone .entry-content a.scp-Opportunities-font,
            .scp-Notes-font a,
            .scp-Notes-font,
            .crm_fullwidth a.kb-attachment.scp-Notes-font i,
            .crm_standalone a.kb-attachment.scp-Notes-font i,
            .crm_fullwidth .container .scp-page-list-view .scp-table-responsive table td a.scp-Notes-font,
            .crm_standalone .container .scp-page-list-view .scp-table-responsive table td a.scp-Notes-font,
            .crm_fullwidth .entry-content a.scp-Notes-font,
            .crm_standalone .entry-content a.scp-Notes-font,
            .scp-Tasks-font a,
            .scp-Tasks-font, 
            .crm_fullwidth a.kb-attachment.scp-Tasks-font i,
            .crm_standalone a.kb-attachment.scp-Tasks-font i,
            .crm_fullwidth .container .scp-page-list-view .scp-table-responsive table td a.scp-Tasks-font,
            .crm_standalone .container .scp-page-list-view .scp-table-responsive table td a.scp-Tasks-font,
            .crm_fullwidth .entry-content a.scp-Tasks-font,
            .crm_standalone .entry-content a.scp-Tasks-font,
            .scp-Calendar-font a,
            .scp-Calendar-font,
            .crm_fullwidth a.kb-attachment.scp-Calendar-font i,
            .crm_standalone a.kb-attachment.scp-Calendar-font i,
            .crm_fullwidth .container .scp-page-list-view .scp-table-responsive table td a.scp-Calendar-font,
            .crm_standalone .container .scp-page-list-view .scp-table-responsive table td a.scp-Calendar-font,
            .crm_fullwidth .entry-content a.scp-Calendar-font,
            .crm_standalone .entry-content a.scp-Calendar-font, 
            .crm_standalone .scp-login-form.scp-form .scp-send.last span.right a,
            .crm_fullwidth .scp-login-form.scp-form .scp-send.last span.right a,.scp-default-font
            .scp-KBDocuments-font a,
            .scp-KBDocuments-font, .scp-default-font a{
        <?php echo 'color:' . get_option('biztech_scp_theme_color') . ';'; ?>
            }
        <?php
        echo get_option('biztech_scp_custom_css');
    }
    ?>
    </style>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            if (jQuery('.datetimepicker').length > 0) {
                // Add any datepickers to all fields with the class
                jQuery('.datetimepicker').datetimepicker();
            }
        });
    </script>
    <?php
}

add_action('wp_head', 'bcp_js_variables');
add_action('admin_enqueue_scripts', 'bcp_enqueue_color_picker');

function bcp_enqueue_color_picker($hook_suffix) {
// first check that $hook_suffix is appropriate for your admin page
    wp_enqueue_style('wp-color-picker');
    wp_enqueue_script('my-script-handle', plugins_url('/assets/js/scp-js.js', __FILE__), array('wp-color-picker'), false, true);
}

//image upload
function bcp_admin_scripts() {
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_register_script('my-upload', plugins_url('/assets/js/scp-admin-js.js', __FILE__), array('jquery', 'media-upload', 'thickbox'));
    wp_enqueue_script('my-upload');
}

function bcp_admin_styles() {
    wp_enqueue_style('thickbox');
}

if (isset($_GET['page']) && $_GET['page'] == 'biztech-crm-portal') {//my_plugin_page
    add_action('admin_print_scripts', 'bcp_admin_scripts');
    add_action('admin_print_styles', 'bcp_admin_styles');
}

function bcp_theme_custom_upload_mimes($existing_mimes) {

    $allowed_mime_types = array('jpg|jpeg|jpe' => 'image/jpeg', 'gif' => 'image/gif', 'png' => 'image/png', 'bmp' => 'image/bmp', 'tif|tiff' => 'image/tiff', 'ico' => 'image/x-icon');

    $arr = array_diff($existing_mimes, $allowed_mime_types);
    foreach ($arr as $ar_key => $ar_val) {
        unset($arr[$ar_key]);
    }
    return $allowed_mime_types;
}

add_filter('upload_mimes', 'bcp_theme_custom_upload_mimes');

//For Sending mail
function mail_user($username_c, $password_c, $first_name, $last_name, $email) {
//for email goes to contact for password and we use that password to log in to the Portal sir comment
    $biztech_redirect_login = get_page_link(get_option('biztech_redirect_login'));
    $url = $biztech_redirect_login;
    $admin_email = get_option('admin_email');

//get admin username
    $user_info = get_user_by('email', $admin_email);
    $admin_username = $user_info->user_login;
//name set to mail
    if ($first_name != '') {
        $name = $first_name . " " . $last_name;
    } else {
        $name = $last_name;
    }
    $to = $email;
//subject
    if (get_option('biztech_scp_mail_subject') != NULL) {
        $subject = get_option('biztech_scp_mail_subject');
    } else {
        $subject = 'Biztech CRM Portal Account Details';
    }
//body
    if (get_option('biztech_scp_mail_body') != NULL) {
        $body = get_option('biztech_scp_mail_body');
        $body = 'Dear ' . $name . ',' . "<br> <br>" . get_option('biztech_scp_mail_body') . "<br> <br>" . 'Your CRM Portal Username is: ' . $username_c . "<br> " . 'Your CRM Portal Password is: ' . $password_c . "<br> <br>" . 'Log in at: ' . $url . ' to get started';
    } else {
        $body = 'Dear ' . $name . ',' . "<br> <br>" . "Thank you for creating an Biztech CRM Portal account. Now you can manage your different modules." . "<br> <br>" . 'Your CRM Portal Username is: ' . $username_c . "<br> " . 'Your CRM Portal Password is: ' . $password_c . "<br> <br>" . 'Log in at: ' . $url . ' to get started';
    }
    if (get_option('biztech_scp_upload_image') != NULL) {
        $crm_logo = "<img src='" . get_option('biztech_scp_upload_image') . "' title='" . get_bloginfo('name') . "' alt='" . get_bloginfo('name') . "' style='border:none' width='100'>";
    } else {
        $crm_logo = get_bloginfo('name');
    }
    $body_full = "<table width='100%' cellspacing='0' cellpadding='0' border='0' style='background:#e4e4e4; padding:30px 0;'>
  <tbody>
    <tr>
      <td><center>
            <table width='800' cellspacing='0' cellpadding='0' border='0' style='font-family: arial,sans-serif; font-size:14px; border-top:4px solid #35b0bf; background:#fff; line-height: 22px;'>
                <tr>
                    <td style='padding: 30px 40px 0;'>
                        <table width='100%' cellspacing='0' cellpadding='0' border='0' style='border-bottom:2px solid #ddd;'>
                            <tr>
                                <td style='padding-bottom:20px;'>
                                    <a href='#'>
                                        $crm_logo
                                    </a>
                                </td>
                                <td align='right' valign='top' style='padding-bottom:20px;'>
                                   <h2 style='margin:0; padding:0; color:#35b0bf; font-size:22px;'>" . get_option('biztech_scp_name') . "</h2> 
                                </td>
                            </tr>
                        </table>
                    </td>
                    
                </tr>
                <tr>
                    <td style='padding:30px 40px 0; color: #484848;' align='left' valign='top'>
                         <p>" . $body . "</p>
                        <p>Regards, <br/>
                        " . get_bloginfo('name') . "</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width='100%' cellspacing='0' cellpadding='0' border='0' style='background:#f7f7f7; padding:15px 40px;'>
                            <tr>
                                <td align='left' valign='middle'>
                                    <a target='_blank' style='color: #35b0bf;text-decoration: none; font-size:14px;' href='" . get_site_url() . "'>" . get_site_url() . "</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center></td>
    </tr>
</tbody>
</table>";
    add_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
    wp_mail($to, $subject, $body_full);
    remove_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
}

//For Sending mail for forgot password
function mail_user_forgotpwd($username, $password, $url, $email) {
//for email goes to contact for password and we use that password to log in to the Portal sir comment
    $biztech_redirect_login = get_option('biztech_redirect_login');
    $url = $biztech_redirect_login;
    $admin_email = get_option('admin_email');

//get admin username
    $user_info = get_user_by('email', $admin_email);
    $admin_username = $user_info->user_login;
//name set to mail
    if ($first_name != '') {
        $name = $first_name . " " . $last_name;
    } else {
        $name = $last_name;
    }
    $to = $email;
    if (get_option('biztech_scp_upload_image') != NULL) {
        $crm_logo = "<img src='" . get_option('biztech_scp_upload_image') . "' title='" . get_bloginfo('name') . "' alt='" . get_bloginfo('name') . "' style='border:none' width='100'>";
    } else {
        $crm_logo = get_bloginfo('name');
    }
    $body = 'Dear ' . $username . ',' . "<br> <br>" . "This notice confirms that your password was changed on " . get_bloginfo('name') . ".<br> <br>Your Biztech CRM Portal account details is as below : " . "<br> <br>" . 'Your CRM Portal Username is: ' . $username . "<br>" . 'Your CRM Portal Password is: ' . $password;
    $subject = get_bloginfo('name') . ': Password Recover';
//sent mail
    $body_full = "<table width='100%' cellspacing='0' cellpadding='0' border='0' style='background:#e4e4e4; padding:30px 0;'>
  <tbody>
    <tr>
      <td><center>
            <table width='800' cellspacing='0' cellpadding='0' border='0' style='font-family: arial,sans-serif; font-size:14px; border-top:4px solid #35b0bf; background:#fff; line-height: 22px;'>
                <tr>
                    <td style='padding: 30px 40px 0;'>
                        <table width='100%' cellspacing='0' cellpadding='0' border='0' style='border-bottom:2px solid #ddd;'>
                            <tr>
                                <td style='padding-bottom:20px;'>
                                    <a href='#'>
                                        $crm_logo
                                    </a>
                                </td>
                                <td align='right' valign='top' style='padding-bottom:20px;'>
                                   <h2 style='margin:0; padding:0; color:#35b0bf; font-size:22px;'>" . get_option('biztech_scp_name') . "</h2> 
                                </td>
                            </tr>
                        </table>
                    </td>
                    
                </tr>
                <tr>
                    <td style='padding:30px 40px 0; color: #484848;' align='left' valign='top'>
                         <p>" . $body . "</p>
                        <p>Regards, <br/>
                        " . get_bloginfo('name') . "</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width='100%' cellspacing='0' cellpadding='0' border='0' style='background:#f7f7f7; padding:15px 40px;'>
                            <tr>
                                <td align='left' valign='middle'>
                                    <a target='_blank' style='color: #35b0bf;text-decoration: none; font-size:14px;' href='" . get_site_url() . "'>" . get_site_url() . "</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center></td>
    </tr>
</tbody>
</table>";

    add_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
    $return_var = wp_mail($to, $subject, $body_full);
    remove_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
    return $return_var;
}

function get_timezone_offset($remote_tz, $origin_tz = null) {
    if ($origin_tz === null) {
        if (!is_string($origin_tz = date_default_timezone_get())) {
            return false; // A UTC timestamp was returned -- bail out!
        }
    }
    $origin_dtz = new DateTimeZone($origin_tz);
    $remote_dtz = new DateTimeZone($remote_tz);
    $origin_dt = new DateTime("now", $origin_dtz);
    $remote_dt = new DateTime("now", $remote_dtz);
    $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
    return $offset;
}

/* Added for output buffer ob_start on 03-aug-2016 */
add_action('init', 'do_output_buffer');

function do_output_buffer() {
    ob_start();
}

/* Added for logut from wp user also logout from CRM portal */

function portal_logut_on_wplogout() {
    if (get_option('biztech_scp_single_signin') != '' && is_user_logged_in() && !current_user_can('manage_options')) {//Added by BC on 02-aug-2016
        unset($_SESSION['scp_user_id']);
        unset($_SESSION['scp_user_account_name']);
        unset($_SESSION['module_array']);
        unset($_SESSION['user_timezone']);
        unset($_SESSION['assigned_user_list']);
        unset($_SESSION['user_date_format']);
        unset($_SESSION['user_time_format']);
        $redirect_url = explode('?', $_SERVER['REQUEST_URI'], 2);
        $redirect_url = $redirect_url[0];

        wp_redirect($redirect_url);
        exit;
    }
}

add_action('wp_logout', 'portal_logut_on_wplogout');

function wpdocs_set_html_mail_content_type() {
    return 'text/html';
}
?>