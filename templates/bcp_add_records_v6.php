<?php

foreach ($results as $k => $vals_lbl) {
    foreach ($vals_lbl->rows as $k_lbl2 => $vals) {
        foreach ($vals as $key_fields => $val_fields) {
            if (!empty($val_fields)) {
                $name_arry = array();
                $ar_val = $val_fields->name;
                $name_arry[] = $ar_val;
                $res2 = $objSCP->get_module_fields($module_name, $name_arry);
                foreach ($res2->module_fields as $k_fileds => $fileds) {
                    if (($fileds->name == 'date_start' || $fileds->name == 'date_due' || $fileds->name == 'date_end') && (empty($_REQUEST[$fileds->name]) || $_REQUEST[$fileds->name] == '' || $_REQUEST[$fileds->name] == NULL)) {
                        $_REQUEST[$fileds->name] = '';
                    } else if ($fileds->name == 'date_start' || $fileds->name == 'date_due' || $fileds->name == 'date_end') {
                        //Updated by BC on 20-jun-2016 for timezone store in session
                        if (empty($_SESSION['user_timezone'])) {
                            $result_timezone = $objSCP->getUserTimezone();
                        } else {
                            $result_timezone = $_SESSION['user_timezone'];
                        }
                        $offset = get_timezone_offset($result_timezone, 'UTC');
                        $GetDate = gmdate("Y-m-d H:i:s", strtotime($_REQUEST[$fileds->name]) + $offset);
                        $_REQUEST[$fileds->name] = $GetDate;
                    }
                        $pass_name_arry[$fileds->name] = stripslashes_deep($_REQUEST[$fileds->name]);
                }

                if ($val_fields->name == 'birthdate' || $val_fields->name == 'active_date' || $val_fields->name == 'exp_date') {
                    if (!empty($_REQUEST[$fileds->name])) {
                        $GetDate4 = date("Y-m-d", strtotime($_REQUEST[$val_fields->name]));
                        $_REQUEST[$val_fields->name] = $GetDate4;
                    } else {
                        $_REQUEST[$fileds->name] = '';
                    }
                }
                if ($val_fields->name == "account_name" && $module_name != "Leads") {
                    $pass_name_arry["account_id"] = stripslashes_deep($_REQUEST[$val_fields->name]);
                } else if ($val_fields->name == "assigned_user_name") {
                    $pass_name_arry["assigned_user_id"] = stripslashes_deep($_REQUEST[$val_fields->name]);
                } else if ($val_fields->name == "email" && $module_name == "Leads") {
                    $pass_name_arry["email"] = array(array('email_address' => stripslashes_deep($_REQUEST[$val_fields->name])));
                } else if ($val_fields->name == "do_not_call" && $module_name == "Leads" && $_REQUEST[$val_fields->name] == '') {
                    $pass_name_arry["do_not_call"] = 0;
                } else if ($val_fields->name == "is_template" && (stripslashes_deep($_REQUEST[$val_fields->name] == '') || stripslashes_deep($_REQUEST[$val_fields->name] == null))) {
                    $pass_name_arry["is_template"] = 0;
                } else if ($val_fields->name == "parent_name") {
                    $pass_name_arry["parent_type"] = stripslashes_deep($_REQUEST[$val_fields->name]);
                    $pass_name_arry["parent_id"] = stripslashes_deep($_REQUEST["parent_id"]);
                } 
//                else if ($val_fields->name == "team_name") {
//                    $team_name_array = $objSCP->getRecordDetail("Contacts", $_SESSION['scp_user_id']);
//                    //$team_id = $team_name_array->team_name[0]->id;
//                    $pass_name_arry[$val_fields->name] = $team_name_array->team_name;
//                } 
//                else if ($val_fields->name == "campaign_name") {//Added by BC on 09-dec-2015
//                    $pass_name_arry["campaign_id"] = stripslashes_deep($_REQUEST[$val_fields->name]);
//                } 
                else if ($val_fields->name == "embed_flag") {//Added by BC on 09-dec-2015
                    if (stripslashes_deep($_REQUEST[$val_fields->name] == '') || stripslashes_deep($_REQUEST[$val_fields->name] == null)) {
                        $pass_name_arry["embed_flag"] = 0;
                    } else {
                        $pass_name_arry["embed_flag"] = stripslashes_deep($_REQUEST[$val_fields->name]);
                    }
                } else {
                    $pass_name_arry[$val_fields->name] = stripslashes_deep($_REQUEST[$val_fields->name]);
                }
            }
        }
    }
}
?>