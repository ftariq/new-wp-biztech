<?php

if (isset($id) && !empty($id) && ($module_name != "Notes" && $casenote == null)) {
    $html .= "
                        <div class='scp-form-title scp-$module_name-font'>
                        <h3>Edit " . $module_without_s . "</h3>";
} else {
    $html .= "
                        <div class='scp-$module_name-font scp-form-title'>
                        <h3>Add New " . $module_without_s . "</h3>";
}
if (isset($id) && !empty($id) && $module_name != 'Quotes' && $module_name != 'KBDocuments') {
    $view = 'edit';
    $deleted = 1;
    $html .= "<div class='scp-move-action-btn'><a id='clear_btn_id' onclick='bcp_clear_search_txtbox(0,\"$module_name\",\"\",\"\",\"\",\"list\",\"$current_url\");' href='javascript:void(0);'  class='scp-$module_name scp-dtl-viewbtn' title='List'><span class='fa fa-list' ></span><span>LIST</span></a><a href='javascript:void(0);' title='Delete' class='deletebtn scp-$module_name scp-dtl-deletebtn' onclick='form_submit(\"$module_name\",\"$view\",\"$id\",\"$deleted\",\"$current_url\");'><span class='fa fa-trash-o'></span><span>DELETE</span></a></div>";
} else if ($module_name != 'Quotes' && $module_name != 'KBDocuments') {

    $html .= "<div class='scp-move-action-btn'><a id='clear_btn_id' onclick='bcp_clear_search_txtbox(0,\"$module_name\",\"\",\"\",\"\",\"list\",\"$current_url\");' href='javascript:void(0);'  class='scp-$module_name scp-dtl-viewbtn' title='List'><span class='fa fa-list' ></span><span>LIST</span></a></div>";
}
$html.="</div>";
$html .= "<div class='scp-form scp-form-2-col'>
                    <form action='" . home_url() . "/wp-admin/admin-post.php' method='post' enctype='multipart/form-data' id='general_form_id'>
                    <div class='scp-form-container'>";
$where_condition = array();
foreach ($results as $k => $vals_lbl) {
    $html .= "<div class='panel " . $vals_lbl->lable_value . " scp-dtl-panel'><div class='scp-col-12 panel-title'><span class='panel_name'>" . $vals_lbl->lable_value . "</span></div>";
    $cb = 0;
    foreach ($vals_lbl->rows as $k_lbl2 => $vals) {
        if (count($vals) > 1) {
            $single_double = "6";
        } else {
            $single_double = "12";
        }
        $g = 0;
        $cb++;
        foreach ($vals as $key_fields => $val_fields) {
            $maxlength_field = '';
            if (count($val_fields) == "0" || $val_fields->name == "team_name") {
                continue;
            }
            if (isset($val_fields->displayParams)) {
                if ($val_fields->displayParams->cols != '' && $val_fields->displayParams->cols != '' && $val_fields->displayParams->maxlength != '') {
                    $cols = $val_fields->displayParams->cols;
                    $rows = $val_fields->displayParams->rows;
                    $max_len = $val_fields->displayParams->maxlength;
                    $maxlength_field = "maxlength='" . $max_len . "'";//max length validation
                }
            } else {
                $cols = 30;
                $rows = 2;
                //$max_len = 150;
            }
            if ($val_fields->type == 'readonly') {
                $readonly = 'readonly';
            } else {
                $readonly = '';
            }
            $ar_val = $val_fields->name;
            if (($module_name == "Meetings" && (($val_fields->name == "duration_hours") || ($val_fields->name == "duration_minutes"))) || ($val_fields->name == "parent_id" && $val_fields->type == "id")) {
                continue;
            }

            $class_name = '';
            $ftype = "text";
            if ($val_fields->type == 'assigned_user_name') {
                continue;
            }
            if ($val_fields->type == 'int') {
                if (empty($val_fields->options)) {
                    array_push($text_sugar_array, $val_fields->type);
                } else {
                    array_push($select_options_sugar_array, $val_fields->type);
                }
            }
            $req = '';
            $option_flg = false;
            $txtarea_flg = false;
            $option_relate_flg = false;
            $option_relate_flg_text = false;
            $check_box_flg = false;
            if ($ar_val == "account_name" && isset($id) && !empty($id) && $module_name != "Leads") {
                $ar_val = "account_id";
            }
            if ($ar_val == "assigned_user_name" && isset($id) && !empty($id)) {
                $ar_val = "assigned_user_id";
            }
            if ($ar_val == "campaign_name" && isset($id) && !empty($id)) {//Added by BC on 09-dec-2015
                $ar_val = "campaign_id";
            }
            if (!empty($record_detail)) {
                $value = $record_detail->$ar_val;
            } else {
                $value = '';
            }
            //Added by BC on 08-jul-2016
            $dwload = $cls = '';
            if (!empty($value)) {
                $dwload = "<i class='fa fa-download' aria-hidden='true'></i> Download";
                $cls = "general-link-btn scp-download-btn";
            }
            if (in_array($val_fields->type, $text_sugar_array) && $val_fields->type != 'address') {
                if ($val_fields->type == 'file') {
                    $ftype = "file";
                } else {
                    $ftype = "text";
                }
                if ($val_fields->type == 'datetimecombo' || $val_fields->type == 'datetime') {
                    $class_name = 'datetimepicker';
                }
                if ($val_fields->type == 'int' && !empty($val_fields->options)) {
                    $option_flg = true;
                }
                if ($val_fields->type == 'email') {
                    $ftype = "email";
                }
                if ($val_fields->type == "currency_id") {//If field is of type id
                    $option_flg = true;
                }
            } else if (in_array($val_fields->type, $textarea_sugar_array) || $val_fields->type == 'address') {
                $txtarea_flg = true;
            } else if (in_array($val_fields->type, $select_options_sugar_array)) {
                $option_flg = true;
            } else if (in_array($val_fields->type, $select_relate_options_sugar_array)) {
                if ($val_fields->name == "contact_name") {
                    $option_relate_flg_text = true;
                    $record_detail_contact = $objSCP->getRecordDetail('Contacts', $_SESSION['scp_user_id']);
                    $value = $record_detail_contact->name;
                    $readonly = 'disabled';
                } else {
                    $option_relate_flg = true;
                }
            } else if (in_array($val_fields->type, $check_box_sugar_array)) {//Added by BC on 07-nov-2015
                $check_box_flg = true;
            }
            if ($val_fields->required == "true") {
                $req = "required";
            }
            if ($val_fields->name == 'case_number') {
                $readonly = 'readonly';
                $req = '';
            }
            if ($option_flg == true) {
                $html .= "<div class='scp-col-" . $single_double . "'>
                                    <div class='scp-form-group " . $req . "'>
                                    <label>" . ucfirst($val_fields->label_value) . "</label>";
                $options = $val_fields->options;
                $html .= "<select class='input-text scp-form-control' id='" . $ar_val . "' name='" . $ar_val . "'>";
                if ($val_fields->type == "currency_id") {//if currency field
                    if (isset($_SESSION['Currencies'])) {
                        $curr_arry = (array) $_SESSION['Currencies'];
                        foreach ($curr_arry as $k_curr => $v_curr) {
                            $html .= "<option value='" . $k_curr . "'";
                            if ($k_curr == $value) {
                                $html .= 'selected';
                            }
                            $html .= ">" . $v_curr->name . " : " . $v_curr->symbol . "</option>";
                        }
                    }
                } else {
                    if (is_array($options)) {
                        foreach ($options as $k_op => $v_op) {

                            $html .= "<option value='" . $v_op->value . "'";

                            if ($v_op->value == $value) {//update 
                                $html .= ' selected ';
                            }
                            $html .= ">" . $v_op->name . "</option>";
                        }
                    } else {
                        foreach ($options as $k_op => $v_op) {

                            $html .= "<option value='" . $options->$k_op->value . "'";

                            if ($options->$k_op->value == $value) {//update 
                                $html .= ' selected ';
                            }
                            $html .= ">" . $options->$k_op->name . "</option>";
                        }
                    }
                }
                $html .= "</select></div></div>";
            } else if ($txtarea_flg == true) {
                $html .= "<div class='scp-col-" . $single_double . "'>
                                    <div class='scp-form-group " . $req . "'>
                                    <label>" . ucfirst($val_fields->label_value) . "</label>
                                    <textarea class='input-text scp-form-control' name='" . $ar_val . "' cols='" . $cols . "' rows='" . $rows . "' $maxlength_field " . $readonly . ">" . $value . "</textarea>
                                    </div>
                                    </div>";
            } else if ($option_relate_flg == true) {
                if ($val_fields->relate_module == 'Users' || in_array($val_fields->relate_module, (array) $_SESSION['module_array'])) {//if relate module is not in active module array 
                    $html .= "<div class='scp-col-" . $single_double . "'>
                                    <div class='scp-form-group " . $req . "'>
                                    <label>" . ucfirst($val_fields->label_value) . "</label>
                                    <div id='related_to_id'>";
                    $select_fields = "id,name";
                    if ($val_fields->name == "assigned_user_id" || $val_fields->name == "assigned_user_name") {// for assigned to all records shown of users
                        //Added by BC on 20-jun-2016
                        if (empty($_SESSION['assigned_user_list'])) {//check session value in module array Added by BC on 15-jun-2016
                            $record_detail2 = $objSCP->getModuleRecords("Users", $select_fields);
                        } else {
                            $record_detail2 = $_SESSION['assigned_user_list'];
                        }
                        $ar_val = "assigned_user_name";
                    } else if ($val_fields->name == "account_name") {
                        $record_detail2 = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], strtolower("Accounts"), $select_fields, $where_condition, '', '', 'date_entered:DESC');
                    } else if ($val_fields->name == "campaign_name") {
                        $record_detail2 = $objSCP->getModuleRecords("Campaigns", $select_fields);
                    } else {
                        $record_detail2 = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $select_fields, $where_condition, '', '', 'date_entered:DESC');
                    }
                    if ($ar_val == "related_doc_name" && isset($id) && !empty($id)) {//related doc id change
                        $varRel = "related_doc_id";
                        $value = $record_detail->$varRel;
                    }
                    $html .= "<select class='input-text scp-form-control' id='" . $val_fields->name . "' name='" . $ar_val . "' $req>
                                    <option value=''></option>";
                    if (isset($record_detail2->records)) {
                        for ($m = 0; $m < count($record_detail2->records); $m++) {
                            $mod_id = $record_detail2->records[$m]->id;
                            $mod_name = $record_detail2->records[$m]->name;
                            $html .= "<option value='" . $mod_id . "'";
                            if ($mod_id == $value) {
                                $html .= 'selected';
                            }
                            $html .= ">" . $mod_name . "</option>";
                        }
                    }
                    $html .= "</select>
                                    </div>
                                    </div>
                                    </div>";
                }
            } else if ($option_relate_flg_text == true) {
                $html .= "<div class='scp-col-" . $single_double . "'>
                                    <div class='scp-form-group " . $req . "'>
                                    <label>" . ucfirst($val_fields->label_value) . "</label>
                                    <input class='input-text scp-form-control' type='" . $ftype . "' id='" . $val_fields->name . "' name='" . $ar_val . "' value='";
                $html .= "" . $value . "'" . $req . " " . $readonly . "/>
                                    </div>
                                    </div>";
            } else if ($check_box_flg == true) {
                if ($value) {
                    $chekd = "checked='checked'";
                } else {
                    $chekd = "";
                    $value = 0;
                }
                $html .= "<div class='scp-col-" . $single_double . "'>
                                    <div class='scp-form-group " . $req . "'>
                                    <label>" . ucfirst($val_fields->name) . "</label>
                                    <input class='input-text scp-form-control scp-special-checkbox' type='checkbox' id='checkbox_" . $ar_val . "' name='" . $ar_val . "' value='";
                $html .= "" . $value . "'" . $req . " " . $readonly . " onclick='chek(this.id)' $chekd/>
                                    </div>
                                    </div>";
            } else {
                if ($val_fields->name == "parent_name" && $val_fields->type == "parent") {
                    if ($module_name == "Notes" && $casenote != null) {
                        $html .= "<div class='scp-col-6'>
                                        <div class='scp-form-group " . $req . "'>
                                        <label>" . ucfirst($val_fields->label_value) . "</label>";

                        if (empty($_SESSION['module_array'])) {//check session value in module array Added by BC on 20-jun-2016
                            $modules_array = $objSCP->getPortal_accessibleModules();
                            $modules = $modules_array->accessible_modules;
                        } else {
                            $modules = $_SESSION['module_array'];
                        }
                        $html .= "<select id='parent_name' class='scp-form-control' name='parent_name' disabled onchange='get_module_list_v7()'> ";
                        foreach ($modules as $key => $values) {
                            if ($values == $module_name) {
                                continue;
                            }
                            if ($values != "Contacts" && $values != $module_name) {
                                $html .= "<option value='" . $values . "'";
                                $html .= ">" . "Cases" . "</option>";
                            }
                        }
                        $html .= "</select>
                                        </div>
                                        </div>
                                        <div class='scp-col-6'>
                                        <div class='scp-form-group " . $req . "'>
                                        <label>Parent ID:</label><div id='related_to_id2'>";
                        if ($id == '') {
                            $parent_type = "Accounts";
                        }
                        $module_name_parent = 'Cases';
                        $select_fields = "id,name";
                        $record_detail3 = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], strtolower($module_name_parent), $select_fields, $where_condition, '', '', 'date_entered:DESC');
                        $html .= "
                                        <select class='input-text scp-form-control' title='' id='parent_id' name='parent_id' disabled>
                                        <option value=''></option>";
                        if (isset($record_detail3->records)) {
                            for ($m = 0; $m < count($record_detail3->records); $m++) {
                                $mod_id = $record_detail3->records[$m]->id;
                                $mod_name = $record_detail3->records[$m]->name;
                                $html .= "<option value='" . $casenote . "'";
                                if ($mod_id == $casenote) {
                                    $html .= 'selected';
                                }
                                $html .= ">" . $mod_name . "</option>";
                            }
                        }
                        $html .= "</select>
                                        </div></div></div>";
                    } else {
                        $html .= "<div class='scp-col-6'>
                                        <div class='scp-form-group " . $req . "'>
                                        <label>" . ucfirst($val_fields->label_value) . "</label>";
                        if (empty($_SESSION['module_array'])) {//check session value in module array Added by BC on 20-jun-2016
                            $modules_array = $objSCP->getPortal_accessibleModules();
                            $modules = $modules_array->accessible_modules;
                        } else {
                            $modules = $_SESSION['module_array'];
                        }
                        $html .= "<select id='parent_name' class='scp-form-control' name='parent_name' onchange='get_module_list_v7()'>";
                        foreach ($modules as $key => $values) {
                            if ($values == $module_name) {
                                continue;
                            }
                            if ($values != "Contacts" && $values != $module_name) {
                                $html .= "<option value='" . $key . "'";
                                if ($key == $parent_type) {
                                    $html .= 'selected';
                                }
                                $html .= ">" . $values . "</option>";
                            }
                        }
                        $html .= "</select>
                                        </div>
                                        </div>
                                        <div class='scp-col-6'>
                                        <div class='scp-form-group " . $req . "'>
                                        <label>Parent ID:</label><div id='related_to_id2'>";
                        if (empty($id) && $id == '') {
                            $parent_type = "Accounts";
                        }
                        $module_name_parent = $parent_type;
                        $select_fields = "id,name";
                        $record_detail3 = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], strtolower($module_name_parent), $select_fields, $where_condition, '', '', 'date_entered:DESC');
                        $html .= "
                                        <select class='input-text scp-form-control' title='' id='parent_id' name='parent_id'>
                                        <option value=''></option>";
                        if (isset($record_detail3->records)) {
                            for ($m = 0; $m < count($record_detail3->records); $m++) {
                                $mod_id = $record_detail3->records[$m]->id;
                                $mod_name = $record_detail3->records[$m]->name;
                                $html .= "<option value='" . $mod_id . "'";
                                if ($mod_id == $parent_id) {
                                    $html .= 'selected';
                                }
                                $html .= ">" . $mod_name . "</option>";
                            }
                        }
                        $html .= "</select>
                                        </div></div></div>";
                    }
                } else {//not parent field then
                    if (($val_fields->name == 'email' || $val_fields->name == 'email1')) {
                        if ($val_fields->required == "true") {
                            $class_name = 'required email';
                        } else {
                            $class_name = 'email';
                        }
                    }
                    $html .= "<div class='scp-col-" . $single_double . "'>
                                        <div class='scp-form-group " . $req . "'>
                                        <label>" . ucfirst($val_fields->label_value) . "</label>";
                    if ($ftype == 'file' && isset($id) && !empty($id) && ($module_name != "Notes" && $casenote == null)) {
                        $html .= "<br><a href='javascript:void(0);' onclick='form_submit_document(\"$id\");' class='" . $cls . " scp-$module_name-font'>" . $dwload . "</a>";
                        $html .= "<input type='hidden' name='edit-file' value='" . $value . "'>";
                    } else {
                        $html .= "<input class='input-text scp-form-control $class_name avia-datepicker-div' type='" . $ftype . "' id='" . $ar_val . "' name='" . $ar_val . "' value='";
                        if ($val_fields->type == 'url' && !isset($id) && empty($id)) {
                            $html .= 'http://';
                        }
                        if ($val_fields->type == 'email' && isset($id) && !empty($id)) {
                            $value = $value[0]->email_address;
                        }
                        if (empty($val_fields)) {
                            $flag = 0;
                        } else {
                            $flag = $val_fields->type == 'datetime';
                        }
                        if (($val_fields->type == 'datetimecombo' || $flag) && isset($id) && !empty($id)) {
                            //Updated by BC on 20-jun-2016 for timezone store in session
                            if (empty($_SESSION['user_timezone'])) {
                                $result_timezone = $objSCP->getUserTimezone();
                            } else {
                                $result_timezone = $_SESSION['user_timezone'];
                            }
                            $offset = get_timezone_offset('UTC', $result_timezone->timezone);
                            $value = date('m/d/Y H:i', strtotime($value) + $offset);
                        }


                        if ($val_fields->name == 'file_mime_type') {
                            $readonly = 'readonly';
                        }

                        $html .= "" . htmlspecialchars($value, ENT_QUOTES, 'UTF-8', false) . "'" . $req . " " . $readonly . "/>";
                        if ($module_name == "Notes" && $ftype == 'file' && isset($id) && !empty($id)) {
                            if (!empty($value)) {
                                $html .= "<br><a href='javascript:void(0);' onclick='form_submit_note_document(\"$id\");' class='" . $cls . " scp-$module_name-font'>" . $dwload . "</a>";
                                $html .= "<input type='hidden' name='edit-file' value='" . $value . "'>";
                            }
                        }
                    }
                    $html .= "</div>
                                        </div>";
                }
            }
            $g++;
            if ($g % 2 == 0) {
                $html .= " <div class='clearfix'></div>";
                $g = 0;
            }
        }
    }
    $html .= "</div>";
}
$html .= "<div class='scp-form-actions'>
                <input type='hidden' name='action' value='bcp_add_moduledata_call'>
                <input type='hidden' name='module_name' value='" . $module_name . "'>
                <input type='hidden' name='view' value='" . $view . "'>
                <input type='hidden' name='current_url' value=" . $current_url . ">";
if (isset($id) && !empty($id)) {
    $html .= "<input type='hidden' name='id' value='" . $id . "'>";
}
if ($module_name == "Notes" && $casenote != null) {
    $html .= "<input type='hidden' name='case_id' value='" . $casenote . "'>";
}
$html .= "<span><input type='submit' value='Submit' class='hover active scp-button action-form-btn scp-$module_name' />";
if ($casenote == null) {//in case view note section remove cancel button
    $html .= "&nbsp&nbsp<input type='button' value='Cancel' class='hover active scp-button action-form-btn' onclick=\"window.location='" . home_url() . "/portal-manage-page/?list-$module_name'\"/></span>";
}
$html .= "</div>
                </div>
                </form>
                </div>";
