<?php

$pro_budle_cnt = count($quote_entry->Result->ProductBundle) - 1;
$html .= '<div class="scp-form scp-form-2-col">
    <div class="panel" id="detailpanel_3">
    <div class="scp-col-12 panel-title"><span class="panel_name">Line Items:</span></div>
    <div class="tbl-responsive">
        <table cellspacing="0" class="panelContainer" id="LBL_LINE_ITEMS">
        ';
if ($pro_budle_cnt >= 0) {
    $html .= '<tr>

                <td width="37.5%" colspan="2" field="line_items" type="function" class="inlineEdit first">';
}
foreach ($quote_entry->Result->ProductBundle as $k_pro => $v_pro) {
    $html .= '<table width="100%" cellspacing="0" cellpadding="0" border="0">';
    if($v_pro->name != ''){
    $html .= '<tr class="group-quotes-wrapper">
                    <th scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL" colspan="2">Group Name:</th>
                    <td style="text-align: left;padding:2px;" colspan="7" class="tabDetailViewDL">' . $v_pro->name . '</td>
                </tr>';
    }
    $ct = 1;
    $ct_qty = 1;
    foreach ($v_pro->Product as $k_line => $v_line) {
        if ($v_line->product_qty == '') {
            $html .= '<tr>
                    <th width="5%" scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL">&nbsp;</th>
                    <th width="10%" scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL">Service</th>
                    <th width="12%" scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL">&nbsp;</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">List</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Discount</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Sale Price</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Tax</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Tax Amount</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Total</th>';
        } else {
            if ($ct_qty == 1) {
                $html .= '<tr>
                    <th width="5%" scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL">&nbsp;</th>
                    <th width="10%" scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL">Quantity</th>
                    <th width="12%" scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL">Product</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">List</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Discount</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Sale Price</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Tax</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Tax Amount</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Total</th>
                </tr>';
            }
            $ct_qty++;
        }
        $html .= '<tr>
                    <td style="text-align: left; padding:2px;" class="tabDetailViewDF">' . $ct . '</td>';
        if ($v_line->product_qty == '') {
            $html .= '<td style="padding:2px;" class="tabDetailViewDF">' . $v_line->name . '</td>
                    <td style="padding:2px;" class="tabDetailViewDF"></td>';
        } else {
            $html .= '<td style="padding:2px;" class="tabDetailViewDF">' . intval($v_line->product_qty) . '</td>
                    <td style="padding:2px;" class="tabDetailViewDF">' . $v_line->name . '</td>';
        }
        $html .= '<td style="text-align: right; padding:2px;" class="tabDetailViewDF">' . $currency_symbol . number_format($v_line->product_list_price, 2) . '</td>';
        if ($v_line->discount == 'Amount') {
            $html .= '<td style="text-align: right; padding:2px;" class="tabDetailViewDF">' . $currency_symbol . number_format($v_line->product_discount, 2) . '</td>';
        } else {
            $html .= '<td style="text-align: right; padding:2px;" class="tabDetailViewDF">' . number_format($v_line->product_discount, 2) . '%</td>';
        }
        $html .= '<td style="text-align: right; padding:2px;" class="tabDetailViewDF">' . $currency_symbol . number_format($v_line->product_unit_price, 2) . '</td>
                    <td style="text-align: right; padding:2px;" class="tabDetailViewDF">' . number_format($v_line->vat, 2) . '%</td>
                    <td style="text-align: right; padding:2px;" class="tabDetailViewDF">' . $currency_symbol . number_format($v_line->vat_amt, 2) . '</td>
                    <td style="text-align: right; padding:2px;" class="tabDetailViewDF">' . $currency_symbol . number_format($v_line->product_total_price, 2) . '</td>
                </tr>
                ';
        $ct++;
    }
    if(count($v_pro->Product) > 0){
    $html .= '<tr>
                    <td nowrap="nowrap" colspan="9"><br></td>
                </tr>
                <tr>
                    <td scope="row" style="text-align: right;padding:2px;" colspan="8" class="tabDetailViewDL">Total:&nbsp;&nbsp;</td>
                    <td style="text-align: right;padding:2px;" class="tabDetailViewDL">' . $currency_symbol . number_format($v_pro->total_amt, 2) . '</td>
                </tr>
                <tr>
                    <td scope="row" style="text-align: right;padding:2px;" colspan="8" class="tabDetailViewDL">Discount:&nbsp;&nbsp;</td>
                    <td style="text-align: right;padding:2px;" class="tabDetailViewDL">' . $currency_symbol . number_format($v_pro->discount_amount, 2) . '</td>
                </tr>
                <tr>
                    <td scope="row" style="text-align: right;padding:2px;" colspan="8" class="tabDetailViewDL">Subtotal:&nbsp;&nbsp;</td>
                    <td style="text-align: right;padding:2px;" class="tabDetailViewDL">' . $currency_symbol . number_format($v_pro->subtotal_amount, 2) . '</td>
                </tr>
                <tr>
                    <td scope="row" style="text-align: right;padding:2px;" colspan="8" class="tabDetailViewDL">Tax:&nbsp;&nbsp;</td>
                    <td style="text-align: right;padding:2px;" class="tabDetailViewDL">' . $currency_symbol . number_format($v_pro->tax_amount, 2) . '</td>
                </tr>
                <tr>
                    <td scope="row" style="text-align: right;padding:2px;" colspan="8" class="tabDetailViewDL"><b>Grand Total:</b>&nbsp;&nbsp;</td>
                    <td style="text-align: right;padding:2px;" class="tabDetailViewDL"><b>' . $currency_symbol . number_format($v_pro->total_amount, 2) . '</b></td>
                </tr>';
    }
                $html .= '</table>';
    
}
$html .= '
                <tr>
                <td width="12.5%" scope="col">
                <b>Total:</b>
                </td>
                <td width="37.5%"  field="total_amt" type="currency" class="inlineEdit">
                <span id="total_amt">
                <b>' . $currency_symbol . number_format($quote_entry->Result->name_value_list->total_amt->value, 2) . '</b>
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <tr>
                <td width="12.5%" scope="col">
                <b>Discount:</b>
                </td>
                <td width="37.5%"  field="discount_amount" type="currency" class="inlineEdit">
                <span id="discount_amount">
                <b>' . $currency_symbol . number_format($quote_entry->Result->name_value_list->discount_amount->value, 2) . '</b>
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <tr>
                <td width="12.5%" scope="col">
                <b>Subtotal:</b>
                </td>
                <td width="37.5%"  field="subtotal_amount" type="currency" class="inlineEdit">
                <span id="subtotal_amount">
                <b>' . $currency_symbol . number_format($quote_entry->Result->name_value_list->subtotal_amount->value, 2) . '</b>
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <tr>
                <td width="12.5%" scope="col">
                <b>Shipping:</b>
                </td>
                <td width="37.5%"  field="shipping_amount" type="currency" class="inlineEdit">
                <span id="shipping_amount">
                <b>' . $currency_symbol . number_format($quote_entry->Result->name_value_list->shipping_amount->value, 2) . '</b>
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <tr>
                <td width="12.5%" scope="col">
                <b>Shipping Tax:</b>
                </td>
                <td width="37.5%"  field="shipping_tax_amt" type="currency" class="inlineEdit">
                <span id="shipping_tax_amt_span">
                <b>' . $currency_symbol . number_format($quote_entry->Result->name_value_list->shipping_tax_amt->value, 2) . '</b>
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <tr>
                <td width="12.5%" scope="col">
                <b>Tax:</b>
                </td>
                <td width="37.5%"  field="tax_amount" type="currency" class="inlineEdit">
                <span id="tax_amount">
                <b>' . $currency_symbol . number_format($quote_entry->Result->name_value_list->tax_amount->value, 2) . '</b>
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <tr>
                <td width="12.5%" scope="col">
                <b>Grand Total:</b>
                </td>
                <td width="37.5%"  field="total_amount" type="currency" class="inlineEdit">
                <span id="total_amount">
                <b>' . $currency_symbol . number_format($quote_entry->Result->name_value_list->total_amount->value, 2) . '</b>
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <div class="inlineEditIcon"> </div>';
if ($pro_budle_cnt >= 0) {
                $html .= '</td>
                </tr>';
}
                $html .= '</table>
    </div>

</div>
</div>';
?>