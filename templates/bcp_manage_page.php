<?php
if (isset($_REQUEST['conerror']) && !empty($_REQUEST['conerror'])) {
    if (isset($_COOKIE['bcp_connection_error']) && $_COOKIE['bcp_connection_error'] != '') {
        $cookie_err = $_COOKIE['bcp_connection_error'];
        unset($_COOKIE['bcp_login_error']);
        return "<div class='error settings-error' id='setting-error-settings_updated'> 
            <p><strong>$cookie_err</strong></p>
        </div>";
    }
}
//for dashbard gui page
$biztech_redirect_dashboard = get_page_link(get_option('biztech_redirect_dashboard_gui'));
if ($biztech_redirect_dashboard != NULL) {
    $redirect_url_dashboard = $biztech_redirect_dashboard;
} else {
    $redirect_url_dashboard = home_url() . "/portal-dashboard-gui/";
}
$biztech_redirect_profile = get_page_link(get_option('biztech_redirect_profile'));
if ($biztech_redirect_profile != NULL) {
    $redirect_url = $biztech_redirect_profile;
} else {
    $redirect_url = home_url() . "/portal-profile/";
}

$pagetemplate = get_post_meta(get_the_ID(), '_wp_page_template', true);
$template = str_replace('.php', "", $pagetemplate);
if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
    $list_result_count_all = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], 'accounts', array('id'), '', '', 0);
    $countAcc = count($list_result_count_all->entry_list);
}
if ($sugar_crm_version == 7) {
    $list_result_count_all = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], 'accounts', 'id', array(), '', '', 'date_entered:DESC');
    if (!empty($list_result_count_all->records)) {
        $countAcc = count($list_result_count_all->records);
    } else {
        $countAcc = 0;
    }
}
$theme_name = wp_get_theme(); //20-aug-2016
if ($theme_name == "Twenty Fifteen") {
    $article_class_gen = "article-fifteen-theme";
}
if ($theme_name == "Twenty Sixteen") {
    $article_class_gen = "article-sixteen-theme";
}


reset($modules);
$first_element = key($modules);

if ($template != 'page-crm-standalone') {
    ?>
    <header class="entry-header entry-wrapper">
        <div class="container"> 
            <?php if ($template != 'page-crm-standalone') {
                ?>
                <a href='javascript:void(0)' class=""><h1 class="entry-title scp-menu-modules"><?php
                        if (get_option('biztech_scp_name') != NULL) {
                            _e(get_option('biztech_scp_name'));
                        } else {
                            _e("All Modules");
                        }
                        ?></h1></a>

                <div class="scp-leftpanel">
                    <ul class="scp-sidemenu scp-open-modules-menu">
                        <?php
                        $mlist = array();
                        foreach ($modules as $key => $values) {
                            ?>
                            <li id="<?php echo $key; ?>_id" class="scp-sidebar-class">
                                <a class="label"> <span class="fa <?php
                                    echo $key;
                                    ?> side-icon-wrapper scp-<?php echo $key; ?> scp-default-bg scp-default-background"></span> &nbsp;<span class="menu-text"><?php echo $values; ?></span><span class="fa arrow"></span></a> <ul  id="dropdown_<?php echo $key; ?>_id" class="inner_ul">
                                                        <?php if (($key == 'Accounts' && $countAcc > 0) || $key == 'Quotes' || $key == 'Invoices' || $key == 'Contracts' || $key == 'Knowledge Base' || $key == 'AOS_Quotes' || $key == 'AOS_Invoices' || $key == 'AOS_Contracts' || $key == 'AOK_KnowledgeBase' || $key == 'KBDocuments') { ?>
                                                            <?php
                                                        } else {
                                                            ?>
                                        <li style="list-style: none;" id="edit-<?php echo $key; ?>"><a href="javascript:void(0)"><span class=""></span><span><?php _e('Add ' . $values); ?></span></a></li>
                                    <?php } ?>
                                    <li style="list-style: none;" id="list-<?php echo $key ?>" ><a class='no-toggle' href="javascript:void(0)"><span></span><span><?php _e('View ' . $values); ?></span></a></li>
                                </ul>

                            </li>
                        <?php 
                        $mlist[] = $key;    
                        }
                        if ((in_array('Calls', $mlist)) || in_array('Meetings', $mlist) || in_array('Tasks', $mlist)) {
                        ?>
                        <li id="calendar_id" class="scp-sidebar-class">
                            <a class="label"> <span class="fa fa-calendar side-icon-wrapper scp-Calendar"></span>&nbsp;<span class="menu-text">Calendar</span><span class="fa arrow"></span> </a> <ul  id="dropdown_calendar_id" class="inner_ul">
                                <li style="list-style: none;" id="view-calendar"><a href="javascript:void(0)">View Calendar</a></li>
                            </ul>
                        </li>
                        <?php } ?>
                    </ul></div><?php
            } else {
                ?><a href='javascript:void(0)' ><h1 class="entry-title"><?php _e("All Modules"); ?></h1></a><?php
                }
                ?>
            <div class='userinfo'>
                <a href='javascript:void(0)' class="scp-menu-manage"><?php echo $_SESSION['scp_user_account_name']; ?> </a> <ul class="scp-open-manage-menu"><li><a class='fa fa-user' href='<?php echo $redirect_url; ?>'> <?php _e("My Profile"); ?></a></li><li><a class='fa fa-tachometer side-icon-wrapper'  href='<?php echo $redirect_url_dashboard; ?>'> <?php _e("Dashboard"); ?></a></li><li><a class='fa fa-power-off'  href='?logout=true'> <?php _e("Log Out"); ?></a></li></ul>
            </div>
        </div>
    </header>
<?php } ?>
<article class = "page type-page status-publish hentry container">

    <div class="entry-content entry-wrapper scp-wrapper-bg">
        <?php if ($template == 'page-crm-standalone') { ?>
            <span id="toggle"><?php echo get_option('biztech_scp_portal_menu_title'); ?><span class="fa arrow"></span></span>
        <?php } ?>
        <div class="scp-leftpanel">
            <?php if ($template == 'page-crm-standalone') { ?>
                <ul class="scp-sidemenu">
                    <li class="scp-menu-profile-sec"> <?php
                        $current_user = wp_get_current_user();

                        if (($current_user instanceof WP_User)) {
                            echo "<div class='scp-user-img'>" . get_avatar($current_user->user_email, 51) . "</div>";
                        }
                        ?><span class="scp-menu-manage username"> <a href='<?php echo $redirect_url; ?>'><?php echo $_SESSION['scp_user_account_name']; ?> </a></span>
                        <a href='<?php echo $redirect_url; ?>' class="fa fa-pencil scp-profile-edit"></a>
                        <ul><li><a class='fa fa-power-off scp-profile-logout'  href='?logout=true'> <?php _e("Log Out"); ?></a></li>
                            <li><a class='fa fa-tachometer side-icon-wrapper scp-profile-dashboard'  href='<?php echo $redirect_url_dashboard; ?>'> <?php _e("Dashboard"); ?></a></li></ul>
                    </li>
                    <?php
                    $mlist = array();
                    foreach ($modules as $key => $values) {
                        ?>
                        <li id="<?php echo $key; ?>_id" class="scp-sidebar-class">
                            <a class="label"> <span class="fa <?php echo $key; ?> side-icon-wrapper"></span> &nbsp;<span class="menu-text"><?php echo $values; ?></span><span class="fa arrow"></span></a>
                            <ul style="display: none;" id="dropdown_<?php echo $key; ?>_id" class="inner_ul">
                                <?php if (($key == 'Accounts' && $countAcc > 0) || $key == 'Quotes' || $key == 'Invoices' || $key == 'Contracts' || $key == 'Knowledge Base' || $key == 'AOS_Quotes' || $key == 'AOS_Invoices' || $key == 'AOS_Contracts' || $key == 'AOK_KnowledgeBase' || $key == 'KBDocuments') { ?>
                                <?php } else { ?>
                                    <li style="list-style: none;" id="edit-<?php echo $key; ?>"><a href="javascript:void(0)"><span class="fa fa-plus side-icon-wrapper"></span><span>Add</span></a></li>
                                <?php } ?>
                                <li style="list-style: none;" id="list-<?php echo $key ?>" ><a class='no-toggle' href="javascript:void(0)"><span class="fa fa-list side-icon-wrapper"></span><span>View</span></a></li>
                            </ul>
                        </li>
                    <?php 
                    $mlist[] = $key;
                    }
                    if ((in_array('Calls', $mlist)) || in_array('Meetings', $mlist) || in_array('Tasks', $mlist)) {
                    ?>
                    <li id="calendar_id" class="scp-sidebar-class">
                        <a class="label"> <span class="fa fa-calendar side-icon-wrapper"></span>&nbsp;<span class="menu-text">Calendar</span><span class="fa fa arrow"></span> </a>
                        <ul style="display: none;"   id="dropdown_calendar_id" class="inner_ul">
                            <li style="list-style: none;" id="view-calendar"><a href="javascript:void(0)"><span><span class="fa fa-list side-icon-wrapper"></span>View</span></a></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </div>
        <div class='scp-entry-header scp-page-list-view'>
            <?php
            if ($template == 'page-crm-standalone') {
                ?>
                <header class="entry-header entry-wrapper">
                    <div class="container"> 
                        <a href='javascript:void(0)' class='scp-manage-page-header-sec'><?php if (get_option('biztech_scp_name') != NULL) { ?>
                                <span class='fa fa-bars scp-bar-toggle'></span><h1 class="entry-title"><?php echo get_option('biztech_scp_name'); ?></h1>
                            <?php } else { ?><span class='fa fa-bars scp-bar-toggle'></span><h1 class="entry-title"><?php _e("All Modules"); ?></h1><?php } ?></a>
                    </div>
                </header>
            <?php } ?>
            <div id="responsedata" class='scp-standalone-content'></div>
            <div id="otherdata" style="display:none;"></div>
        </div>
    </div>
</article>




<script type="text/javascript">

    function bcp_module_order_by(page, modulename, order_by, order, view, get_current_url) { // for order by
        var searchval = jQuery('#search_name').val();
        var el = jQuery(".scp-entry-header");
        App.blockUI(el);
        var data = {
            'action': 'bcp_list_module',
            'view': view,
            'modulename': modulename,
            'page_no': page,
            'current_url': get_current_url,
            'order_by': order_by,
            'order': order,
            'searchval': searchval,
        };
        jQuery.post(ajaxurl, data, function (response) {
            App.unblockUI(el);
            if ($.trim(response) != '-1') {
                jQuery('#responsedata').html(response);
                // jQuery('#succid').hide();
            }
            else
            {
                var redirect_url = $(location).attr('href') + "?conerror=1";
                window.location.href = redirect_url;
            }
        });
    }

    function bcp_module_paging(page, modulename, ifsearch, order_by, order, view, get_current_url) { // for paging
        var searchval = jQuery('#search_name').val();
        var AOK_Knowledge_Base_Categories = jQuery('#AOK_Knowledge_Base_Categories').val();
        if (ifsearch == 1 && (searchval.trim() == '' || searchval == null) && modulename != 'AOK_KnowledgeBase') {
            alert('Please enter valid data to search');
            return false;
        }
        if (ifsearch == 1 && modulename == 'AOK_KnowledgeBase' && AOK_Knowledge_Base_Categories == '' && (searchval.trim() == '' || searchval == null)) {
            alert('Please Enter Name OR Select Category');
            return false;
        }
        var el = jQuery(".scp-entry-header");
        App.blockUI(el);
        var data = {
            'action': 'bcp_list_module',
            'view': view,
            'modulename': modulename,
            'page_no': page,
            'current_url': get_current_url,
            'order_by': order_by,
            'order': order,
            'searchval': searchval,
            'AOK_Knowledge_Base_Categories': AOK_Knowledge_Base_Categories
        };
        jQuery.post(ajaxurl, data, function (response) {
            App.unblockUI(el);
            if ($.trim(response) != '-1') {
                jQuery('#responsedata').html(response);
                // jQuery('#succid').hide();
            }
            else
            {
                var redirect_url = $(location).attr('href') + "?conerror=1";
                window.location.href = redirect_url;
            }
        });
    }

    function bcp_clear_search_txtbox(page, modulename, ifsearch, order_by, order, view, get_current_url) { //for clear search text box
        jQuery('#otherdata').hide();
        jQuery(this).parents('form').find('input[type="text"]').val('');
        var el = jQuery(".scp-entry-header");
        App.blockUI(el);
        var data = {
            'action': 'bcp_list_module',
            'view': view,
            'modulename': modulename,
            'page_no': page,
            'current_url': get_current_url,
            'order_by': order_by,
            'order': order,
        };
        jQuery.post(ajaxurl, data, function (response) {
            App.unblockUI(el);
            if ($.trim(response) != '-1') {
                jQuery('#responsedata').html(response);
                // jQuery('#succid').hide();
            }
            else
            {
                var redirect_url = $(location).attr('href') + "?conerror=1";
                window.location.href = redirect_url;
            }
        });

    }
    $(document).ready(function () {

        if (jQuery('.datetimepicker').length > 0) {
            // Add any datepickers to all fields with the class
            jQuery('.datetimepicker').datetimepicker();
        }
        var curURL = window.location.href;
        //for display default account page
        if (curURL.indexOf("?") < 0) {//Updated by BC on 04-jun-2016 for default module list shown dynamically
            var first_element = '<?php echo $first_element; ?>';
            //var first_element_without_s = '<?php echo strtolower(rtrim($first_element, 's')); ?>';
            bcp_common_call("list-" + first_element);
            // $(first_element_without_s + '#_id').addClass('noborder');
            // $('#dropdown_' + first_element + '_id').css('display', 'block');
            //$('#list-' + first_element + '  a').addClass('scp-active-submenu');
        }
        //for display listing page after record insert 
        var param = curURL.split("?");
        if (param[1] != '' && param[1] != null && param[1] != 'undefined') {
            sucmsg = 1;
            bcp_common_call(param[1], sucmsg);
        }
        $(".inner_ul> li").click(function ()
        {
            var divId = $(this).attr('id');
            sucmsg = 0;
            //$('.no-toggle').removeAttr('style');
            // $('a').removeClass('scp-active-submenu');
            //$('#' + divId + ' a').addClass('scp-active-submenu');
            bcp_common_call(divId, sucmsg);
        });
        function bcp_common_call(divId, sucmsg) {
            jQuery('#otherdata').hide();
            var getdata = divId.split("-");
            var view = getdata[0];
            var modulename = getdata[1];
            var get_current_url = "<?php echo $_SERVER['REQUEST_URI']; ?>"
            var order_by = '';
            var order = '';
            var el = jQuery(".scp-entry-header");
            if (modulename.indexOf("&") >= 0) { // in calnder,after click go to detail page
                getmodulename = modulename.split("&");
                modulename = getmodulename[0];

                if (divId.indexOf("detail") >= 0) {// display selected in module list
                    setview = "list";
                    //modulename1 = modulename.toLowerCase();
                    modulename1 = modulename;
                    //$('#' + modulename1 + '_id').addClass('noborder');
                    // $('#dropdown_' + modulename1 + '_id').css('display', 'block');
                    // $('#list-' + modulename + '  a').addClass('scp-active-submenu');
                    // $("#" + modulename1 + "_id a.label").addClass('scp-active-menu');
                }
            }
            if (divId.indexOf("list") >= 0) {// display selected in module list afetr record added
                //modulename1 = modulename.toLowerCase();
                modulename1 = modulename;
                // $('#' + modulename1 + '_id').addClass('noborder');
                // $('#dropdown_' + modulename1 + '_id').css('display', 'block');
                //$('#list-' + modulename + '  a').addClass('scp-active-submenu');
                //$("#" + modulename1 + "_id a.label").addClass('scp-active-menu');
            }
            App.blockUI(el);
            if (view == 'edit') {
                var data = {
                    'action': 'bcp_add_module',
                    'view': view,
                    'modulename': modulename,
                    'id': "<?php
            if (isset($_REQUEST['id'])) {
                echo $_REQUEST['id'];
            }
            ?>",
                    'current_url': get_current_url
                };
            }
            if (view == 'view') {
                if (modulename == 'calendar') { // if calender module then call other page
                    var data = {
                        'action': 'bcp_calendar_display',
                    };
                } else {
                    var data = {
                        'action': 'bcp_view_module',
                        'view': view,
                        'modulename': modulename,
                        'id': "<?php
            if (isset($_REQUEST['id'])) {
                echo $_REQUEST['id'];
            }
            ?>",
                        'current_url': get_current_url
                    };
                }
            }
            if (view == 'list') {
                var data = {
                    'action': 'bcp_list_module',
                    'view': view,
                    'modulename': modulename,
                    'page_no': 0,
                    'current_url': get_current_url,
                    'order_by': order_by,
                    'order': order
                };
            }
            if (view == 'detail') {
                var data = {
                    'action': 'bcp_view_module',
                    'view': view,
                    'modulename': modulename,
                    'id': "<?php
            if (isset($_REQUEST['id'])) {
                echo $_REQUEST['id'];
            }
            ?>",
                    'current_url': get_current_url

                };
            }
            jQuery.post(ajaxurl, data, function (response) {
                App.unblockUI(el);
                if ($.trim(response) != '-1') {
                    jQuery('#responsedata').html(response);
                    if (sucmsg == 1) {
                        jQuery('#succid').show(); //display success msg of updated/added/deleted
                    } else {
                        jQuery('#succid').hide();
                    }
                }
                else
                {
                    var redirect_url = $(location).attr('href') + "?conerror=1";
                    window.location.href = redirect_url;
                }
            });
        }
    });
    function bcp_module_call_view(modulename, id, view, curURL) { // for view particular record(detail page)
        var el = jQuery(".scp-entry-header");
        App.blockUI(el);
        var data = {
            'action': 'bcp_view_module',
            'view': view,
            'modulename': modulename,
            'id': id,
            'current_url': curURL
        };
        jQuery.post(ajaxurl, data, function (response) {
            App.unblockUI(el);
            if ($.trim(response) != '-1') {
                jQuery('#responsedata').html(response);
                jQuery('#succid').hide();

            }
            else
            {
                var redirect_url = $(location).attr('href') + "?conerror=1";
                window.location.href = redirect_url;
            }
        });
    }

    function bcp_module_call_add(htmlval, modulename, view, success, deleted, id) { // for add record layout in module
        htmlval = (typeof htmlval === 'undefined') ? 0 : htmlval;
        success = (typeof success === 'undefined') ? null : success;
        deleted = (typeof deleted === 'undefined') ? null : deleted;
        id = (typeof id === 'undefined') ? null : id;
        var get_current_url = "<?php echo $_SERVER['REQUEST_URI']; ?>"
        var el = jQuery(".scp-entry-header");
        /* Added for working edit from calendar detail page */
        var split_arry = get_current_url.split('?');
        split_arry[1] = (typeof split_arry[1] === 'undefined') ? null : split_arry[1];
        if (split_arry[1] != null) {
            var splited_url_var = split_arry[1].split('-')[0];
            if (splited_url_var == 'detail') {
                get_current_url = split_arry[0] + "?list-" + modulename;
            }
        }
        App.blockUI(el);
        var data = {
            'action': 'bcp_add_module',
            'view': view,
            'modulename': modulename,
            'success': success,
            'deleted': deleted,
            'id': id,
            'current_url': get_current_url

        };
        jQuery.post(ajaxurl, data, function (response) {
            App.unblockUI(el);
            if ($.trim(response) != '-1') {
                if (htmlval == 0) {
                    jQuery('#otherdata').hide();
                    jQuery('#responsedata').html(response);
                    jQuery('#succid').hide();
                }
                else {
                    $('#otherdata').show();
                    jQuery('#otherdata').html(response);
                    jQuery('#succid').hide();
                }
                $("#otherdata").appendTo("#responsedata");
            }
            else
            {
                var redirect_url = $(location).attr('href') + "?conerror=1";
                window.location.href = redirect_url;
            }
        });
    }

    function form_submit_note_document(id) {
        jQuery('#scp_note_id').val(id);
        document.getElementById("download_note_id").submit();
        return false;
    }
    function form_submit_document(id) {
        jQuery('#scp_doc_id').val(id);
        document.getElementById("doc_submit").submit();
        return false;
    }
    //Added by BC on 22-jun-2016 for kbdocument attachment
    function form_submit_KBdocument(id) {
        jQuery('#scp_kbdoc_id').val(id);
        document.getElementById("kbdoc_submit").submit();
        return false;
    }

    function form_submit(modulename, view, doc_id, deleted, curURL) {
        jQuery('#scp_id').val(doc_id);
        if (confirm('Are you sure you want to delete the record?')) {
            var el = jQuery(".scp-entry-header");
            App.blockUI(el);
            var data = {
                'action': 'prefix_admin_bcp_add_moduledata_call',
                'view': view,
                'module_name': modulename,
                'current_url': curURL,
                'id': doc_id,
                'delete': deleted
            };
            jQuery.post(ajaxurl, data, function (response, status) {
                App.unblockUI(el);
                if ($.trim(response) != '-1') {
                    redirect_url = response;
                    window.location.href = redirect_url;
                }
                else
                {
                    var redirect_url = $(location).attr('href') + "?conerror=1";
                    window.location.href = redirect_url;
                }
            });
            // document.getElementById("actionform").submit();
            return false;
        } else {
            return false;
        }
    }
    function chek(id) {//Added by BC on 07-nov-2015
        if (jQuery('#' + id).is(':checked')) {
            jQuery('#' + id).val(1);
            jQuery("#" + id).prop('checked', true);
        } else {
            jQuery('#' + id).val(0);
            jQuery('input[name="do_not_call"]').removeAttr('checked');
        }
    }
    function get_module_list()
    {
        var p_type = jQuery('#parent_name').val();
        data = {action: 'get_module_list', parent_type: p_type};
        jQuery.post(ajaxurl, data, function (response) {
            jQuery('#related_to_id2').html(response);
        });
    }
    function get_module_list_v7()
    {
        var p_type = jQuery('#parent_name').val();
        data = {action: 'get_module_list_v7', parent_name: p_type};
        jQuery.post(ajaxurl, data, function (response) {
            jQuery('#related_to_id2').html(response);
        });
    }

    function bcp_module_add_casenote(htmlval, modulename, view, casenote) { // for add record layout in module
        htmlval = (typeof htmlval === 'undefined') ? 0 : htmlval;
        success = (typeof success === 'undefined') ? null : success;
        deleted = (typeof deleted === 'undefined') ? null : deleted;
        casenote = (typeof htmlval === 'undefined') ? null : casenote;
        id = (typeof id === 'undefined') ? null : id;
        var get_current_url = "<?php echo $_SERVER['REQUEST_URI']; ?>"
        var el = jQuery(".scp-entry-header");
        App.blockUI(el);
        var data = {
            'action': 'bcp_add_module',
            'view': view,
            'modulename': modulename,
            'current_url': get_current_url,
            'casenote': casenote

        };
        jQuery.post(ajaxurl, data, function (response) {
            App.unblockUI(el);
            if ($.trim(response) != '-1') {
                if (htmlval == 0) {
                    jQuery('#responsedata').html(response);
                    jQuery('#succid').hide();
                }
                else {
                    $('#otherdata').show();
                    jQuery('#otherdata').html(response);
                    jQuery('#succid').hide();
                }
                $("#otherdata").appendTo("#responsedata");
            }
            else
            {
                var redirect_url = $(location).attr('href') + "?conerror=1";
                window.location.href = redirect_url;
            }
        });
    }
//Added by BC on 24-jun-2016 for Case Updates
//Submit case comment form
    function case_updates_form_submit(id) {
        var update_text = $.trim(jQuery('#update_text').val());
        if (!(update_text)) {
            alert('Please enter your comments.');
            return false;
        } else {
            var el = jQuery("#case-updates");
            App.blockUI(el);
            var data = {
                'action': 'prefix_admin_bcp_case_updates',
                'id': id,
                'update_text': update_text,
            };
            jQuery.post(ajaxurl, data, function (response, status) {
                App.unblockUI(el);
                if ($.trim(response) != '-1') {
                    jQuery('#case-updates').html(response);
                }
                else
                {
                    var redirect_url = $(location).attr('href') + "?conerror=1";
                    window.location.href = redirect_url;
                }
            });
            return false;
        }
    }
</script>



