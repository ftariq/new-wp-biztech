<?php
if (isset($_REQUEST['conerror']) && !empty($_REQUEST['conerror'])) {
    if (isset($_COOKIE['bcp_connection_error']) && $_COOKIE['bcp_connection_error'] != '') {
        $cookie_err = $_COOKIE['bcp_connection_error'];
        unset($_COOKIE['bcp_login_error']);
        return "<div class='error settings-error' id='setting-error-settings_updated'> 
            <p><strong>$cookie_err</strong></p>
        </div>";
    }
}
$biztech_redirect_profile = get_page_link(get_option('biztech_redirect_profile'));
if ($biztech_redirect_profile != NULL) {
    $redirect_url = $biztech_redirect_profile;
} else {
    $redirect_url = home_url() . "/portal-profile/";
}
$biztech_redirect_manage_page = get_page_link(get_option('biztech_redirect_manange'));
if ($biztech_redirect_manage_page != NULL) {
    $redirect_url_manage = $biztech_redirect_manage_page;
} else {
    $redirect_url_manage = home_url() . "/portal-manage-page/";
}
$pagetemplate = get_post_meta(get_the_ID(), '_wp_page_template', true);
$template = str_replace('.php', "", $pagetemplate);
?>

<header class="entry-header entry-wrapper">
    <div class="container"> 
        <?php if (get_option('biztech_scp_name') != NULL) { ?>
            <h1 class="entry-title"><?php echo get_option('biztech_scp_name'); ?></h1>
        <?php } else { ?>
            <h1 class="entry-title"><?php _e("All Modules"); ?></h1>
        <?php } ?>
        <div class='userinfo'>
            <a href='javascript:void(0)' class="scp-menu-dashboard"><?php echo $_SESSION['scp_user_account_name']; ?> </a> <ul class="scp-open-dashboard-menu"><li><a class='fa fa-user' href='<?php echo $redirect_url; ?>'> <?php _e("My Profile"); ?></a></li><li><a class='fa fa-bars side-icon-wrapper'  href='<?php echo $redirect_url_manage; ?>'> <?php _e("Manage Page"); ?></a></li><li><a class='fa fa-power-off'  href='?logout=true'> <?php _e("Log Out"); ?></a></li></ul>
        </div>
    </div>
</header>
<article class = "page type-page status-publish hentry container">
    <div class="dashboard-container">
        <div class="entry-content entry-wrapper dashboard-box">

            <?php
            $m_count = 1;
//get option to redirect to which page after login
            $biztech_redirect_manage_dash = get_page_link(get_option('biztech_redirect_manange'));
            if ($biztech_redirect_manage_dash != NULL) {
                $redirect_url_dash = $biztech_redirect_manage_dash;
            } else {
                $redirect_url_dash = home_url() . "/portal-manage-page/";
            }
            $mlist = array();
            foreach ($modules as $key => $values) {
                $module_without_s = strtolower(rtrim($values, 's'));
                ?>
                <a href="<?php echo $redirect_url_dash . '?list-' . $key; ?>">
                    <div class="module-box scp-<?php echo $key; ?> <?php
                    if (($m_count % 5 == 0) && $m_count != 1) {
                        echo 'last';
                    }
                    ?>">
                        <span class="fa <?php
                        echo $key;
                        ?> module-icon side-icon-wrapper"></span>
                        <h4><?php echo $values; ?></h4>
                    </div>
                </a>
                <?php
                $m_count++;
                $mlist[] = $key;
            }
            if ((in_array('Calls', $mlist)) || in_array('Meetings', $mlist) || in_array('Tasks', $mlist)) {
                ?>
                <a href="<?php echo $redirect_url_dash . '?view-calendar'; ?>">
                    <div class="module-box scp-Calendar last" >
                        <span class="fa fa-calendar module-icon side-icon-wrapper"></span>
                        <h4>Calendar</h4>
                    </div>
                </a>
            <?php } ?>
            <div class='scp-entry-header'>
                <?php
                $get_param = $_SERVER['QUERY_STRING'];
                if (isset($get_param) && !empty($get_param)) {
                    if (strpos($get_param, 'list') === false) {
                        
                    } else {
                        if (isset($_COOKIE['bcp_add_record']) && $_COOKIE['bcp_add_record'] != '') {
                            $cookie_err = $_COOKIE['bcp_add_record'];
                            unset($_COOKIE['bcp_add_record']);
                            echo "<span class='success' id='succid'>" . $cookie_err . "</span>";
                        }
                    }
                }
                ?>
                <div id="responsedata"></div>
                <div id="otherdata" style="display:none;"></div>
            </div>
        </div>
        <?php
        global $objSCP, $sugar_crm_version;
        $recent_activity_modules = get_option('biztech_scp_recent_activity');
        if (!empty($recent_activity_modules)) {
            ?> <div id="bcp_recent_activities"  class="scp_recent_activities"><?php
            if ($sugar_crm_version == 7) {
                $res_count = 1;
                $where_con = array();
                $recent_activity_modules = get_option('biztech_scp_recent_activity');
                foreach ($recent_activity_modules as $key => $module_name) {
                    if (!array_key_exists($module_name, (array) $modules)) {//those recent tabs shown which are in assign modules to user
                        continue;
                    }
                    if (is_array((array) $_SESSION['module_array'])) {//Added for getting module key OR lable added from sugar side
                        $arry_lable = (array) $_SESSION['module_array'];
                        $module_name_label = $arry_lable[$module_name];
                    }
                    $n_array_str = 'name,date_entered';
                    $order_by_query = "date_entered:DESC";
                    $limit = 5;
                    $offset = 0;
                    if ($module_name == 'KBDocuments') {//Added by BC on 22-jun-2016 for KBDocument
                        if (isset($_SESSION['sugar_version']) && $_SESSION['sugar_version'] == '7.7.0.0') {//if sugar 7 version 7.7
                            $list_result = $objSCP->get_entry_list_custom('KBContents', '', $where_con, $limit, $offset, $order_by_query);
                        } else {
                            $n_array_str = 'id,kbdocument_name,date_entered,description';
                            $list_result = $objSCP->get_entry_list_custom($module_name, $n_array_str, $where_con, $limit, $offset, $order_by_query);
                        }
                    } else {
                        $list_result = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $n_array_str, $where_con, $limit, $offset, $order_by_query);
                    }

                    $cnt = count($list_result->records);
                    ?>
                        <div class="activity-tab <?php
                        if (($res_count % 2 == 0) && $res_count != 1) {
                            echo 'last';
                        }
                        ?>"><h5 class = "fa <?php echo $module_name; ?> scp-<?php echo $module_name; ?> side-icon-wrapper activity-tab-heading scp-default-background"><?php _e("Recent $module_name_label"); ?></h5>
                             <?php
                             if ($cnt > 0) {
                                 if ($module_name == 'KBDocuments') {
                                     $list_res_kb = $list_result->records;
                                     foreach ($list_res_kb as $list_result_s) {
                                         if (isset($_SESSION['sugar_version']) && $_SESSION['sugar_version'] == '7.7.0.0') {//if sugar 7 version 7.7
                                            $name = $list_result_s->name;
                                         }else{
                                            $name = $list_result_s->kbdocument_name;
                                         }
                                         $kb_created_date_origional = $list_result_s->date_entered;
                                         $value_date = date('d-M-Y', strtotime($kb_created_date_origional));
                                         ?>
                                        <div class = "scp-recent-data"> <span ><?php echo $name;
                                         ?></span>
                                            <span class="scp-activity-date"><?php echo $value_date; ?></span></div>
                                        <?php
                                    }
                                } else {
                                    foreach ($list_result->records as $list_result_s) {

                                        //$id = $list_result_s->id;
                                        $name = $list_result_s->name;
                                        $dateval = $list_result_s->date_entered;
                                        if ($dateval != '') { // aaded to display blank date instead of default date
                                            if (empty($_SESSION['user_timezone'])) {
                                                $result_timezone = $objSCP->getUserTimezone();
                                            } else {
                                                $result_timezone = $_SESSION['user_timezone'];
                                            }
                                            date_default_timezone_set($result_timezone->timezone);
                                            $result_timezone = date_default_timezone_get();
                                            $UTC = new DateTimeZone("UTC");
                                            $newTZ = new DateTimeZone($result_timezone);
                                            $date = new DateTime($dateval, $UTC);
                                            $date->setTimezone($newTZ);
                                            $date_format = $_SESSION['user_date_format'];
                                            $time_format = $_SESSION['user_time_format'];
                                            $value_date = $date->format('d-M-Y');
                                        }
                                        ?> 

                                        <div class="scp-recent-data"> <span ><?php echo $name; ?></span>
                                            <span class="scp-activity-date"><?php echo $value_date; ?></span></div>
                                        <?php
                                    }
                                }
                                ?><div class="scp-recent-data scp-view-notification scp-<?php echo $module_name; ?>-font scp-default-font">  <a href="<?php echo $redirect_url_dash . '?list-' . $module_name; ?>"> <?php _e("View All " . $module_name_label); ?></a></div><?php } else {
                                ?>
                                <div class="scp-recent-data"> <span><?php _e("No Records"); ?></span></div>
                                <?php
                            }
                            $res_count++;
                            ?> </div><?php
                    }
                }
                if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
                    $res_count = 1;
                    $where_con = '';
                    foreach ($recent_activity_modules as $key => $module_name) {
                        if (!array_key_exists($module_name, (array) $modules)) {//those recent tabs shown which are in assign modules to user
                            continue;
                        }
                        if (is_array((array) $_SESSION['module_array'])) {//Added for getting module key OR lable added from sugar side
                            $arry_lable = (array) $_SESSION['module_array'];
                            $module_name_label = $arry_lable[$module_name];
                        }
                        $n_array_str = array('name', 'date_entered');
                        $order_by_query = "date_entered DESC";
                        $limit = 5;
                        $offset = 0;
                        if ($module_name == 'AOK_KnowledgeBase') {//Added by BC on 19-may-2016
                            $n_array = array('id', 'name', 'date_entered', 'description', 'currency_id');
                            $list_result = $objSCP->get_entry_list($module_name, $where_con, $n_array, $order_by_query, 0, $limit, $offset);
                        } else {
                            $list_result = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $n_array_str, $where_con, $order_by_query, 0, $offset, $limit);
                        }
                        $cnt = count($list_result->entry_list);
                        ?> <div class="activity-tab <?php
                            if (($res_count % 2 == 0) && $res_count != 1) {
                                echo 'last';
                            }
                            ?>"><h5 class = "fa <?php echo $module_name; ?> scp-<?php echo $module_name; ?> side-icon-wrapper  activity-tab-heading scp-default-background"><?php _e("Recent $module_name_label"); ?></h5> <?php
                            if ($cnt > 0) {
                                ?>

                                <?php
                                foreach ($list_result->entry_list as $list_result_s) {

                                    //$id = $list_result_s->name_value_list->id->value;
                                    $name = $list_result_s->name_value_list->name->value;
                                    $dateval = $list_result_s->name_value_list->date_entered->value;
                                    if ($dateval != '') { // aaded to display blank date instead of default date
                                        if (empty($_SESSION['user_timezone'])) {
                                            $result_timezone = $objSCP->getUserTimezone();
                                        } else {
                                            $result_timezone = $_SESSION['user_timezone'];
                                        }
                                        $UTC = new DateTimeZone("UTC");
                                        $newTZ = new DateTimeZone($result_timezone);
                                        $date = new DateTime($dateval, $UTC);
                                        $date->setTimezone($newTZ);
                                        $value_date = $date->format('d-M-Y');
                                    }
                                    ?> 
                                    <div class="scp-recent-data">
                                        <span><?php echo $name; ?></span>
                                        <span  class="scp-activity-date"><?php echo $value_date; ?></span></div>
                                    <?php
                                }
                                ?><div class="scp-recent-data scp-view-notification scp-<?php echo $module_name; ?>-font scp-default-font">   <a href="<?php echo $redirect_url_dash . '?list-' . $module_name; ?>"> <?php _e("View All " . $module_name_label); ?></a></div><?php
                                } else {
                                    ?>
                                <div class="scp-recent-data">
                                    <span><?php _e("No Records"); ?></span></div>
                                <?php
                            }
                            $res_count++;
                            ?></div><?php
                        }
                    }
                    ?></div><?php
            }
            ?>

    </div>
</article>


