<?php

$html1 = "<tr class='row main-col'>";
foreach ($results as $key_name => $val_array) {
    if ($key_name == "SET_COMPLETE") {
        continue;
    }
    if (!empty($val_array->related_module)) {
        if ($val_array->type == 'relate' && ($val_array->related_module != 'Users' && !array_key_exists($val_array->related_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array 
            continue;
        }
    }
    $ar_val = strtolower($key_name);
    $name_arry[] = $ar_val;
    //make type array
    $type_array[$ar_val] = $val_array->type;
    $res2 = $objSCP->get_module_fields($module_name, array($ar_val));
    $fields_meta[$ar_val] = $res2;
    foreach ($res2->module_fields as $k_fileds => $fileds) {
        if ($k_fileds == "modified_user_id" || $k_fileds == "created_by" || ($k_fileds == "id" && $id_show == false)) {
            continue;
        }
        $field_label = str_replace(':', '', $fileds->label);
        $field_name = $fileds->name;
        if ($val_array->type == 'file') {//Front Side: 'Notes' Module: No need to place sorting on 'Attachment'
            $html1 .= "<th><a>" . $field_label . "</a></th>";
        } else {
            if (($order_by == $field_name) && ($order == 'desc')) {
                $html1 .= "<th><a href='javascript:void(0);' onclick='bcp_module_order_by(0,\"$module_name\",\"$field_name\",\"asc\",\"$view\",\"$current_url\");' class='scp-desc-sort'>" . $field_label . "</a></th>";
            } else if (($order_by == $field_name) && ($order == 'asc')) {
                $html1 .= "<th><a href='javascript:void(0);' onclick='bcp_module_order_by(0,\"$module_name\",\"$field_name\",\"desc\",\"$view\",\"$current_url\");' class='scp-asc-sort'>" . $field_label . "</a></th>";
            } else if ($field_label != '') {
                $html1 .= "<th><a href='javascript:void(0);' onclick='bcp_module_order_by(0,\"$module_name\",\"$field_name\",\"asc\",\"$view\",\"$current_url\");' class='scp-both-sort'>" . $field_label . "</a></th>";
            }
        }
    }
}
$html1 .= "<th class='action-th'><a>Action</a></th>";
$html1 .= "</tr>";
$currency_symbol = isset($currency_symbol) ? $currency_symbol : '';
foreach ($list_result->entry_list as $list_result_s) {
    $id = $list_result_s->name_value_list->id->value;
    if (isset($list_result_s->name_value_list->parent_type->value)) {
        $parent_type = $list_result_s->name_value_list->parent_type->value;
    }
    if ($module_name == 'AOS_Quotes' || $module_name == 'AOS_Invoices' || $module_name == 'AOS_Contracts') {//Added by BC on 02-july-2016 for currency symbol
        $curreny_id = $list_result_s->name_value_list->currency_id->value;
        if (!empty($curreny_id)) {
            $currency_symbol = $_SESSION['Currencies']->$curreny_id->symbol;
        }
    }
    $html1 .= "<tr>";
    foreach ($name_arry as $nkey => $nval) {
        switch ($nval) {
            case 'assigned_user_id':
                $get_entrty_module = "Users";
                break;
            case 'modified_user_id':
                $get_entrty_module = "Users";
                break;
            case 'created_by':
                $get_entrty_module = "Users";
                break;
            case 'parent_id':
                if ($parent_type) {
                    $get_entrty_module = $parent_type;
                } else {
                    $get_entrty_module = "";
                }
                break;
            default:
                $get_entrty_module = "";
                break;
        }
        $hlink_st = $hlink_en = $cls = '';

        $value = $list_result_s->name_value_list->$nval->value;

        if ($nval == "filename") {
            if (!empty($value)) {//Added by BC on 08-jul-2016
                $cls = "general-link-btn scp-download-btn";

                if ($module_name == "Notes") {
                    $hlink_st = "<a href='javascript:void(0);' onclick='form_submit_note_document(\"$id\");' class='kb-attachment $cls scp-$module_name-font'>";
                    $hlink_en = "</a>";
                }
                if ($module_name == "Documents") {
                    $hlink_st = "<a href='javascript:void(0);' onclick='form_submit_document(\"$id\");' class='kb-attachment $cls scp-$module_name-font'>";
                    $hlink_en = "</a>";
                }
            }
        }
        if ($nval == "date_modified" || $nval == "date_start" || $nval == "date_end" || $nval == "date_entered" || $nval == "date_due") {
            if ($value != '') { // aaded to display blank date instead of default date
                $UTC = new DateTimeZone("UTC");
                $newTZ = new DateTimeZone($result_timezone);
                $date = new DateTime($value, $UTC);
                $date->setTimezone($newTZ);
                $value = $date->format($objSCP->date_format . " " . $objSCP->time_format);
            }
        }
        if ($type_array[$nval] == 'date') {
            $value = (!empty($value)) ? date($objSCP->date_format, strtotime($value)) : '';
        }
        if ($get_entrty_module) {
            $select_fields = array('name');
            $record_detail = $objSCP->get_entry($get_entrty_module, $value, $select_fields);
            if (isset($record_detail) && !empty($record_detail)) {
                $value = $record_detail->entry_list[0]->name_value_list->name->value;
            }
        }

        if ($fields_meta[$nval]->module_fields->$nval->type == 'enum' || $fields_meta[$nval]->module_fields->$nval->type == 'multienum' || $fields_meta[$nval]->module_fields->$nval->type == 'dynamicenum') {
            $value = (!empty($value)) ? $fields_meta[$nval]->module_fields->$nval->options->$value->value : '';
        }
        if ($fields_meta[$nval]->module_fields->$nval->type == 'currency' && $value != '') {//Added by BC on 02-jul-2016 for currency
            $value = $currency_symbol . number_format($value, 2);
        }
        if ($nval == "portal_flag" || $nval == "embed_flag" || $nval == "do_not_call" || $nval == "is_template") {
            if ($value == "1") {
                $value = "Yes";
            } else {
                $value = "No";
            }
        }
        if ($type_array[$nval] == 'url' && !empty($value)) {//for url
            if (strpos($value, 'http') === false) {
                $value = 'http://' . $value;
            }
            $hlink_st = "<a href='" . $value . "' target='_blank'>";
            $hlink_en = "</a>";
        }

        if ($value == '' || empty($value)) {
            $value = "-";
        }
        if ($nval != 'link') {
            if ($nval == "filename" && !empty($value) && $value != '-') {
                $html1 .="<td>" . $hlink_st . "<i class='fa fa-download' aria-hidden='true'></i> Download" . $hlink_en . "</td>";
            } else {
                $html1 .="<td>" . $hlink_st . $value . $hlink_en . "</td>";
            }
        }
    }
    $view = 'edit';
    $deleted = 1;
    if ($module_name != 'AOS_Quotes' && $module_name != 'AOS_Invoices' && $module_name != 'AOS_Contracts' && $module_name != 'AOK_KnowledgeBase') {
        $html1 .= "<td class='action edit'><a href='javascript:void(0);' onclick='bcp_module_call_add(0,\"$module_name\",\"$view\",\"\",\"\",\"$id\");'><span class='fa fa-pencil' title='Edit'></span></a>   <a href='javascript:void(0);' onclick='bcp_module_call_view(\"$module_name\",\"$id\",\"detail\",\"$current_url\");'><span class='fa fa-eye' title='View'></span></a><a href='javascript:void(0);' onclick='form_submit(\"$module_name\",\"$view\",\"$id\",\"$deleted\",\"$current_url\");'><span class='fa fa-trash-o' title='Delete'></span></a></td>";
    } else {
        $html1 .= "<td class='action edit'><a href='javascript:void(0);' onclick='bcp_module_call_view(\"$module_name\",\"$id\",\"detail\",\"$current_url\");'><span class='fa fa-eye' title='View'></span></a></td>";
    }
    $html1 .= "</tr>";
}
?>