<?php

//echo '<pre>';print_r($quote_entry);exit();
$pro_budle_cnt = count($quote_entry->Result->ProductBundle) - 1;
$html .= '<div class="scp-form scp-form-2-col">
    <div class="panel" id="detailpanel_3">
    <div class="scp-col-12 panel-title"><span class="panel_name">Line Items:</span></div>
    <div class="tbl-responsive">
        <table cellspacing="0" class="panelContainer" id="LBL_LINE_ITEMS">
                <tr>

                <td width="37.5%" colspan="2" field="line_items" type="function" class="inlineEdit first">';
foreach ($quote_entry->Result->ProductBundle as $k_pro => $v_pro) {
    $html .= '
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr class="group-quotes-wrapper">
                    <th scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL" colspan="2">Group Name:</th>
                    <td style="text-align: left;padding:2px;" colspan="7" class="tabDetailViewDL">' . $v_pro->name . '</td>
                </tr>
                <tr class="group-quotes-wrapper">
                <th scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL" colspan="2">Group Stage:</th>
                    <td style="text-align: left;padding:2px;" colspan="7" class="tabDetailViewDL">' . $v_pro->bundle_stage . '</td>
                </tr>
                ';
    $ct = 1;
    $ct_qty = $ct_pline = 1;
    foreach ($v_pro->Product as $k_line => $v_line) {
        if ($ct_qty == 1) {
            $html .= '<tr>
                    <th width="5%" scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL">&nbsp;</th>
                    <th width="10%" scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL">Quantity</th>
                    <th width="12%" scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL">Quoted Line Item</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Cost</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">List</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Unit Price</th>';
            if((number_format($v_line->discount_amount, 2) != 0.00)){
                    $html .= '<th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Total Discount</th>';
            }else{
                $html .= '<th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL"></th>';
            }
                $html .= '</tr>';
        }
        $ct_qty++;
        $html .= '<tr>';
        if($quote_entry->Result->name_value_list->show_line_nums->value == 1){
         $html .= '<td style="text-align: left; padding:2px;" class="tabDetailViewDF">' . $ct . '</td>';
        }else{
          $html .= '<td style="text-align: left; padding:2px;" class="tabDetailViewDF"></td>';  
        }
        $html .= '<td style="padding:2px;" class="tabDetailViewDF">' . intval($v_line->quantity) . '</td>
                    <td style="padding:2px;" class="tabDetailViewDF">' . $v_line->name . '</td>';
        $html .= '<td style="text-align: right; padding:2px;" class="tabDetailViewDF">' . $currency_symbol . number_format($v_line->cost_price, 2) . '</td>';
        $html .= '<td style="text-align: right; padding:2px;" class="tabDetailViewDF">' . $currency_symbol . number_format($v_line->list_price, 2) . '</td>';
        $html .= '<td style="text-align: right; padding:2px;" class="tabDetailViewDF">' . $currency_symbol . number_format($v_line->discount_price, 2) . '</td>';
        if((number_format($v_line->discount_amount, 2) != 0.00)){
            if($v_line->discount_select == 1){
                $html .= '<td style="text-align: right; padding:2px;" class="tabDetailViewDF">' . number_format($v_line->discount_amount, 2) . '%</td>';
            }else{
                $html .= '<td style="text-align: right; padding:2px;" class="tabDetailViewDF">' . $currency_symbol . number_format($v_line->discount_amount, 2) . '</td>';
            }
        }else{
            $html .= '<td style="text-align: right; padding:2px;" class="tabDetailViewDF"></td>';
        }
        $html .= '</tr>';
        $ct++;
    }
    if (count($v_pro->Comment_protduct_line) > 0) {
        foreach ($v_pro->Comment_protduct_line as $k_pline => $v_pline) {
            if ($ct_pline == 1 && count($v_pro->Product) == 0) {
                $html .= '<tr>
                    <th width="5%" scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL">&nbsp;</th>
                    <th width="10%" scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL">Quantity</th>
                    <th width="12%" scope="row" style="text-align: left;padding:2px;" class="tabDetailViewDL">Quoted Line Item</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Cost</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">List</th>
                    <th width="12%" scope="row" style="text-align: right;padding:2px;" class="tabDetailViewDL">Unit Price</th>
                </tr>';
            }
            $html .= '<tr>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td>' . $v_pline->description . '</td>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '<td></td>';
            $html .= '</tr>';
            $ct_pline++;
        }
    }
    $html .= '<tr>
                    <td nowrap="nowrap" colspan="9"><br></td>
                </tr>
                <tr>
                    <td scope="row" style="text-align: right;padding:2px;" colspan="6" class="tabDetailViewDL">Subtotal:&nbsp;&nbsp;</td>
                    <td style="text-align: right;padding:2px;" class="tabDetailViewDL">' . $currency_symbol . number_format($v_pro->subtotal, 2) . '</td>
                </tr>';
    if(number_format($v_pro->new_sub, 2) != 0.00 && number_format($v_pro->deal_tot, 2) != 0.00){
                $html .= '<tr>
                    <td scope="row" style="text-align: right;padding:2px;" colspan="6" class="tabDetailViewDL">Discount:&nbsp;&nbsp;</td>
                    <td style="text-align: right;padding:2px;" class="tabDetailViewDL">' . $currency_symbol . number_format($v_pro->deal_tot, 2) . '</td>
                </tr>
                <tr>
                    <td scope="row" style="text-align: right;padding:2px;" colspan="6" class="tabDetailViewDL">Discounted Subtotal::&nbsp;&nbsp;</td>
                    <td style="text-align: right;padding:2px;" class="tabDetailViewDL">' . $currency_symbol . number_format($v_pro->new_sub, 2) . '</td>
                </tr>';
}
                $html .= '<tr>
                    <td scope="row" style="text-align: right;padding:2px;" colspan="6" class="tabDetailViewDL">Tax:&nbsp;&nbsp;</td>
                    <td style="text-align: right;padding:2px;" class="tabDetailViewDL">' . $currency_symbol . number_format($v_pro->tax, 2) . '</td>
                </tr>
                <tr>
                    <td scope="row" style="text-align: right;padding:2px;" colspan="6" class="tabDetailViewDL">Shipping:&nbsp;&nbsp;</td>
                    <td style="text-align: right;padding:2px;" class="tabDetailViewDL">' . $currency_symbol . number_format($v_pro->shipping, 2) . '</td>
                </tr>
                <tr>
                    <td scope="row" style="text-align: right;padding:2px;" colspan="6" class="tabDetailViewDL"><b>Total:</b>&nbsp;&nbsp;</td>
                    <td style="text-align: right;padding:2px;" class="tabDetailViewDL"><b>' . $currency_symbol . number_format($v_pro->total, 2) . '</b></td>
                </tr>
                </table>';
}
if($quote_entry->Result->name_value_list->calc_grand_total->value == 1){
$html .= '
                <tr>
                <th colspan="2" scope="col">
                Grand Total
                </th>
                </tr>
                <tr>
                <td width="12.5%" scope="col">
                Currency:
                </td>
                <td width="37.5%"  field="total_amt" type="currency" class="inlineEdit">
                <span id="total_amt">
                ' . $currency_iso4217 . '
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <tr>
                <td width="12.5%" scope="col">
                Subtotal:
                </td>
                <td width="37.5%"  field="total_amt" type="currency" class="inlineEdit">
                <span id="total_amt">
                ' . $currency_symbol . number_format($quote_entry->Result->name_value_list->subtotal->value, 2) . '
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <tr>
                <td width="12.5%" scope="col">
                Discount:
                </td>
                <td width="37.5%"  field="discount_amount" type="currency" class="inlineEdit">
                <span id="discount_amount">
                ' . $currency_symbol . number_format($quote_entry->Result->name_value_list->deal_tot->value, 2) . '
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <tr>
                <td width="12.5%" scope="col">
                Discounted Subtotal:
                </td>
                <td width="37.5%"  field="subtotal_amount" type="currency" class="inlineEdit">
                <span id="subtotal_amount">
                ' . $currency_symbol . number_format($quote_entry->Result->name_value_list->new_sub->value, 2) . '
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <tr>';
                $taxratid = $quote_entry->Result->name_value_list->taxrate_id->value;
                if(!empty($taxratid)){
                    $trate_val = $_SESSION['TatRates']->$taxratid->value;
                    $html .= '<td width="12.5%" scope="col">
                    Tax Rate:
                    </td>
                    <td width="37.5%"  field="tax_amount" type="currency" class="inlineEdit">
                    <span id="tax_amount">
                    ' . $currency_symbol . number_format($trate_val, 2) . '%
                    </span>
                    <div class="inlineEditIcon"> </div>         </td>
                    </tr>
                    <tr>';
                }
                $html .='<td width="12.5%" scope="col">
                Tax:
                </td>
                <td width="37.5%"  field="tax_amount" type="currency" class="inlineEdit">
                <span id="tax_amount">
                ' . $currency_symbol . number_format($quote_entry->Result->name_value_list->tax->value, 2) . '
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <tr>
                <td width="12.5%" scope="col">
                Shipping Provider:
                </td>
                <td width="37.5%"  field="shipper_name" type="shipper_name" class="inlineEdit">
                <span id="shipper_name">
                ' . $quote_entry->Result->name_value_list->shipper_name->value . '
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <tr>
                <td width="12.5%" scope="col">
                Shipping:
                </td>
                <td width="37.5%"  field="total_amount" type="currency" class="inlineEdit">
                <span id="total_amount">
                ' . $currency_symbol . number_format($quote_entry->Result->name_value_list->shipping->value, 2) . '
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <tr>
                <td width="12.5%" scope="col">
                Total:
                </td>
                <td width="37.5%"  field="total_amount" type="currency" class="inlineEdit">
                <span id="total_amount">
                ' . $currency_symbol . number_format($quote_entry->Result->name_value_list->total->value, 2) . '
                </span>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>
                <div class="inlineEditIcon"> </div>         </td>
                </tr>';
}
                $html .= '</table>
    </div>

</div>
</div>';
?>