<?php

if (isset($id) && !empty($id) && $casenote == null) {
    $html .= "
                        <div class='scp-form-title scp-$module_name-font'>
                        <h3>Edit " . $module_without_s . "</h3>";

    if ($module_name != 'AOS_Quotes' && $module_name != 'AOS_Invoices' && $module_name != 'AOS_Contracts' && $module_name != 'AOK_KnowledgeBase') {
        $view = 'edit';
        $deleted = 1;
        $html .= "<div class='scp-move-action-btn'><a id='clear_btn_id' onclick='bcp_clear_search_txtbox(0,\"$module_name\",\"\",\"\",\"\",\"list\",\"$current_url\");' href='javascript:void(0);'  class='scp-$module_name scp-dtl-viewbtn' title='List'><span class='fa fa-list' ></span><span>LIST</span></a><a href='javascript:void(0);' title='Delete' class='deletebtn scp-$module_name scp-dtl-deletebtn' onclick='form_submit(\"$module_name\",\"$view\",\"$id\",\"$deleted\",\"$current_url\");'><span class='fa fa-trash-o'></span><span>DELETE</span></a></div>";
    }
} else {
    $html .= "
                        <div class=' scp-$module_name-font scp-form-title'>
                        <h3>Add New " . $module_without_s . "</h3>";
    if ($module_name != 'AOS_Quotes' && $module_name != 'AOS_Invoices' && $module_name != 'AOS_Contracts' && $module_name != 'AOK_KnowledgeBase') {

        $html .= "<div class='scp-move-action-btn'><a id='clear_btn_id' onclick='bcp_clear_search_txtbox(0,\"$module_name\",\"\",\"\",\"\",\"list\",\"$current_url\");' href='javascript:void(0);'  class='scp-$module_name scp-dtl-viewbtn' title='List'><span class='fa fa-list' ></span><span>LIST</span></a></div>";
    }
}
$html.="</div>";
$html .= "<div class='scp-form scp-form-2-col'>
                    <form action='" . home_url() . "/wp-admin/admin-post.php' method='post' enctype='multipart/form-data' id='general_form_id'>
                    <div class='scp-form-container'>";

$parent_type = isset($parent_type) ? $parent_type : '';
$parent_id = isset($parent_id) ? $parent_id : '';
foreach ($results as $k => $vals_lbl) {
    $html .= "<div class='panel " . $vals_lbl->lable_value . " scp-dtl-panel'><div class='scp-col-12 panel-title'><span class='panel_name'>" . $vals_lbl->lable_value . "</span></div>";
    foreach ($vals_lbl->rows as $k_lbl2 => $vals) {
        if (count($vals) > 1) {
            $single_double = "6";
        } else {
            $single_double = "12";
        }
        foreach ($vals as $key_fields => $val_fields) {
            $option_relate_flg_text = $maxlength_field = '';
            if (isset($val_fields->displayParams) && $val_fields->displayParams->cols != '' && $val_fields->displayParams->cols != '' && $val_fields->displayParams->maxlength != '') {
                $cols = $val_fields->displayParams->cols;
                $rows = $val_fields->displayParams->rows;
                $max_len = $val_fields->displayParams->maxlength;
                $maxlength_field = "maxlength='" . $max_len . "'";//max length validation
            } else {
                $cols = 30;
                $rows = 2;
                //$max_len = 150;
            }
            if (isset($val_fields->type) && $val_fields->type == 'readonly') {
                $readonly = 'readonly';
            } else {
                $readonly = '';
            }
            $ar_val = isset($val_fields->name) ? $val_fields->name : '';
            $res2 = $objSCP->get_module_fields($module_name, array($ar_val)); //get module fields
            foreach ($res2->module_fields as $k_fileds => $fileds) {//loop for module fields
                if (($module_name == "Meetings" && (($fileds->name == "duration_hours") || ($fileds->name == "duration_minutes"))) || ($fileds->name == "parent_id" && $fileds->type == "id")) {
                    continue;
                }
                $class_name = '';
                $ftype = "text";
                if ($fileds->type == 'assigned_user_name') {
                    continue;
                }
                if ($fileds->type == 'int') {
                    if (empty($fileds->options)) {
                        array_push($text_sugar_array, $fileds->type);
                    } else {
                        array_push($select_options_sugar_array, $fileds->type);
                    }
                }
                $req = '';
                $option_flg = false;
                $txtarea_flg = false;
                $option_relate_flg = false;
                $check_box_flg = false;
                if ($ar_val == "account_name" && isset($id) && !empty($id) && $module_name != "Leads") {
                    $ar_val = "account_id";
                }
                if ($ar_val == "assigned_user_name" && isset($id) && !empty($id)) {
                    $ar_val = "assigned_user_id";
                }
                if ($ar_val == "campaign_name" && isset($id) && !empty($id)) {//Added by BC on 09-dec-2015
                    $ar_val = "campaign_id";
                }
                if ($ar_val == "related_doc_name" && isset($id) && !empty($id)) {//related doc id change
                    $ar_val = "related_doc_id";
                }
                //value
                $value = isset($record_detail->entry_list[0]->name_value_list->$ar_val->value) ? $record_detail->entry_list[0]->name_value_list->$ar_val->value : '';
                //Added by BC on 08-jul-2016
                $dwload = $cls = '';
                if (!empty($value)) {
                    $dwload = "<i class='fa fa-download' aria-hidden='true'></i> Download";
                    $cls = "general-link-btn scp-download-btn";
                }
                if (in_array($fileds->type, $text_sugar_array) && $val_fields->type != 'address') {
                    if ($val_fields->type == 'file') {
                        $ftype = "file";
                    } else {
                        $ftype = "text";
                    }
                    if ($fileds->type == 'datetimecombo' || $fileds->type == 'datetime') {
                        $class_name = 'datetimepicker';
                    }
                    if ($fileds->type == 'int' && !empty($fileds->options)) {
                        $option_flg = true;
                    }
                    if ($fileds->type == "id") {//If field is of type id
                        if ($fileds->name == "currency_id") {//if currency field
                            $option_flg = true;
                        } else {
                            $option_relate_flg_text = true;
                            $readonly = 'readonly';
                        }
                    }
                } else if (in_array($fileds->type, $textarea_sugar_array) || $val_fields->type == 'address') {
                    $txtarea_flg = true;
                } else if (in_array($fileds->type, $select_options_sugar_array)) {
                    $option_flg = true;
                } else if (in_array($fileds->type, $select_relate_options_sugar_array)) {
                    if ($val_fields->name == "contact_name") {
                        $option_relate_flg_text = true;
                        $where_con = "contacts.id = '{$_SESSION['scp_user_id']}'";
                        $record_detail_contact = $objSCP->get_entry_list('Contacts', $where_con);
                        $value = $record_detail_contact->entry_list[0]->name_value_list->name->value;
                        $readonly = 'disabled';
                    } else {
                        $option_relate_flg = true;
                    }
                } else if (in_array($fileds->type, $check_box_sugar_array)) {//Added by BC on 07-nov-2015
                    $check_box_flg = true;
                }

                if ($fileds->required == TRUE) {
                    $req = "required";
                }

                if ($option_flg == true) {
                    $html .= "<div class='scp-col-" . $single_double . "'>
                                    <div class='scp-form-group " . $req . "'>
                                    <label>" . $fileds->label . "</label>";
                    $options = $fileds->options;
                    $html .= "<select class='input-text scp-form-control' id='" . $ar_val . "' name='" . $ar_val . "'>";
                    if ($fileds->name == "currency_id") {//if currency field
                        if (isset($_SESSION['Currencies'])) {
                            $curr_arry = (array) $_SESSION['Currencies'];
                            foreach ($curr_arry as $k_curr => $v_curr) {
                                $html .= "<option value='" . $k_curr . "'";
                                if ($k_curr == $value) {
                                    $html .= 'selected';
                                }
                                $html .= ">" . $v_curr->name . " : " . $v_curr->symbol . "</option>";
                            }
                        }
                    } else {
                        foreach ($options as $k_op => $v_op) {
                            //  for special option case of add cases
                            $html .= "<option value='" . $options->$k_op->name . "'";
                            if ($options->$k_op->name == $value) {
                                $html .= 'selected';
                            }
                            $html .= ">" . $options->$k_op->value . "</option>";
                        }
                    }
                    $html .= "</select></div></div>";
                } else if ($txtarea_flg == true) {
                    $html .= "<div class='scp-col-" . $single_double . "'>
                                    <div class='scp-form-group " . $req . "'>
                                    <label>" . $fileds->label . "</label>
                                    <textarea class='input-text scp-form-control' id='" . $fileds->id_name . "' name='" . $ar_val . "' cols='" . $cols . "' rows='" . $rows . "' $maxlength_field " . $readonly . ">" . $value . "</textarea>
                                    </div>
                                    </div>";
                } else if ($option_relate_flg == true) {
                    if ($fileds->related_module == 'Users' || array_key_exists($fileds->related_module, (array) $_SESSION['module_array'])) {//if relate module is not in active module array 
                        $html .= "<div class='scp-col-" . $single_double . "'>
                                    <div class='scp-form-group " . $req . "'>
                                    <label>" . $fileds->label . "</label>
                                    <div id='related_to_id'>";
                        $module_name_record = $fileds->related_module;
                        $select_fields = array('id', 'name');
                        if ($fileds->name == "assigned_user_id" || $fileds->name == "assigned_user_name") {//Added by BC on 12-aug-2015 for assigned to all records shown of users
                            $record_detail2 = $objSCP->get_entry_list($module_name_record, '', $select_fields);
                            $ar_val = "assigned_user_name";
                        } else if ($val_fields->name == "campaign_name") {
                            $record_detail2 = $objSCP->get_entry_list("Campaigns", '', $select_fields);
                        } else {
                            $record_detail2 = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], strtolower($module_name_record), $select_fields);
                        }
                        $html .= "<select class='input-text scp-form-control' id='" . $fileds->id_name . "' name='" . $ar_val . "' $req>
                                    <option value=''></option>";
                        for ($m = 0; $m < count($record_detail2->entry_list); $m++) {
                            $mod_id = $record_detail2->entry_list[$m]->name_value_list->id->value;
                            $mod_name = $record_detail2->entry_list[$m]->name_value_list->name->value;
                            $html .= "<option value='" . $mod_id . "'";
                            if ($mod_id == $value) {
                                $html .= 'selected';
                            }
                            $html .= ">" . $mod_name . "</option>";
                        }
                        $html .= "</select>
                                    </div>
                                    </div>
                                    </div>";
                    }
                } else if ($option_relate_flg_text == true) {
                    $html .= "<div class='scp-col-" . $single_double . "'>
                                    <div class='scp-form-group " . $req . "'>
                                    <label>" . $fileds->label . "</label>
                                    <input class='input-text scp-form-control' type='" . $ftype . "' id='" . $fileds->id_name . "' name='" . $ar_val . "' value='";
                    $html .= "" . $value . "'" . $req . " " . $readonly . "/>
                                    </div>
                                    </div>";
                } else if ($check_box_flg == true) {
                    if ($value) {
                        $chekd = "checked='checked'";
                    } else {
                        $chekd = "";
                        $value = 0;
                    }
                    $html .= "<div class='scp-col-" . $single_double . "'>
                                    <div class='scp-form-group " . $req . "'>
                                    <label>" . ucfirst($fileds->name) . "</label>
                                    <input class='input-text scp-form-control scp-special-checkbox' type='checkbox' id='checkbox_" . $ar_val . "' name='" . $ar_val . "' value='";
                    $html .= "" . $value . "'" . $req . " " . $readonly . " onclick='chek(this.id)' $chekd/>
                                    </div>
                                    </div>";
                } else {

                    if ($fileds->name == "parent_name" && $fileds->type == "parent") {
                        if ($module_name == "Notes" && $casenote != null) {
                            $html .= "<div class='scp-col-6'>
                                        <div class='scp-form-group " . $req . "'>
                                        <label>" . $fileds->label . "</label>";
                            $values = 'Cases';
                            //$modules = $objSCP->getPortal_accessibleModules();
                            $html .= "<select id='parent_name' class='scp-form-control' name='parent_name' onchange='get_module_list()' disabled>";
                            $html .= "<option value='" . $values . "'";
                            if ($values == 'Cases') {
                                $html .= 'selected';
                            }
                            $html .= ">" . $values . "</option>";


                            $html .= "</select>
                                        </div>
                                        </div>
                                        <div class='scp-col-6'>
                                        <div class='scp-form-group " . $req . "'>
                                        <label>Parent ID:</label><div id='related_to_id2'>";
                            $parent_type = "Cases";
                            $parent_id = $casenote;
                            $module_name_parent = $parent_type;
                            $select_fields = array('id', 'name');

                            $record_detail2 = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], strtolower($module_name_parent), $select_fields);
                            $html .= "
                                        <select class='input-text scp-form-control' title='' id='parent_id' name='parent_id' disabled>
                                        <option value=''></option>";
                            for ($m = 0; $m < count($record_detail2->entry_list); $m++) {
                                $mod_id = $record_detail2->entry_list[$m]->name_value_list->id->value;
                                $mod_name = $record_detail2->entry_list[$m]->name_value_list->name->value;
                                $html .= "<option value='" . $mod_id . "'";
                                if ($mod_id == $parent_id) {
                                    $html .= 'selected';
                                }
                                $html .= ">" . $mod_name . "</option>";
                            }
                            $html .= "</select>
                               </div></div></div>";
                        } else {
                            $html .= "<div class='scp-col-6'>
                                        <div class='scp-form-group " . $req . "'>
                                        <label>" . $fileds->label . "</label>";
                            if (empty($_SESSION['module_array'])) {//check session value in module array
                                $modules_array2 = $objSCP->getPortal_accessibleModules();
                                $modules = $modules_array2->accessible_modules;
                            } else {
                                $modules = $_SESSION['module_array'];
                            }
                            $html .= "<select id='parent_name' class='scp-form-control' name='parent_name' onchange='get_module_list()'>";
                            foreach ($modules as $key => $values) {
                                if ($key != "Contacts" && $key != $module_name) {
                                    $html .= "<option value='" . $key . "'";
                                    if ($key == $parent_type) {
                                        $html .= 'selected';
                                    }
                                    $html .= ">" . $values . "</option>";
                                }
                            }
                            $html .= "</select>
                                        </div>
                                        </div>
                                        <div class='scp-col-6'>
                                        <div class='scp-form-group " . $req . "'>
                                        <label>Parent ID:</label><div id='related_to_id2'>";
                            if ($id == '') {
                                $parent_type = "Accounts";
                            }
                            $module_name_parent = $parent_type;
                            $select_fields = array('id', 'name');

                            $record_detail3 = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], strtolower($module_name_parent), $select_fields);
                            $html .= "
                                        <select class='input-text scp-form-control' title='' id='parent_id' name='parent_id'>
                                        <option value=''></option>";
                            for ($m = 0; $m < count($record_detail3->entry_list); $m++) {
                                $mod_id = $record_detail3->entry_list[$m]->name_value_list->id->value;
                                $mod_name = $record_detail3->entry_list[$m]->name_value_list->name->value;
                                $html .= "<option value='" . $mod_id . "'";
                                if ($mod_id == $parent_id) {
                                    $html .= 'selected';
                                }
                                $html .= ">" . $mod_name . "</option>";
                            }
                            $html .= "</select>
                                        </div></div></div>";
                        }
                    } else {//not parent field then
                        if ($val_fields->name == 'file_mime_type') {
                            $readonly = 'readonly';
                        }
                        if ($val_fields->name == 'case_number') {
                            $readonly = 'readonly';
                            $req = '';
                        }
                        if (($val_fields->name == 'email' || $val_fields->name == 'email1')) {
                            if ($val_fields->required == "true") {
                                $class_name = 'required email';
                            } else {
                                $class_name = 'email';
                            }
                        }
                        $html .= "<div class='scp-col-" . $single_double . "'>
                                        <div class='scp-form-group " . $req . "'>
                                        <label>" . $fileds->label . "</label>";
                        if ($ftype == 'file' && isset($id) && !empty($id) && ($module_name != "Notes" && $casenote == null)) {
                            //$html .= "<br><a href='javascript:void(0);'  onclick='form_submit_document(\"$id\");'>" . $value . "</a>";
                            $html .= "<br><a href='javascript:void(0);'  onclick='form_submit_document(\"$id\");' class='" . $cls . " scp-$module_name-font'>" . $dwload . "</a>";
                            $html .= "<input type='hidden' name='edit-file' value='" . $value . "'>";
                        } else {
                            $html .= "<input class='input-text scp-form-control $class_name avia-datepicker-div' type='" . $ftype . "' id='" . $ar_val . "' name='" . $ar_val . "' value='";

                            if ($fileds->type == 'url' && empty($id)) {
                                $html .= 'http://';
                            }
                            if (($fileds->type == 'datetimecombo' || $fileds->type == 'datetime') && isset($id) && !empty($id)) {
                                //Updated by BC on 17-jun-2016 for timezone store in session
                                if (empty($_SESSION['user_timezone'])) {
                                    $result_timezone = $objSCP->getUserTimezone();
                                } else {
                                    $result_timezone = $_SESSION['user_timezone'];
                                }
                                $UTC = new DateTimeZone("UTC");
                                $newTZ = new DateTimeZone($result_timezone);
                                $date = new DateTime($value, $UTC);
                                $date->setTimezone($newTZ);
                                $value = $date->format('Y-m-d H:i');
                            }

                            $html .= "" . htmlspecialchars($value, ENT_QUOTES, 'UTF-8', false) . "'" . $req . " " . $readonly . "/>";
                            if ($module_name == "Notes" && $ftype == 'file' && isset($id) && !empty($id)) {
                                if (!empty($value)) {
                                    //$html .= "<br><a href='javascript:void(0);' onclick='form_submit_note_document(\"$id\");'>" . $value . "</a>";
                                    $html .= "<br><a href='javascript:void(0);' onclick='form_submit_note_document(\"$id\");' class='" . $cls . " scp-$module_name-font'>" . $dwload . "</a>";
                                    $html .= "<input type='hidden' name='edit-file' value='" . $value . "'>";
                                }
                            }
                        }
                        $html .= "</div>
                                        </div>";
                    }
                }
            }
        }
    }
    $html .= "</div>";
}
$html .= "<div class='scp-form-actions'>
                <input type='hidden' name='action' value='bcp_add_moduledata_call'>
                <input type='hidden' name='module_name' value='" . $module_name . "'>
                <input type='hidden' name='view' value='" . $view . "'>
                <input type='hidden' name='current_url' value=" . $current_url . ">";
if (isset($id) && !empty($id)) {
    $html .= "<input type='hidden' name='id' value='" . $id . "'>";
}
if ($module_name == "Notes" && $casenote != null) {
    $html .= "<input type='hidden' name='case_id' value='" . $casenote . "'>";
}
$html .= "<span><input type='submit' value='Submit' class='hover active scp-button action-form-btn scp-$module_name' />";
if ($casenote == null) {//in case view note section remove cancel button
    $html .= "&nbsp&nbsp<input type='button' value='Cancel' class='hover active scp-button action-form-btn' onclick=\"window.location='" . home_url() . "/portal-manage-page/?list-$module_name'\"/></span>";
}
$html .= "</div>
                </div>
                </form>
                </div>";
?>