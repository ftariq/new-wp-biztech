<?php

include_once("../../../../wp-config.php");
include_once("../../../../wp-load.php");
include_once("../../../../wp-includes/wp-db.php");

global $wpdb, $sugar_crm_version;
if (isset($_REQUEST['scp_user_id']) && !empty($_REQUEST['scp_user_id'])) {
    $scp_sugar_rest_url = $_REQUEST['scp_sugar_rest_url'];
    $scp_sugar_username = $_REQUEST['scp_sugar_username'];
    $scp_sugar_password = $_REQUEST['scp_sugar_password'];
    $scp_user_id = $_REQUEST['scp_user_id'];

    $objSCP = new SugarRestApiCall($scp_sugar_rest_url, $scp_sugar_username, $scp_sugar_password);
    //$accessble_modules = $objSCP->getUser_accessibleModules($_SESSION['scp_user_id']);
    if (!is_object($_SESSION['module_array'])) {//check session value in module array
        $modules_array2 = $objSCP->getPortal_accessibleModules();
        $accessble_modules = $modules_array2->accessible_modules;
    } else {
        $accessble_modules = (array) $_SESSION['module_array'];
    }
    $arry_event_modules = array('Meetings', 'Calls', 'Tasks');
    $t = 0;
    // set default timezone from sugar side*******
    //Updated by BC on 17-jun-2016 for timezone store in session
    if (empty($_SESSION['user_timezone'])) {
        $result_timezone = $objSCP->getUserTimezone();
        if ($sugar_crm_version == 7) {
            $result_timezone = $result_timezone->timezone;
        }
    } else {
        $result_timezone = $_SESSION['user_timezone'];
        if ($sugar_crm_version == 7) {
            $result_timezone = $_SESSION['user_timezone']->timezone;
        }
    }
    date_default_timezone_set($result_timezone); //updated by BC on 20-jun-2016
    $result_timezone = date_default_timezone_get();
    //default timezone******
    foreach ($arry_event_modules as $module_name) {
        if (array_key_exists($module_name, $accessble_modules)) {

            $arry = array('id', 'name', 'date_start');
            if ($module_name == "Calls") {
                array_push($arry, "duration_hours", "duration_minutes");
            }
            if ($module_name == "Meetings") {
                array_push($arry, "date_end");
            }
            if ($module_name == "Tasks") {
                array_push($arry, "date_due");
            }
            //version 6
            if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
                $list_result = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $arry);
                $cnt = count($list_result->entry_list);
                if ($cnt > 0) {

                    for ($m = 0; $m < $cnt; $m++) {//for loop starts
                        $id = $list_result->entry_list[$m]->name_value_list->id->value;
                        $name = $list_result->entry_list[$m]->name_value_list->name->value;
                        $date_start = $list_result->entry_list[$m]->name_value_list->date_start->value;
                        //for convert utc timezone format
                        $UTC = new DateTimeZone("UTC");
                        $newTZ = new DateTimeZone($result_timezone);
                        $date = new DateTime($date_start, $UTC);
                        $date->setTimezone($newTZ);
                        $date_start = $date->format("Y-m-d H:i:s");
                        ////////////////////////////
                        $explode_date = explode(" ", $date_start);

                        $ary[$t]['id'] = $id;
                        $ary[$t]['title'] = preg_replace("/[^A-Za-z0-9\-: ']/", '', $name);
                        $ary[$t]['start'] = $date_start;
                        $ary[$t]['allDay'] = false;

                        if ($module_name == "Calls") {
                            $duration_hours = $list_result->entry_list[$m]->name_value_list->duration_hours->value;
                            $duration_minutes = $list_result->entry_list[$m]->name_value_list->duration_minutes->value;
                            if (!empty($duration_hours)) {
                                $time_hr_min = "+" . $duration_hours . " hour ";
                            }
                            if (!empty($duration_minutes)) {
                                $time_hr_min .= "+" . $duration_minutes . " minutes";
                            }
                            if (!empty($time_hr_min)) {
                                $end_date_calls = date("Y-m-d H:i:s", strtotime($time_hr_min, strtotime($date_start)));
                                $ary[$t]['end'] = $end_date_calls;
                            }
                            $ary[$t]['hours'] = $duration_hours;
                            $ary[$t]['backgroundColor'] = get_option('biztech_scp_calendar_calls_color');
                            $view1 = 'detail';
                            //$ary[$t]['url'] = '?' . $view1 . '-' . $module_name . '&id=' . $id;
                        }
                        if ($module_name == "Meetings") {
                            $date_end = $list_result->entry_list[$m]->name_value_list->date_end->value;
                            $ary[$t]['end'] = $date_end;
                            $ary[$t]['hours'] = $duration_hours;
                            $ary[$t]['backgroundColor'] = get_option('biztech_scp_calendar_meetings_color');
                            $view1 = 'detail';
                            //$ary[$t]['url'] = '?' . $view1 . '-' . $module_name . '&id=' . $id;
                        }
                        if ($module_name == "Tasks") {
                            $date_end = $list_result->entry_list[$m]->name_value_list->date_due->value;
                            if (isset($date_end) && !empty($date_end)) {
                                $ary[$t]['end'] = $date_end;
                                //getting hours
                                $timestamp1 = strtotime($date_start);
                                $timestamp2 = strtotime($date_end);
                                $hour = abs($timestamp2 - $timestamp1) / (60 * 60);
                                $ary[$t]['hours'] = $hour;
                            } else {
                                $ary[$t]['hours'] = 0;
                            }
                            $ary[$t]['backgroundColor'] = get_option('biztech_scp_calendar_tasks_color');
                            $view1 = 'detail';
                            //$ary[$t]['url'] = '?' . $view1 . '-' . $module_name . '&id=' . $id;
                        }
                        $t++;
                    }//for loop ends
                }
            }
            //version 7
            $final_str = implode(',', $arry);
            if ($sugar_crm_version == 7) {
                $list_result = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $final_str, array(), '', '', 'id:ASC');
                $cnt = count($list_result->records);
                if ($cnt > 0) {

                    for ($m = 0; $m < $cnt; $m++) {//for loop starts
                        $id = $list_result->records[$m]->id;
                        $name = $list_result->records[$m]->name;
                        $date_start = $list_result->records[$m]->date_start;
                        //Added by BC on 01-sep-2015 for convert utc timezone format
                        $UTC = new DateTimeZone("UTC");
                        $newTZ = new DateTimeZone($result_timezone);
                        $date = new DateTime($date_start, $UTC);
                        $date->setTimezone($newTZ);
                        $date_start = $date->format("Y-m-d H:i:s");
                        ////////////////////////////
                        $explode_date = explode(" ", $date_start);

                        $ary[$t]['id'] = $id;
                        $ary[$t]['title'] = preg_replace("/[^A-Za-z0-9\-: ']/", '', $name);
                        $ary[$t]['start'] = $date_start;
                        $ary[$t]['allDay'] = false;

                        if ($module_name == "Calls") {
                            $duration_hours = $list_result->records[$m]->duration_hours;
                            $duration_minutes = $list_result->records[$m]->duration_minutes;
                            if (!empty($duration_hours)) {
                                $time_hr_min = "+" . $duration_hours . " hour ";
                            }
                            if (!empty($duration_minutes)) {
                                $time_hr_min .= "+" . $duration_minutes . " minutes";
                            }
                            if (!empty($time_hr_min)) {
                                $end_date_calls = date("Y-m-d H:i:s", strtotime($time_hr_min, strtotime($date_start)));
                                $ary[$t]['end'] = $end_date_calls;
                            }
                            $ary[$t]['hours'] = $duration_hours;
                            $ary[$t]['backgroundColor'] = get_option('biztech_scp_calendar_calls_color');
                            $view1 = 'detail';
                            //$ary[$t]['url'] = '?' . $view1 . '-' . $module_name . '&id=' . $id;
                        }
                        if ($module_name == "Meetings") {
                            $date_end = $list_result->records[$m]->date_end;
                            $ary[$t]['end'] = $date_end;
                            $ary[$t]['hours'] = $duration_hours;
                            $ary[$t]['backgroundColor'] = get_option('biztech_scp_calendar_meetings_color');
                            $view1 = 'detail';
                            //$ary[$t]['url'] = '?' . $view1 . '-' . $module_name . '&id=' . $id;
                        }
                        if ($module_name == "Tasks") {
                            $date_end = $list_result->records[$m]->date_due;
                            if (isset($date_end) && !empty($date_end)) {
                                $ary[$t]['end'] = $date_end;
                                //getting hours
                                $timestamp1 = strtotime($date_start);
                                $timestamp2 = strtotime($date_end);
                                $hour = abs($timestamp2 - $timestamp1) / (60 * 60);
                                $ary[$t]['hours'] = $hour;
                            } else {
                                $ary[$t]['hours'] = 0;
                            }
                            $ary[$t]['backgroundColor'] = get_option('biztech_scp_calendar_tasks_color');
                            $view1 = 'detail';
                            //$ary[$t]['url'] = '?' . $view1 . '-' . $module_name . '&id=' . $id;
                        }
                        $t++;
                    }//for loop ends
                }
            }
        }
    }
    echo json_encode($ary);
}
?>