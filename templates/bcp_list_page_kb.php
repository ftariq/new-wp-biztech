<?php

if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
    $class = 'search-input-six';
}
if ($sugar_crm_version == 7) {
    $class = 'search-input-seven';
}
$html .= "<div class='scp-knowledgebase-wrapper'>
		<div class='head-part'>
                <input type='text' name='search_name' value='" . $search_name . "' id='search_name' placeholder='Search' class='$class'/>";
if (($sugar_crm_version == 6 || $sugar_crm_version == 5) && $module_name == 'AOK_KnowledgeBase') {
    //Addded by BC on 22-jun-2016
    //knowledge Base Category List
    $list_knb_cat = $objSCP->get_entry_list('AOK_Knowledge_Base_Categories', '', array('id', 'name'), '', 0);
    $html .= "<div class='select-style' ><select name='AOK_Knowledge_Base_Categories' id='AOK_Knowledge_Base_Categories'>";
    $html .= "<option value=''>Select KB Category</option>";
    foreach ($list_knb_cat->entry_list as $list_result_s) {
        $id = $list_result_s->name_value_list->id->value;
        $name = $list_result_s->name_value_list->name->value;

        $html .= "<option value='" . $id . "'";
        if ($id == $AOK_Knowledge_Base_Categories) {
            $html .= 'selected="selected"';
        }
        $html .= ">" . $name . "</option>";
    }
    $html .= "</select></div>";
}
$html .= "<div class='action-btns'>
					<span><a href='javascript:;' onclick='bcp_module_paging(0,\"$module_name\",1,\"\",\"\",\"$view\",\"$current_url\")' id='search_btn_id' ><i class='fa fa-search'></i></a></span>
					<span><a id='clear_btn_id' onclick='bcp_clear_search_txtbox(0,\"$module_name\",\"\",\"\",\"\",\"$view\",\"$current_url\");' href='javascript:;'><i class='fa fa-remove'></i></a></span>
			</div>  
		</div>";
if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
    $list_res_kb = $list_result->entry_list;
}
if ($sugar_crm_version == 7) {
    $list_res_kb = $list_result->records;
}
foreach ($list_res_kb as $list_result_s) {
    $kbcat_name = '';
    if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
        $id = $list_result_s->name_value_list->id->value;
        $kb_title = $list_result_s->name_value_list->name->value;
        $kb_created_date_origional = $list_result_s->name_value_list->date_entered->value;
        $kb_desc_full = html_entity_decode($list_result_s->name_value_list->description->value);//strip html tags if any
        $kb_desc = strlen($kb_desc_full) > 100 ? substr($kb_desc_full, 0, 100) . "..." : $kb_desc_full;
        $kb_created_date = date('M d, Y', strtotime($kb_created_date_origional));
        $result_kbcat = $objSCP->get_relationships('AOK_KnowledgeBase', $id, 'aok_knowledgebase_categories', array('id','name'));
        if(!empty($result_kbcat->entry_list[0])){
        $kbcat_name = ($result_kbcat->entry_list[0]->name_value_list->name->value);}
        if(!empty($kbcat_name)){
            $kbcat_name = $kbcat_name." - ";
        }else{
            $kbcat_name = "Uncategorized - ";
        }
    }
    //echo "<pre>".print_r($list_result_s);exit;
    if ($sugar_crm_version == 7) {
        $id = $list_result_s->id;
        if (isset($_SESSION['sugar_version']) && $_SESSION['sugar_version'] == '7.7.0.0') {//if sugar 7 version 7.7
            $kb_title = $list_result_s->name;
            $kb_desc_full = html_entity_decode($list_result_s->kbdocument_body);//strip html tags if any
        }else{
            $kb_title = $list_result_s->kbdocument_name;
            $kb_desc_full = html_entity_decode($list_result_s->description);//strip html tags if any
        }
        $kb_created_date_origional = $list_result_s->date_entered;
        //$kb_desc = substr($kb_desc_full, 0, 100);
        $kb_desc = strlen($kb_desc_full) > 100 ? substr($kb_desc_full, 0, 100) . "..." : $kb_desc_full;
        $kb_created_date = date('M d, Y', strtotime($kb_created_date_origional));
    }
//    $html .= "<p><a href='javascript:void(0);' onclick='bcp_module_call_view(\"$module_name\",\"$id\",\"detail\",\"$current_url\");'>Read More</a></p>";
    $html .= "<div class='detail-part'>
				<div class='question'>
					<h4 title='Click here to view'><i class='fa fa-plus'></i>" . $kb_title . "</h4>
					<p><i class='fa fa-key'></i>" . $kbcat_name . $kb_created_date . "</p>
				</div>
				<div class='answer'  style='display:none;'>
					<p>" . $kb_desc_full . "</p>";
    if ($module_name == 'KBDocuments') {//Added by BC on 22-jun-2016
        if (count($list_result_s->attachment_list) > 0) {
    $html .= '<div class="panel New Panel 1 scp-dtl-panel"><div class="scp-col-12 panel-title"><span class="panel_name">Attachments</span></div>';
        foreach ($list_result_s->attachment_list as $k_attch => $v_attch) {
            if (isset($_SESSION['sugar_version']) && $_SESSION['sugar_version'] == '7.7.0.0') {//if sugar 7 version 7.7
                $html .= "<a href='javascript:void(0);' onclick='form_submit_note_document(\"$v_attch->id\");' class='general-link-btn scp-download-btn scp-KBDocuments-font scp-default-font'><i class='fa fa-download' aria-hidden='true'></i>&nbsp;&nbsp;" . $v_attch->name . "s</a><br>";
            }else{
                $html .= "<a href='javascript:void(0);' onclick='form_submit_KBdocument(\"$v_attch->document_revision_id\");' class='general-link-btn scp-download-btn scp-KBDocuments-font scp-default-font'><i class='fa fa-download' aria-hidden='true'></i>&nbsp;&nbsp;" . $v_attch->name . "</a><br>";
            }
        }
    $html .= "</div>";
    }
}
    $html .= "</div>
			</div>";
}
$html .= "</div>";
$html .= "<script>
    jQuery(document).ready(function(){
     jQuery('.detail-part .question').on('click', function() {
            if(!jQuery(this).hasClass('active')) {
                jQuery('.detail-part .answer').slideUp();
                jQuery('.detail-part .question').removeClass('active');
                jQuery(this).addClass('active');
                jQuery(this).parent('.detail-part').find('.answer').slideDown();
            }else{
                jQuery(this).parent('.detail-part').find('.answer').slideUp();
                jQuery(this).parent('.detail-part').find('.question').removeClass('active');
            }
        });
        });
    </script>";
