
<!-- BEGIN PAGE -->
<div class="">
    <div class="row ">
        <div class="col-md-6 col-sm-6">
            <!-- BEGIN PORTLET-->
            <div class="portlet box blue calendar">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-calendar"></i><h3><span class="fa fa-calendar side-icon-wrapper"></span> Calendar</h3></div>
                </div>
                <div class="portlet-body light-grey">
                    <div id="calendar"></div>
                    <ul class="list-group">
                        <li class="list-group-item">Calls : <span class="badge" style="<?php echo 'background:' . get_option('biztech_scp_calendar_calls_color'); ?>"> </span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Meetings : <span class="badge" style="<?php echo 'background:' . get_option('biztech_scp_calendar_meetings_color'); ?>"> </span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tasks : <span class="badge" style="<?php echo 'background:' . get_option('biztech_scp_calendar_tasks_color'); ?>"> </span></li>
                    </ul>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
</div>
<!-- END PAGE -->

<script type='text/javascript'>
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    var h = {};

    if ($('#calendar').width() <= 400) {
        $('#calendar').addClass("mobile");
        h = {
            left: 'title, prev, next',
            center: '',
            right: 'today,month,agendaWeek,agendaDay'
        };
    } else {
        $('#calendar').removeClass("mobile");
        if (App.isRTL()) {
            h = {
                right: 'title',
                center: '',
                left: 'prev,next,today,month,agendaWeek,agendaDay'
            };
        } else {
            h = {
                left: 'title',
                center: '',
                right: 'prev,next,today,month,agendaWeek,agendaDay'
            };
        }
    }
    //var a=new Date(y, m, d + 1, 19, 0) ;

    var el2 = jQuery("#calendar");
    App.blockUI(el2);

    $('#calendar').fullCalendar('destroy'); // destroy the calendar
    $('#calendar').fullCalendar({//re-initialize the calendar
        disableDragging: true,
        disableResizing: true,
        defaultView: 'month',
        slotMinutes: 15,
        minTime: 8,
        header: h,
        editable: true,
        lazyFetching: true,
        timeFormat: 'h(:mm) t - ', // uppercase H for 24-hour clock,lowercase h for 12-hour clock
        viewDisplay: function (view) {
            //App.blockUI(el2);
        },
        events: {
            url: '<?php echo plugin_dir_url(__FILE__); ?>' + "json_events.php?scp_sugar_rest_url=<?php echo get_option('biztech_scp_rest_url'); ?>&scp_sugar_username=<?php echo get_option('biztech_scp_username'); ?>&scp_sugar_password=<?php echo get_option('biztech_scp_password'); ?>&scp_user_id=<?php echo $_SESSION['scp_user_id']; ?>",
            success: function (data)
            {
                App.unblockUI(el2);
            }
        }
    });
    //for opening event in new tab
    jQuery('#calendar').on('click', '.fc-event', function (e) {
        e.preventDefault();
        //window.open(jQuery(this).attr('href'), '_blank');
        window.open(jQuery(this).attr('href'), '_self');
    });

</script>