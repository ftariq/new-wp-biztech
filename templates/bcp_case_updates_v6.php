<?php

$html .= "<h3 class='scp-Cases-font'><span class='fa Cases side-icon-wrapper'></span> Case Updates</h3>";
$sel_update_fields = array('id', 'name', 'date_entered', 'contact_id', 'internal');
if ($sugar_crm_version == 6) {//custom case updates call for sugar 6
    $getCaseCommetsResult = $objSCP->get_relationships('Cases', $id, 'bc_case_updates_cases', $sel_update_fields, '', 'date_entered ASC');
}
if ($sugar_crm_version == 5) {//for suite 6 default
    $getCaseCommetsResult = $objSCP->get_relationships('Cases', $id, 'aop_case_updates', $sel_update_fields, '', 'date_entered ASC');
}
if ($getCaseCommetsResult->entry_list != NULL) {
    $html .= "<ul class='scp-data-scroll'>";
    $cntcomts = 0;

    $countCaseCommets = 0;
    foreach ($getCaseCommetsResult->entry_list as $setCaseCommetsObj) {
        $get_internal = $setCaseCommetsObj->name_value_list->internal->value;
        if ($get_internal == 1) {
            continue;
        }
        $countCaseCommets++;
    }
    $countCaseCommets = $countCaseCommets - 1;

    foreach ($getCaseCommetsResult->entry_list as $setCaseCommetsObj) {
        $lastcomt = '';
        $setCaseCommets = $setCaseCommetsObj->name_value_list;
        //get internal
        $get_internal = $setCaseCommets->internal->value;
        if ($get_internal == 1) {
            continue;
        }
        //get date entered
        $get_date = $setCaseCommets->date_entered->value;
        $UTC = new DateTimeZone("UTC");
        $newTZ = new DateTimeZone($result_timezone);
        $date = new DateTime($get_date, $UTC);
        $date->setTimezone($newTZ);
        $date_entered = $date->format($objSCP->date_format . " " . $objSCP->time_format);

        //get contact user name
        $contact_iid = $setCaseCommets->contact_id->value;

        if (isset($contact_iid) && !empty($contact_iid)) {
            $where_con_cc = "contacts.id = '{$contact_iid}'";
            $record_detail_contact_cc = $objSCP->get_entry_list('Contacts', $where_con_cc);
            $salutation = $record_detail_contact_cc->entry_list[0]->name_value_list->salutation->value;
            $contact_user_name = $record_detail_contact_cc->entry_list[0]->name_value_list->name->value;
            if (isset($salutation) && !empty($salutation)) {
                $con_name = "- ".$salutation . " " . $contact_user_name;
            } else {
                $con_name = "- ".$contact_user_name;
            }
        } else {
            $con_name = '- Administrator';
        }
        //get name
        $comment_name = $setCaseCommets->name->value;
        //get comment id
        $comment_id = $setCaseCommets->id->value;
        
        if ($countCaseCommets == $cntcomts) {
            $lastcomt = 'last';
        }
        $html .= "<li class='" . $lastcomt . " scp-case-list'>
                            <div id='comment_section_" . $comment_id . "' class='caseUpdate'>" . $comment_name . "</div>
                            <span class='scp-case-author'>" . $con_name . "</span><span class='scp-case-date'>" . $date_entered . "</span>";
        $html .= "</li>";
        $cntcomts++;
    }
    $html .= "</ul>";
    $html .= "<div class='scp-case-form'><form action = '" . home_url() . "/wp-admin/admin-post.php' method = 'post' enctype = 'multipart/form-data' id = 'case_updates'>
            <input type = 'hidden' name = 'action' value = 'bcp_case_updates'>
            <textarea rows=\"2\" cols=\"95\" name=\"update_text\" id=\"update_text\" placeholder=\"Enter your comments...\"></textarea>
            <br><br><input type='button' value='Save' class='hover active scp-button action-form-btn scp-Cases-update' onclick='case_updates_form_submit(\"$id\")' />
            </form></div>";
} else {
    $html .= "<div class='scp-case-form'><form action = '" . home_url() . "/wp-admin/admin-post.php' method = 'post' enctype = 'multipart/form-data' id = 'case_updates'>
            <input type = 'hidden' name = 'action' value = 'bcp_case_updates'>
            <textarea rows=\"2\" cols=\"95\" name=\"update_text\" id=\"update_text\" placeholder=\"Enter your comments...\"></textarea>
            <br><br><input type='button' value='Save' class='hover active scp-button action-form-btn scp-Cases-update' onclick='case_updates_form_submit(\"$id\")' />
            </form></div>";
}