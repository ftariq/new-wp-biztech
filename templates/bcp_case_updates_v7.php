<?php

$html .= "<h3 class='scp-Cases-font'><span class='fa Cases side-icon-wrapper'></span> Case Updates</h3>";
$sel_update_fields = "id,name,date_entered,contact_id_c,internal,description";
//custom case updates call for sugar 7
$getCaseCommetsResult = $objSCP->getRelationship('Cases', $id, 'bc_case_updates_cases', $sel_update_fields, array(), '', '', 'date_entered:DESC');

if ($getCaseCommetsResult->records != NULL) {
    $html .= "<ul class='scp-data-scroll'>";
    $cntcomts = 0;

    $countCaseCommets = 0;
    foreach ($getCaseCommetsResult->records as $setCaseCommetsObj) {
        $get_internal = $setCaseCommetsObj->internal;
        if ($get_internal == 1) {
            continue;
        }
        $countCaseCommets++;
    }
    $countCaseCommets = $countCaseCommets - 1;

    foreach ($getCaseCommetsResult->records as $setCaseCommets) {
        //$setCaseCommets = $setCaseCommetsObj->name_value_list;
        //get internal
        $get_internal = $setCaseCommets->internal;
        if ($get_internal == 1) {
            continue;
        }
        //get date entered
        $get_date = $setCaseCommets->date_entered;
        $UTC = new DateTimeZone("UTC");
        $newTZ = new DateTimeZone($result_timezone);
        $date = new DateTime($get_date, $UTC);
        $date->setTimezone($newTZ);
        //Updated by BC on 16-nov-2015
        $date_format = $_SESSION['user_date_format'];
        $time_format = $_SESSION['user_time_format'];
        $date_entered = $date->format($date_format . " " . $time_format);

        //get contact user name
        $contact_iid = $setCaseCommets->contact_id_c;

        if (isset($contact_iid) && !empty($contact_iid)) {
            $record_detail_contact_cc = $objSCP->getRecordDetail('Contacts', $contact_iid);
            if(isset($record_detail_contact_cc->salutation)){
            $salutation = $record_detail_contact_cc->salutation;}else{
                $salutation='';
            }
             if(isset($record_detail_contact_cc->name)){
             $contact_user_name = $record_detail_contact_cc->name;}else{
                 $contact_user_name='';
             }
            if (isset($salutation) && !empty($salutation)) {
                $con_name = "- " . $salutation . " " . $contact_user_name;
            } else {
                $con_name = "- " . $contact_user_name;
            }
        } else {
            $con_name = '- Administrator';
        }
        //get name
        $comment_name = $setCaseCommets->description;
        //get comment id
        $comment_id = $setCaseCommets->id;

        if ($countCaseCommets == $cntcomts) {
            $lastcomt = 'last';
        }else{
            $lastcomt = '';  
        }
        $html .= "<li class='" . $lastcomt . " scp-case-list'>
                            <div id='comment_section_" . $comment_id . "' class='caseUpdate'>" . $comment_name . "</div>
                            <span class='scp-case-author'>" . $con_name . "</span><span class='scp-case-date'>" . $date_entered . "</span>";
        $html .= "</li>";
        $cntcomts++;
        $lastcomt = '';
    }
    $html .= "</ul>";
    $html .= "<div class='scp-case-form'><form action = '" . home_url() . "/wp-admin/admin-post.php' method = 'post' enctype = 'multipart/form-data' id = 'case_updates'>
            <input type = 'hidden' name = 'action' value = 'bcp_case_updates'>
            <textarea rows=\"2\" cols=\"95\" name=\"update_text\" id=\"update_text\" placeholder=\"Enter your comments...\"></textarea>
            <br><br><input type='button' value='Save' class='hover active scp-button action-form-btn scp-Cases-update' onclick='case_updates_form_submit(\"$id\")' />
            </form></div>";
} else {
    $html .= "<div class='scp-case-form'><form action = '" . home_url() . "/wp-admin/admin-post.php' method = 'post' enctype = 'multipart/form-data' id = 'case_updates'>
            <input type = 'hidden' name = 'action' value = 'bcp_case_updates'>
            <textarea rows=\"2\" cols=\"95\" name=\"update_text\" id=\"update_text\" placeholder=\"Enter your comments...\"></textarea>
            <br><br><input type='button' value='Save' class='hover active scp-button action-form-btn scp-Cases-update' onclick='case_updates_form_submit(\"$id\")' />
            </form></div>";
}