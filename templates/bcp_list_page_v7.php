<?php

$html .= "<tr class='row main-col'>";
foreach ($results as $key_name => $val_array) {
    if ($key_name == "SET_COMPLETE") {
        continue;
    }
    $val_ary_type = (isset($val_array->type)) ? $val_array->type : NULL;
    if (isset($val_array->related_module)) {
        if ($val_ary_type == 'relate' && ($val_array->related_module != 'Users' && !array_key_exists($val_array->related_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array 
            continue;
        }
    }
    $ar_val = strtolower($key_name);
    $name_arry[] = $ar_val;
    //make type array
    
        $fields_meta[$ar_val] = $val_ary_type;
    
    if (isset($val_array->label_value)) {
        $field_label = str_replace(':', '', $val_array->label_value);
    } else {
        $field_label = '';
    }
    $field_name = $ar_val;
    if ($field_label == "modified_user_id" || $field_label == "created_by" || ($field_label == "id" && $id_show == false)) {
        continue;
    }
    if ($val_ary_type == 'file') {//Front Side: 'Notes' Module: No need to place sorting on 'Attachment'
        $html .= "<th><a>" . $field_label . "</a></th>";
    } else {
        if (($order_by == $field_name) && ($order == 'desc')) {
            $html .= "<th><a href='javascript:void(0);' onclick='bcp_module_order_by(0,\"$module_name\",\"$field_name\",\"asc\",\"$view\",\"$current_url\");' class='scp-desc-sort'>" . $field_label . "</a></th>";
        } else if (($order_by == $field_name) && ($order == 'asc')) {
            $html .= "<th><a href='javascript:void(0);' onclick='bcp_module_order_by(0,\"$module_name\",\"$field_name\",\"desc\",\"$view\",\"$current_url\");' class='scp-asc-sort'>" . $field_label . "</a></th>";
        } else if ($field_label != '') {
            $html .= "<th><a href='javascript:void(0);' onclick='bcp_module_order_by(0,\"$module_name\",\"$field_name\",\"asc\",\"$view\",\"$current_url\");' class='scp-both-sort'>" . $field_label . "</a></th>";
        }
    }
}
$html .= "<th class='action-th'><a>Action</a></th>";
$html .= "</tr>";
foreach ($list_result->records as $list_result_s) {
    if (isset($list_result_s)) {
        $id = $list_result_s->id;
    }if (isset($list_result_s->parent_type)) {
        $parent_type = $list_result_s->parent_type;
    }

    if ($module_name == 'Quotes') {//Added by BC on 11-july-2016 for currency symbol
        $curreny_id = $list_result_s->currency_id;
        if (!empty($curreny_id)) {
            $currency_symbol = $_SESSION['Currencies']->$curreny_id->symbol;
        }
    }
    $team_arr = array();
    $html .= "<tr>";
    foreach ($name_arry as $nkey => $nval) {
        switch ($nval) {
            case 'assigned_user_id':
                $get_entrty_module = "Users";
                break;
            case 'modified_user_id':
                $get_entrty_module = "Users";
                break;
            case 'created_by':
                $get_entrty_module = "Users";
                break;
            case 'parent_id':
                if ($parent_type) {
                    $get_entrty_module = $parent_type;
                } else {
                    $get_entrty_module = "";
                }
                break;
            case 'account_name':
                $get_entrty_module = "Accounts";
                break;
            default:
                $get_entrty_module = "";
                break;
        }
        $hlink_st = $hlink_en = $cls = '';
        $value =  (isset($list_result_s->$nval)) ? $list_result_s->$nval : NULL;
        
        if ($nval == "filename") {
            if (!empty($value)) {//Added by BC on 08-jul-2016
                $cls = "general-link-btn scp-download-btn";

                if ($module_name == "Notes") {
                    $hlink_st = "<a href='javascript:void(0);' onclick='form_submit_note_document(\"$id\");' class='kb-attachment $cls scp-$module_name-font'>";
                    $hlink_en = "</a>";
                }
                if ($module_name == "Documents") {
                    $hlink_st = "<a href='javascript:void(0);' onclick='form_submit_document(\"$id\");' class='kb-attachment $cls scp-$module_name-font'>";
                    $hlink_en = "</a>";
                }
            }
            //else {
//                $hlink_st = '-';
//            }
        }
        if ($nval == "date_modified" || $nval == "date_start" || $nval == "date_end" || $nval == "date_entered" || $nval == "date_due") {
            if ($value != '') { // aaded to display blank date instead of default date
                $UTC = new DateTimeZone("UTC");
                $newTZ = new DateTimeZone($result_timezone);
                $date = new DateTime($value, $UTC);
                $date->setTimezone($newTZ);
                //Updated by BC on 16-nov-2015
                $date_format = $_SESSION['user_date_format'];
                $time_format = $_SESSION['user_time_format'];
                $value = $date->format($date_format . " " . $time_format);
            }
        }
        if ($fields_meta[$nval] == 'date') {//for date field
            $value = (!empty($value)) ? date($objSCP->date_format, strtotime($value)) : '';
        }
        if ($get_entrty_module) {
            if ($nval != "account_name" && $module_name != "Leads") {
                $select_fields = array('name');
                $record_detail = $objSCP->getModuleRecords($get_entrty_module, $value, $select_fields);
                if (isset($record_detail) && !empty($record_detail)) {
                    $value = $record_detail->records[0]->name;
                }
            }
        }
        //Added by BC on 16-jul-2016 for enum options
        if ($fields_meta[$nval] == 'enum' || $fields_meta[$nval] == 'multienum' || $fields_meta[$nval] == 'dynamicenum') {
            $res_getEnum = $objSCP->getEnumValues($module_name, $nval);
            foreach ($res_getEnum as $k_opt => $v_opt) {
                if ($value == $k_opt) {
                    $value = (!empty($value)) ? $v_opt : '';
                }
            }
        }
        if ($fields_meta[$nval] == 'currency' && !empty($value)) {
            $value = $currency_symbol . number_format($value, 2);
        }
        if ($nval == "portal_flag" || $nval == "embed_flag" || $nval == "do_not_call" || $nval == "is_template") {
            if ($value == 1) {
                $value = "Yes";
            } else {
                $value = "No";
            }
        }
        if ($nval == "team_name") {
            foreach ($value as $teams) {
                $team_arr[] = $teams->name;
            }
            $value = implode(', ', $team_arr);
        }

        if ($nval == 'link') {
            continue;
        }
        if ($fields_meta[$nval] == 'url' && !empty($value)) {
            if (strpos($value, 'http') === false) {
                $value = 'http://' . $value;
            }
            $hlink_st = "<a href='" . $value . "' target='_blank'>";
            $hlink_en = "</a>";
        }
        if ($fields_meta[$nval] == 'email') {//for email field
            $value = (!empty($value[0]->email_address)) ? $value[0]->email_address : '';
        }
        if ($value == '' || empty($value)) {
            $value = "-";
        }
        if ($nval == "filename" && !empty($value) && $value != '-') {
            $html .="<td>" . $hlink_st . "<i class='fa fa-download' aria-hidden='true'></i> Download" . $hlink_en . "</td>";
        } else {
            $html .="<td>" . $hlink_st . $value . $hlink_en . "</td>";
        }
    }
    $view = 'edit';
    $deleted = 1;
    if ($module_name != 'Quotes' && $module_name != 'KBDocuments') {
        $html .= "<td class='action edit'><a href='javascript:void(0);' onclick='bcp_module_call_add(0,\"$module_name\",\"$view\",\"\",\"\",\"$id\");'><span class='fa fa-pencil' title='Edit'></span></a>   <a href='javascript:void(0);' onclick='bcp_module_call_view(\"$module_name\",\"$id\",\"detail\",\"$current_url\");'><span class='fa fa-eye' title='View'></span></a><a href='javascript:void(0);' onclick='form_submit(\"$module_name\",\"$view\",\"$id\",\"$deleted\",\"$current_url\");'><span class='fa fa-trash-o' title='Delete'></span></a></td>";
    } else {
        $html .= "<td class='action edit'><a href='javascript:void(0);' onclick='bcp_module_call_view(\"$module_name\",\"$id\",\"detail\",\"$current_url\");'><span class='fa fa-eye' title='View'></span></a></td>";
    }
    $html .= "</tr>";
}
?>