<?php

foreach ($results as $k => $vals_lbl) {
    foreach ($vals_lbl->rows as $k_lbl2 => $vals) {
        foreach ($vals as $key_fields => $val_fields) {
            if (isset($val_fields) && !empty($val_fields)) {
                $name_arry = array();
                $ar_val = $val_fields->name;
                $name_arry[] = $ar_val;
                $field_name = '';
                if(isset($_REQUEST[$val_fields->name])){
                    $field_name = $_REQUEST[$val_fields->name];
                }
                if ($val_fields->name == 'date_start' && ($field_name == '' || $field_name == NULL)) {
                    $field_name = '';
                } else if ($val_fields->name == 'date_start') {
                    //Updated by BC on 20-jun-2016 for timezone store in session
                    if (empty($_SESSION['user_timezone'])) {
                        $result_timezone = $objSCP->getUserTimezone();
                    } else {
                        $result_timezone = $_SESSION['user_timezone'];
                    }
                    $offset = get_timezone_offset($result_timezone->timezone, 'UTC');
                    $GetDate = gmdate("Y-m-d H:i:s", strtotime($field_name) + $offset);
                    ////////////////////////////////////////
                    $GetDate1 = str_replace(' ', "T", $GetDate);
                    $GetDate = $GetDate1 . "+00:00";
                    $field_name = $GetDate;
                }
                if ($val_fields->name == 'date_end' && ( $field_name == '' || $field_name == NULL)) {
                    $field_name = '';
                } else if ($val_fields->name == 'date_end') {
                    //Updated by BC on 20-jun-2016 for timezone store in session
                    if (empty($_SESSION['user_timezone'])) {
                        $result_timezone = $objSCP->getUserTimezone();
                    } else {
                        $result_timezone = $_SESSION['user_timezone'];
                    }
                    $offset = get_timezone_offset($result_timezone->timezone, 'UTC');
                    $GetDate = gmdate("Y-m-d H:i:s", strtotime($field_name) + $offset);
                    $GetDate1 = str_replace(' ', "T", $GetDate);
                    $GetDate = $GetDate1 . "+00:00";
                    $field_name = $GetDate;
                }
                if ($val_fields->name == 'date_due' && ($field_name == '' || $field_name == NULL)) {
                    $field_name = '';
                } else if ($val_fields->name == 'date_due') {
                    //Updated by BC on 20-jun-2016 for timezone store in session
                    if (empty($_SESSION['user_timezone'])) {
                        $result_timezone = $objSCP->getUserTimezone();
                    } else {
                        $result_timezone = $_SESSION['user_timezone'];
                    }
                    $offset = get_timezone_offset($result_timezone->timezone, 'UTC');
                    $GetDate = gmdate("Y-m-d H:i:s", strtotime($field_name) + $offset);
                    $GetDate1 = str_replace(' ', "T", $GetDate);
                    $GetDate = $GetDate1 . "+00:00";
                    $field_name = $GetDate;
                }
                if ($val_fields->name == "account_name" && $module_name != "Leads") {
                    $pass_name_arry["account_id"] = stripslashes_deep($field_name);
                } else if ($val_fields->name == "assigned_user_name") {
                    $pass_name_arry["assigned_user_id"] = stripslashes_deep($field_name);
                } else if ($val_fields->name == "email") {
                    $pass_name_arry["email1"] = stripslashes_deep($field_name);
                } else if ($val_fields->name == "do_not_call" && $module_name == "Leads" && $field_name == '') {
                    $pass_name_arry["do_not_call"] = 0;
                } else if ($val_fields->name == "portal_flag" && (stripslashes_deep($field_name == '') || stripslashes_deep($field_name == null))) {
                    $pass_name_arry["portal_flag"] = 0;
                } else if ($val_fields->name == "embed_flag" && (stripslashes_deep($field_name == '') || stripslashes_deep($field_name == null))) {
                    $pass_name_arry["embed_flag"] = 0;
                } else if ($val_fields->name == "parent_name") {
                    $pass_name_arry["parent_type"] = stripslashes_deep($field_name);
                    $pass_name_arry["parent_id"] = stripslashes_deep($_REQUEST["parent_id"]);
                } else if ($val_fields->name == "team_name") {
                    $team_name_array = $objSCP->getRecordDetail("Contacts", $_SESSION['scp_user_id']);
                    //$team_id = $team_name_array->team_name[0]->id;
                    $pass_name_arry[$val_fields->name] = $team_name_array->team_name;
                } else if ($val_fields->name == "campaign_name") {//Added by BC on 09-dec-2015
                    $pass_name_arry["campaign_id"] = stripslashes_deep($field_name);
                } else if ($val_fields->name == "related_doc_name") {//related doc id pass
                    $pass_name_arry["related_doc_id"] = trim($_REQUEST[$val_fields->name]);
                } else {
                        $pass_name_arry[$val_fields->name] = stripslashes_deep($field_name);
                }
            }
        }
    }
}
?>