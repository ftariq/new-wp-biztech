<?php

$html .= "
                    <div class='scp-form-title scp-$module_name-font scp-default-font'>
                    <h3>Details of " . $name . "</h3>";
if ($module_name != 'AOS_Quotes' && $module_name != 'AOS_Invoices' && $module_name != 'AOS_Contracts' && $module_name != 'AOK_KnowledgeBase') {
    $view2 = 'edit';
    $deleted = 1;

    $html .= "<div class='scp-move-action-btn'><a href='javascript:void(0);' title='Delete' class='deletebtn scp-$module_name scp-dtl-deletebtn' onclick='form_submit(\"$module_name\",\"$view2\",\"$id\",\"$deleted\",\"$current_url\");'><span class='fa fa-trash-o'></span><span>DELETE</span></a><a href='javascript:void(0);' title='Edit'  onclick='bcp_module_call_add(0,\"$module_name\",\"$view2\",\"\",\"\",\"$id\");' class='scp-$module_name scp-dtl-editbtn'><span class='fa fa-pencil' ></span><span>EDIT</span></a><a id='clear_btn_id' onclick='bcp_clear_search_txtbox(0,\"$module_name\",\"\",\"\",\"\",\"list\",\"$current_url\");' href='javascript:void(0);'  class='scp-$module_name scp-dtl-viewbtn' title='List'><span class='fa fa-list' ></span><span>LIST</span></a></div>";
}
$html.="</div>";
$html .="<div class='scp-form scp-form-2-col'>";
$currency_symbol = isset($currency_symbol) ? $currency_symbol : '';
foreach ($results as $k => $vals_lbl) {
    $html .= "<div class='panel " . $vals_lbl->lable_value . " scp-dtl-panel'><div class='scp-col-12 panel-title'><span class='panel_name'>" . $vals_lbl->lable_value . "</span></div>";

    foreach ($vals_lbl->rows as $k_lbl2 => $vals) {
        foreach ($vals as $key_fields => $val_fields) {
            $team_arr = array();
            $labl_val = isset($val_fields->label_value) ? $val_fields->label_value : '';
            if (isset($val_fields->name) && $val_fields->name == '') {
                continue;
            }
            $ar_val = $k_fileds = isset($val_fields->name) ? $val_fields->name : '';
            $hlink_acc_st = $hlink_acc_ed = '';
            if ($k_fileds == "modified_user_id" || $k_fileds == "created_by") {
                continue;
            }
            if ($val_fields->type == 'relate' && ($val_fields->relate_module != 'Users' && !array_key_exists($val_fields->relate_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array 
                continue;
            }
            switch ($ar_val) {
                case 'assigned_user_id':
                    $ar_val = 'assigned_user_name';
                    break;
                case 'modified_user_id':
                    $ar_val = 'modified_by_name';
                    break;
                case 'created_by':
                    $ar_val = 'created_by_name';
                    break;
                case 'account_id':
                    $ar_val = 'account_name';
                    break;
            }

            $value = isset($record_detail->entry_list[0]->name_value_list->$ar_val->value) ? html_entity_decode($record_detail->entry_list[0]->name_value_list->$ar_val->value) : '';
            //Added by BC on 08-jul-2016
            $dwload = $cls = '';
            if (!empty($value)) {
                $dwload = "<i class='fa fa-download' aria-hidden='true'></i> Download";
                $cls = "general-link-btn scp-download-btn";
            }
            // for show formate as in sugar crm
            if ($k_fileds == "date_modified" || $k_fileds == "date_start" || $k_fileds == "date_end" || $k_fileds == "date_entered" || $k_fileds == "date_due") {
                $UTC = new DateTimeZone("UTC");
                $newTZ = new DateTimeZone($result_timezone);
                $date = new DateTime($value, $UTC);
                $date->setTimezone($newTZ);
                $value = $date->format($objSCP->date_format . " " . $objSCP->time_format);
            }
            if (isset($val_fields->type) && $val_fields->type == 'date') {//for date field
                $value = (!empty($value)) ? date($objSCP->date_format, strtotime($value)) : '';
            }
            if (isset($val_fields->name) && $val_fields->name == "contact_name") {
                $where_con = "contacts.id = '{$_SESSION['scp_user_id']}'";
                $record_detail_contact = $objSCP->get_entry_list('Contacts', $where_con);
                $value = $record_detail_contact->entry_list[0]->name_value_list->name->value;
            }
            //Added by BC on 07-aug-2015 for special option case of add cases,update on 11-july-2016                                                                                                                                                            
            if (isset($val_fields->type) && ($val_fields->type == 'enum' || $val_fields->type == 'multienum' || $val_fields->type == 'dynamicenum')) {
                $res2 = $objSCP->get_module_fields($module_name, array($ar_val));
                foreach ($res2->module_fields as $k_fileds_sub => $fileds) {
                    $value = (!empty($value)) ? $fileds->options->$value->value : '';
                }
            }
            if (isset($val_fields->name) && ($val_fields->name == "portal_flag" || $val_fields->name == "embed_flag" || $val_fields->name == "do_not_call" || $val_fields->name == "is_template")) {//Added by BC on 21-jun-2016
                if ($value == "1") {
                    $value = "Yes";
                } else {
                    $value = "No";
                }
            }
            if (isset($val_fields->type) && $val_fields->type == 'url' && isset($id) && !empty($id)) {
                if (strpos($value, 'http') === false) {
                    if ($value != '') {
                        $value = 'http://' . $value;
                    }
                }
                $hlink_acc_st = "<a href='" . $value . "' target='_blank'>";
                $hlink_acc_ed = "</a>";
            }
            if (isset($val_fields->type) && $val_fields->type == 'file' && isset($id) && !empty($id)) {
                if (!empty($value)) {
                    if ($module_name == "Notes") {
                        $hlink_acc_st .= "<a href='javascript:void(0);' onclick='form_submit_note_document(\"$id\");' class='$cls scp-$module_name-font'>";
                        $hlink_acc_ed = "</a>";
                    }
                    if ($module_name == "Documents") {
                        $hlink_acc_st .= "<a href='javascript:void(0);' onclick='form_submit_document(\"$id\");' class='$cls scp-$module_name-font'>";
                        $hlink_acc_ed = "</a>";
                    }
                } else {
                    $hlink_acc_st .='-';
                }
            }
            //for team names
            if ($k_fileds == "team_name") {
                foreach ($value as $teams) {
                    $team_arr[] = $teams->name;
                }
                $value = implode(', ', $team_arr);
            }
            if ($val_fields->name == "currency_id") {//see currency symbol as in add and edit page
                if (isset($_SESSION['Currencies'])) {
                    $curr_arry = (array) $_SESSION['Currencies'];
                    foreach ($curr_arry as $k_curr => $v_curr) {
                        if ($k_curr == $value) {
                            $value = $v_curr->name . " : " . $v_curr->symbol;
                        }
                    }
                }
            }
            if (isset($val_fields->type) && $val_fields->type == 'currency' && !empty($value)) {//Added by BC on 01-07-2016 for currency
                $value = $currency_symbol . number_format($value, 2);
            }
            if (isset($val_fields->type) && $val_fields->type == 'file' && !empty($value)) {//Added by BC on 15-jul-2016
                $html .= "                                     
                                <div class='scp-col-6 panel-left-label'>
                                <label><b>" . $labl_val . "</b></label>
                                <span class='data-view'>" . $hlink_acc_st . $dwload . $hlink_acc_ed . "</span>
                                </div>";
            } else {
                $html .= "                                     
                                <div class='scp-col-6 panel-left-label'>
                                <label><b>" . $labl_val . "</b></label>
                                <span class='data-view'>" . $hlink_acc_st . $value . $hlink_acc_ed . "</span>
                                </div>";
            }
        }
    }
    $html .= "</div>";
}
$html .= "</div>";
//for getting all notes related to this case
if ($module_name == "Cases") {
    //Added by BC on 24-jun-2016 for suiteCRM case comments*****************
    if ($sugar_crm_version == 5 || $sugar_crm_version == 6) {//custom case updates call for sugar 6
        $html .= "<div id='case-updates' class='all-notes scp-section-heading '>";
        include( TEMPLATE_PATH . 'bcp_case_updates_v6.php');
        $html .= "</div>";
    }
    //End by BC for suit case comments**********************************
    if (is_array((array) $_SESSION['module_array'])) {//Added for getting module key OR lable added from sugar side
        $arry_lable = (array) $_SESSION['module_array'];
        $module_name_label = $arry_lable["Notes"];
    }
    $html .= "<div class='all-notes scp-section-heading'>
                        <h3 class='scp-Notes-font'><span class='fa Notes side-icon-wrapper'></span> " . $module_name_label . "</h3>";
    $select_fields_cases = array('id', 'name', 'description', 'filename');
    $getCurrentCaseNotes = $objSCP->get_relationships('Cases', $id, 'notes', $select_fields_cases, '', 'date_entered desc');
    if ($getCurrentCaseNotes->entry_list != NULL) {//Added by BC on 22-sep-2015
        $html .= "<ul class='scp-data-scroll'>";
        $cntnotes = 0;

        $countNotes = 0;
        foreach ($getCurrentCaseNotes->entry_list as $setCurrentCaseNotesObj) {
            $countNotes++;
        }
        $countNotes = $countNotes - 1;

        foreach ($getCurrentCaseNotes->entry_list as $setCurrentCaseNotesObj) {
            $setCurrentCaseNotes = $setCurrentCaseNotesObj->name_value_list;
            if ($countNotes == $cntnotes) {
                $last = 'last';
            }
            $html .= "<li class='" . $last . "'>
                                <span class='name'>" . $setCurrentCaseNotes->name->value . "</span>
                                <span class='description'>" . nl2br($setCurrentCaseNotes->description->value) . "</span>";
            if ($setCurrentCaseNotes->filename->value != NULL) {
                $dwld_id = $setCurrentCaseNotes->id->value;
                $dwload_icn = "<i class='fa fa-download' aria-hidden='true'></i> Download";
                $cls_icn = "general-link-btn scp-download-btn scp-Notes-font";
                $html .= "<a href='javascript:void(0);' onclick='form_submit_note_document(\"$dwld_id\");' class='$cls_icn'> $dwload_icn </a>";
            }
            $html .= "</li>";
            $cntnotes++;
            $last = '';
        }

        $html .= "</ul>";
    } else {
        $html .= "<strong>No Record(s) Found.</strong>";
    }
    $html .= "</div>";
}
//End all notes
?>