<?php
add_action('admin_menu', 'biztech_crm_portal_create_menu');

function biztech_crm_portal_create_menu() {
    //create admin side menu
    add_menu_page('Customer Portal', 'Customer Portal', 'administrator', 'biztech-crm-portal', 'sugar_crm_portal_settings_page');
    //call register settings function
    add_action('admin_init', 'register_sugar_crm_portal_settings');
}

function remove_table_cache() {
    global $wpdb;
    $table_name = $wpdb->prefix . "module_layout";
    $wpdb->query('TRUNCATE TABLE ' . $table_name);
}

function register_sugar_crm_portal_settings() {

    //register our settings
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_name');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_rest_url');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_username');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_password');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_case_per_page');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_sugar_crm_version');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_tab_active');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_tab_inactive');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_upload_image');
    //Calendar calls color 
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_calendar_calls_color');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_calendar_meetings_color');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_calendar_tasks_color');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_custom_css');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_portal_menu_title');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_mail_subject');
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_mail_body');
    //Added by BC on 16-dec-016 for redirect page after login
    register_setting('sugar_crm_portal-settings-group', 'biztech_redirect_login');
    //Added by BC on 19-dec-016 
    register_setting('sugar_crm_portal-settings-group', 'biztech_redirect_signup'); //for redirect page for sign up
    register_setting('sugar_crm_portal-settings-group', 'biztech_redirect_profile'); // for redirect page for profile chnages
    register_setting('sugar_crm_portal-settings-group', 'biztech_redirect_forgotpwd'); //for redirect page for forgot passwords  
    register_setting('sugar_crm_portal-settings-group', 'biztech_redirect_manange'); //for redirect page for manage page
    register_setting('sugar_crm_portal-settings-group', 'biztech_redirect_dashboard_gui'); //for redirect page for dashboard
    //Added by BC on 04-jun-2016biztech_scp_theme_color
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_sidebar_position'); //for side bar position
    //Added by BC on 01-aug-2016 
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_theme_color'); //for theme color
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_recent_activity'); //for display recent activity on dashboard
    //Added by BC on 02-aug-2016
    register_setting('sugar_crm_portal-settings-group', 'biztech_scp_single_signin'); //for Single Sign-in
    register_setting('sugar_crm_portal-settings-group', 'biztech_portal_template'); //for save portal template
    //Added by BC on 20-jun-2016
    if (isset($_REQUEST['biztech_scp_rest_url']) && isset($_REQUEST['biztech_scp_sugar_crm_version'])) {
        $checking_rest_url = get_option('biztech_scp_rest_url');
        $new_rest_url = $_REQUEST['biztech_scp_rest_url'];
        if ($new_rest_url != $checking_rest_url) {

            if (get_option('biztech_scp_recent_activity') !== false) {
                // The option already exists, so we just update it.
                update_option('biztech_scp_recent_activity', ''); //clear recent activity option if url changed
            }
            if (isset($_REQUEST['biztech_scp_recent_activity'])) {
                $_POST['biztech_scp_recent_activity'] = '';
            }
            remove_table_cache();
        }
    }
    //End by BC
}

function sugar_crm_portal_settings_page() {
    $check_var = _isCurl();
    if (($check_var == 'yes')) {//Added by BC on 06-jun-2016 for CURL CHECKING
        // Admin side page options                
        ?>
        <div class='wrap'>
            <h2>SugarCRM/SuiteCRM Customer Portal Pro Settings</h2>

            <form method='post' action='options.php'>
                <?php settings_fields('sugar_crm_portal-settings-group'); ?>
                <?php do_settings_sections('sugar_crm_portal-settings-group'); ?>
                <table class='form-table'>
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Portal Name</th>
                        <td><input type='text'  class='regular-text' id="txtPortalName" value="<?php echo get_option('biztech_scp_name'); ?>" name='biztech_scp_name'></td>
                    </tr>
                    <tr>
                        <?php
                        $sugarCrmVersion = array(
                            '' => 'Select Version',
                            '7' => 'SugarCRM 7',
                            '6' => 'SugarCRM 6',
                            '5' => 'SuiteCRM',
                        );
                        ?>
                        <th scope="row">Version</th>
                        <td>
                            <select id="biztech_scp_sugar_crm_version" name="biztech_scp_sugar_crm_version">
                                <?php
                                $sel = '';
                                foreach ($sugarCrmVersion as $key => $velue) {

                                    if (get_option('biztech_scp_sugar_crm_version') == $key) {
                                        $sel = 'selected="selected"';
                                    }
                                    ?>
                                    <option value="<?php echo $key; ?>" <?php echo $sel; ?>><?php echo $velue; ?></option>
                                    <?php
                                    $sel = "";
                                }
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr valign='top'>
                        <th scope='row'>CRM URL</th>
                        <td><input type='text'  class='regular-text' value="<?php echo get_option('biztech_scp_rest_url'); ?>" name='biztech_scp_rest_url'>
                            <br />Enter your URL like http://example.com
                        </td>
                    </tr>

                    <tr valign='top'>
                        <th scope='row'>Username</th>
                        <td><input type='text' value="<?php echo get_option('biztech_scp_username'); ?>" name='biztech_scp_username'></td>
                    </tr>

                    <tr valign='top'>
                        <th scope='row'>Password</th>
                        <td><input type='password' value="<?php echo get_option('biztech_scp_password'); ?>" name='biztech_scp_password'></td>
                    </tr>

                    <tr valign='top' class="hide_class">
                        <th scope='row'>Records Per Page</th>
                        <td><input type="number" class="small-text" value="<?php
                            if (get_option('biztech_scp_case_per_page') != NULL) {
                                echo get_option('biztech_scp_case_per_page');
                            } else {
                                echo "5";
                            }
                            ?>" min="1" step="1" name="biztech_scp_case_per_page"></td>
                    </tr>
                    <!--//Added by BC on 10-jun-2015-->
                    <tr valign="top" class="hide_class">
                        <th scope="row">Portal Logo</th>
                        <td>
                            <label for="upload_image">
                                <div id="wpss_upload_image_thumb" class="wpss-file">
                                    <?php if (get_option('biztech_scp_upload_image') != NULL) { ?>
                                        <img src="<?php echo get_option('biztech_scp_upload_image'); ?>"  width="65"/><?php
                                    } else {
                                        echo $defaultImage;
                                    }
                                    ?>
                                </div>
                                <input id="upload_image" type="text" size="36" name="biztech_scp_upload_image" value="<?php
                                if (get_option('biztech_scp_upload_image') != NULL) {
                                    echo get_option('biztech_scp_upload_image');
                                } else {
                                    echo "";
                                }
                                ?>" />
                                <input id="upload_image_button" type="button" value="Upload Image" />
                                <input id="remove_button" type="button" value="Remove" onclick="clear_image()"/>
                                <br />Enter an URL or upload an image for the portal logo.
                                <script type="text/javascript">
                                    function clear_image() {
                                        jQuery("#upload_image").val('');
                                        jQuery('#wpss_upload_image_thumb img').hide();
                                    }
                                </script>
                            </label>
                        </td>
                    </tr>
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Mobile Portal Menu Title</th>
                        <td><input type="text" value="<?php
                            if (get_option('biztech_scp_portal_menu_title') != NULL) {
                                echo get_option('biztech_scp_portal_menu_title');
                            } else {
                                echo "Portal Menu";
                            }
                            ?>" name="biztech_scp_portal_menu_title" /></td>
                    </tr>
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Theme Color</th>
                        <td><input type="text" value="<?php
                            if (get_option('biztech_scp_theme_color') != NULL) {
                                echo get_option('biztech_scp_theme_color');
                            } else {
                                echo "";
                            }
                            ?>" class="my-color-field" data-default-color="" name="biztech_scp_theme_color" />
                            <br />Clear theme color to restore default</td>
                    </tr>
                    <tr valign='top' class="hide_class" id="calandar_call_color">
                        <th scope='row'>Calendar Calls Color</th>
                        <td><input type="text" value="<?php
                            if (get_option('biztech_scp_calendar_calls_color') != NULL) {
                                echo get_option('biztech_scp_calendar_calls_color');
                            } else {
                                echo "#7d9e12";
                            }
                            ?>" class="my-color-field" data-default-color="#7d9e12" name="biztech_scp_calendar_calls_color" /></td>
                    </tr>
                    <tr valign='top' class="hide_class" id="calandar_meeting_color">
                        <th scope='row'>Calendar Meetings Color</th>
                        <td><input type="text" value="<?php
                            if (get_option('biztech_scp_calendar_meetings_color') != NULL) {
                                echo get_option('biztech_scp_calendar_meetings_color');
                            } else {
                                echo "#00585e";
                            }
                            ?>" class="my-color-field" data-default-color="#00585e" name="biztech_scp_calendar_meetings_color" /></td>
                    </tr>
                    <tr valign='top' class="hide_class" id="calandar_task_color">
                        <th scope='row'>Calendar Tasks Color</th>
                        <td><input type="text" value="<?php
                            if (get_option('biztech_scp_calendar_tasks_color') != NULL) {
                                echo get_option('biztech_scp_calendar_tasks_color');
                            } else {
                                echo "#2582ed";
                            }
                            ?>" class="my-color-field" data-default-color="#2582ed" name="biztech_scp_calendar_tasks_color" /></td> 
                    </tr>
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Custom Css</th>
                        <td>
                            <textarea cols="50" rows="10" name="biztech_scp_custom_css"><?php
                                if (get_option('biztech_scp_custom_css') != NULL) {
                                    echo get_option('biztech_scp_custom_css');
                                }
                                ?>
                            </textarea>
                            <br/>
                            <?php _e('Leave blank to restore to default', 'sugar-portal'); ?> 
                        </td>
                    </tr>
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Portal User Registration Mail Subject</th>
                        <td><input type="text" value="<?php
                            if (get_option('biztech_scp_mail_subject') != NULL) {
                                echo get_option('biztech_scp_mail_subject');
                            }
                            ?>" name="biztech_scp_mail_subject" class="regular-text" />
                            <br />Leave blank to take default mail subject
                        </td>
                    </tr>
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Portal User Registration Mail General Message</th>
                        <td>
                            <textarea cols="50" rows="10" name="biztech_scp_mail_body"><?php
                                if (get_option('biztech_scp_mail_body') != NULL) {
                                    echo get_option('biztech_scp_mail_body');
                                }
                                ?></textarea>
                        </td>
                    </tr>
                    <!-- Added By BC on 04-jun-2016 -->
                    <tr valign='top' class="hide_class">
                        <th scope="row"><?php _e('Recent Activities On Dashboard', 'sugar-portal'); ?></th>
                        <td> 
                            <?php
                            if (class_exists('SugarRestApiCall')) {
                                $scp_sugar_rest_url = get_option('biztech_scp_rest_url');
                                $scp_sugar_username = get_option('biztech_scp_username');
                                $scp_sugar_password = get_option('biztech_scp_password');

                                $objSCP = new SugarRestApiCall($scp_sugar_rest_url, $scp_sugar_username, $scp_sugar_password);
                                if ($objSCP->login() != NULL) {//check
                                    if (get_option('biztech_scp_sugar_crm_version') == 6 || get_option('biztech_scp_sugar_crm_version') == 5) {
                                        $list_result = $objSCP->get_entry_list("bc_user_group");
                                    }
                                    if (get_option('biztech_scp_sugar_crm_version') == 7) {
                                        $select_fields = "id,name,accessible_modules";
                                        $list_result = $objSCP->getModuleRecords("bc_wp_user_group", $select_fields);
                                    }
                                    //get module list array
                                    $modules_list_ary_all = array();
                                    //For sugarCRM 6 & suiteCRM
                                    if (get_option('biztech_scp_sugar_crm_version') == 6 || get_option('biztech_scp_sugar_crm_version') == 5) {
                                        if (count($list_result->entry_list) > 0) {
                                            foreach ($list_result->entry_list as $k_op => $v_op) {
                                                $val_case = $v_op->name_value_list->id->value;
                                                $name_case = $v_op->name_value_list->name->value;
                                                $accessable_modules = $v_op->name_value_list->accessible_modules->value;
                                                $accessable_modules_array = $objSCP->unencodeMultienum($accessable_modules);
                                                foreach ($accessable_modules_array as $key_mod => $value_mod) {
                                                    if (!in_array($value_mod, $modules_list_ary_all)) {
                                                        $modules_list_ary_all[] = $value_mod;
                                                    }
                                                }
                                            }
                                        }
                                        $all_modules = $objSCP->get_all_modules(); //echo '<pre>'; print_r($all_modules); exit;
                                        $modules_with_lable = array();
                                        if (isset($all_modules->modules) && $all_modules != null) {
                                            foreach ($all_modules->modules as $all_module) {
                                                $modules_with_lable[$all_module->module_key] = $all_module->module_label;
                                            }
                                        }
                                    }
                                    //For SugarCRM 7
                                    if (get_option('biztech_scp_sugar_crm_version') == 7) {
                                        if (count($list_result->records) > 0) {
                                            for ($m = 0; $m < count($list_result->records); $m++) {
                                                $mod_id = $list_result->records[$m]->id;
                                                $mod_name = $list_result->records[$m]->name;
                                                $accessable_modules = $list_result->records[$m]->accessible_modules;
                                                foreach ($accessable_modules as $key_mod => $value_mod) {
                                                    if (!in_array($value_mod, $modules_list_ary_all)) {
                                                        $modules_list_ary_all[] = $value_mod;
                                                    }
                                                }
                                            }
                                        }
                                        $all_modules = $objSCP->get_all_modules();
                                        $modules_with_lable = array();
                                        $modules_with_lable = (array) $all_modules;
                                    }
                                }
                            }
                            ////////////////////
                            $recent_act_module = get_option('biztech_scp_recent_activity');
                            $html1 = '';
                            ?>
                            <select id="biztech_scp_recent_activity" name="biztech_scp_recent_activity[]" multiple="multiple" size="4">
                                <?php
                                sort($modules_list_ary_all);
                                foreach ($modules_list_ary_all as $key => $value) {
                                    $html1 .= "<option value='" . $value . "'";
                                    if ($recent_act_module != "" && in_array($value, $recent_act_module)) {
                                        $html1 .= ' selected';
                                    }
                                    $html1 .= ">" . $modules_with_lable[$value] . "</option>";
                                }
                                echo $html1;
                                ?>
                            </select>
                        </td>
                    </tr>
                    <!--Added By BC on 19-feb-016 to provide option for redirect page fot sign up-->
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Sign up Page</th>
                        <td>
                            <select name = "biztech_redirect_signup" id="biztech_redirect_signup">
                                <?php
                                $pages = get_pages();
                                $page = get_page_by_path('portal-sign-up');
                                $signup_page = $page->ID;
                                foreach ($pages as $pagg) {
                                    $option = '<option value="' . $pagg->ID . '" ';
                                    if (get_option('biztech_redirect_signup') == $pagg->ID) {
                                        $option .='selected=selected';
                                    } else if ((get_option('biztech_redirect_signup') == NULL ) && $pagg->ID == $signup_page) {
                                        $option .='selected=selected';
                                    }
                                    $option .= '>';
                                    $option .= $pagg->post_title;

                                    $option .= '</option>';
                                    echo $option;
                                }
                                ?>
                            </select>
                        </td>               
                    </tr>
                    <!--Added By BC on 16-feb-016 to provide option for redirect page for login-->
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Login Page</th>
                        <td><select name = "biztech_redirect_login" id="biztech_redirect_login">
                                <!-- <option value = ""><?php echo attribute_escape(__('Select page'));
                                ?></option> -->
                                <?php
                                $page = get_page_by_path('portal-login');
                                $login_page = $page->ID;
                                $pages = get_pages();
                                foreach ($pages as $pagg) {
                                    $option = '<option value="' . ($pagg->ID) . '" ';
                                    if (get_option('biztech_redirect_login') == ($pagg->ID)) {
                                        $option .='selected=selected';
                                    } else if ((get_option('biztech_redirect_login') == NULL ) && $pagg->ID == $login_page) {
                                        $option .='selected=selected';
                                    }
                                    $option .= '>';
                                    $option .= $pagg->post_title;

                                    $option .= '</option>';
                                    echo $option;
                                }
                                ?>
                            </select></td>
                    </tr>

                    <!--Added By BC on 19-feb-016 to provide option for redirect page for changing profile-->
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Profile Page</th>
                        <td><select name = "biztech_redirect_profile" id="biztech_redirect_profile">
                                <!-- <option value = ""><?php echo attribute_escape(__('Select page'));
                                ?></option> -->
                                <?php
                                $pages = get_pages();
                                $page = get_page_by_path('portal-profile');
                                $profile_page = $page->ID;
                                foreach ($pages as $pagg) {
                                    $option = '<option value="' . ($pagg->ID) . '" ';
                                    if (get_option('biztech_redirect_profile') == ($pagg->ID)) {
                                        $option .='selected=selected';
                                    } else if ((get_option('biztech_redirect_profile') == NULL ) && $pagg->ID == $profile_page) {
                                        $option .='selected=selected';
                                    }
                                    $option .= '>';
                                    $option .= $pagg->post_title;

                                    $option .= '</option>';
                                    echo $option;
                                }
                                ?>
                            </select></td>
                    </tr>
                    <!--Added By BC on 16-feb-016 to provide option for redirect page for forgot password-->
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Forgot Password Page</th>
                        <td><select name = "biztech_redirect_forgotpwd" id="biztech_redirect_forgotpwd">
                                <!-- <option value = ""><?php echo attribute_escape(__('Select page'));
                                ?></option> -->
                                <?php
                                $pages = get_pages();
                                $page = get_page_by_path('portal-forgot-password');
                                $forgot_page = $page->ID;
                                foreach ($pages as $pagg) {
                                    $option = '<option value="' . ($pagg->ID) . '" ';
                                    if ((get_option('biztech_redirect_forgotpwd') != NULL) && (get_option('biztech_redirect_forgotpwd') == ($pagg->ID))) {
                                        $option .='selected=selected';
                                    } else if ((get_option('biztech_redirect_forgotpwd') == NULL ) && $pagg->ID == $forgot_page) {
                                        $option .='selected=selected';
                                    }
                                    $option .= '>';
                                    $option .= $pagg->post_title;

                                    $option .= '</option>';
                                    echo $option;
                                }
                                ?>
                            </select></td>
                    </tr>
                    <!--Added By BC on 16-feb-016 to provide option for Manage Page -->
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Manage Page</th>
                        <td><select name = "biztech_redirect_manange" id="biztech_redirect_manange">
                                <!-- <option value = ""><?php echo attribute_escape(__('Select page'));
                                ?></option> -->
                                <?php
                                $pages = get_pages();
                                $page = get_page_by_path('portal-manage-page');
                                $manage_page = $page->ID;
                                foreach ($pages as $pagg) {
                                    $option = '<option value="' . ($pagg->ID) . '" ';
                                    if (get_option('biztech_redirect_manange') == ($pagg->ID)) {
                                        $option .='selected=selected';
                                    } else if ((get_option('biztech_redirect_manange') == NULL ) && $pagg->ID == $manage_page) {
                                        $option .='selected=selected';
                                    }
                                    $option .= '>';
                                    $option .= $pagg->post_title;

                                    $option .= '</option>';
                                    echo $option;
                                }
                                ?>
                            </select></td>
                    </tr>
                    <!--Added By BC on 30-may-2016 to provide option for Dashboard GUI Page -->
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Dashboard GUI</th>
                        <td><select name = "biztech_redirect_dashboard_gui" id="biztech_redirect_dashboard_gui">
                                <?php
                                $pages = get_pages();
                                $page = get_page_by_path('portal-dashboard-gui');
                                $dashboard_page = $page->ID;
                                foreach ($pages as $pagg) {
                                    $option = '<option value="' . ($pagg->ID) . '" ';
                                    if (get_option('biztech_redirect_dashboard_gui') == ($pagg->ID)) {
                                        $option .='selected=selected';
                                    } else if ((get_option('biztech_redirect_dashboard_gui') == NULL ) && $pagg->ID == $dashboard_page) {
                                        $option .='selected=selected';
                                    }
                                    $option .= '>';
                                    $option .= $pagg->post_title;

                                    $option .= '</option>';
                                    echo $option;
                                }
                                ?>
                            </select></td>
                    </tr>
                    <!--Added by BC on 02-aug-2016 for Single Sign-In-->
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Single Sign-In</th>
                        <td><input type='checkbox' id="biztech_scp_single_signin" value="1" name='biztech_scp_single_signin' <?php
                            if (get_option('biztech_scp_single_signin') == 1) {
                                echo 'checked="checked"';
                            }
                            ?>></td>
                    </tr>
                    <tr valign='top' class="hide_class">
                        <th scope='row'>Portal Template</th>
                        <td><select name = "biztech_portal_template" id="biztech_portal_template">
                                <?php
                                $template_ary = [1 => 'CRM Standalone Page', 0 => 'CRM Full Width Page'];
                                $option = '';
                                foreach ($template_ary as $key11 => $value11) {
                                    $option .= '<option value="' . $key11 . '"';
                                    if ((get_option('biztech_portal_template') != NULL) && (get_option('biztech_portal_template') == $key11)) {
                                        $option .=' selected="selected"';
                                    }
                                    $option .= '>';
                                    $option .= $value11;
                                    $option .= '</option>';
                                }
                                echo $option;
                                ?>
                            </select></td>
                    </tr>
                </table>
                <?php submit_button(); ?>
                <script type="text/javascript">
                    jQuery(function () {
                        jQuery('#submit').click(function (e) {
                            var txtPortalName = jQuery('#txtPortalName').val();
                            if (txtPortalName.length > 0 && !isNaN(txtPortalName)) {
                                alert('Only Numbers are not allowed');
                                return false;
                            } else if (txtPortalName.length > 0 && !(txtPortalName.match(/\w/))) {
                                alert('Only Special characters are not allowed');
                                return false;
                            } else {
                                return true;
                            }
                        });
                    });
                </script>
            </form>
        </div>
        <?php
        $scp_sugar_rest_url = get_option('biztech_scp_rest_url');
        $scp_sugar_username = get_option('biztech_scp_username');
        $scp_sugar_password = get_option('biztech_scp_password');

        if (class_exists('SugarRestApiCall')) {
            $objSCP = new SugarRestApiCall($scp_sugar_rest_url, $scp_sugar_username, $scp_sugar_password);
            if (((get_option('biztech_scp_sugar_crm_version') == 6 || get_option('biztech_scp_sugar_crm_version') == 5) && $objSCP->session_id != '') || (get_option('biztech_scp_sugar_crm_version') == 7 && $objSCP->access_token != '')) {//Added by BC on 02-sep-2015
                //Added by BC on 03-aug-2015 for generate key in option table
                $generated_scp_key = md5($scp_sugar_username . $scp_sugar_password . time());
                if (!get_option('biztech_scp_key')) {
                    add_option('biztech_scp_key', $generated_scp_key);
                    $results = $objSCP->store_wpkey($generated_scp_key);
                } else {
                    $generated_scp_key = get_option('biztech_scp_key');
                    $results = $objSCP->store_wpkey($generated_scp_key);
                }
                //End by BC
                ?>
                <div class='updated settings-error' id='setting-error-settings_updated'> 
                    <p><strong>Connection successful.</strong></p>
                </div>
                <script type="text/javascript">
                    jQuery(".hide_class").show();</script>
                <?php
                $scp_portal_template = get_option('biztech_portal_template');
                if ($scp_portal_template == 0) {
                    $set_portal_template = 'page-crm-fullwidth.php';
                } else if ($scp_portal_template == 1) {
                    $set_portal_template = 'page-crm-standalone.php';
                }
                update_post_meta(get_option('biztech_redirect_login'), '_wp_page_template', $set_portal_template);
                update_post_meta(get_option('biztech_redirect_signup'), '_wp_page_template', $set_portal_template);
                update_post_meta(get_option('biztech_redirect_profile'), '_wp_page_template', $set_portal_template);
                update_post_meta(get_option('biztech_redirect_forgotpwd'), '_wp_page_template', $set_portal_template);
                update_post_meta(get_option('biztech_redirect_manange'), '_wp_page_template', $set_portal_template);
                update_post_meta(get_option('biztech_redirect_dashboard_gui'), '_wp_page_template', $set_portal_template);
            } else {
                ?>
                <div class='error settings-error' id='setting-error-settings_updated'> 
                    <p><strong>Connection not successful. Please check SugarCRM Version, URL, Username and Password.</strong></p>
                </div>
                <script type="text/javascript">
                    jQuery(".hide_class").hide();</script>
                <?php
            }
        } else {
            ?>
            <div class='error settings-error' id='setting-error-settings_updated'> 
                <p><strong>Connection not successful. Please check SugarCRM Version, URL, Username and Password.</strong></p>
            </div>
            <script type="text/javascript">
                jQuery(".hide_class").hide();
            </script>
            <?php
        }
    } else {//else CURL CHECKING 
        ?>
        <div class='error settings-error' id='setting-error-settings_updated'> 
            <p><strong><?php echo $check_var; ?></strong></p>
        </div>
        <?php
    }
}

add_action('init', 'sugar_crm_portal_start_session', 1);  // start session

function sugar_crm_portal_start_session() {
    if (!session_id()) {
        session_save_path("/tmp"); //wpdemo server its needed
        session_start();
    }
}

if (isset($_REQUEST['logout']) == 'true') {    // logout 

    function sugar_crm_portal_logout() {
        unset($_SESSION['scp_user_id']);
        unset($_SESSION['scp_user_account_name']);
        unset($_SESSION['module_array']);
        unset($_SESSION['user_timezone']);
        unset($_SESSION['assigned_user_list']);
        unset($_SESSION['user_date_format']);
        unset($_SESSION['user_time_format']);
        $redirect_url = explode('?', $_SERVER['REQUEST_URI'], 2);
        $redirect_url = $redirect_url[0];

        if (get_option('biztech_scp_single_signin') != '' && is_user_logged_in()) {//If user logged in than logout
            wp_logout();
        }

        wp_redirect($redirect_url);
        exit;
    }

    add_action('init', 'sugar_crm_portal_logout', 1);
}

function scp_deleteDirectory($dir) {
    if (!file_exists($dir)) {
        return true;
    }

    if (!is_dir($dir)) {
        return unlink($dir);
    }

    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') {
            continue;
        }

        if (!scp_deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
            return false;
        }
    }

    return rmdir($dir);
}
?>