//click on menus
jQuery(document).ready(function () {
    if (jQuery('#toggle').is(":visible")) {//default close on standalone
            jQuery(".scp-leftpanel").toggle();
            jQuery(this).toggleClass('toggle-icon-wrapper');
        }
    jQuery('.scp-sidebar-class').on('click', function (e) {

        //jQuery('.scp-sidebar-class').removeClass('noborder');
        var that = jQuery(this),
                id = that.attr('id');
        var clas = that.attr('class');
        if (!that.hasClass("noborder"))
        {
            jQuery(".inner_ul").not("#dropdown_" + id).slideUp();
            jQuery('.scp-active-menu').not("#" + id + " a.label").removeClass('scp-active-menu').addClass('scp-sidemenu');

            if (typeof id !== 'undefined')
            {
                var div = '#dropdown_' + id;
                if (jQuery("#" + id + " a.label").hasClass('scp-sidemenu')) {
                    jQuery("#" + id + " a.label").removeClass('scp-sidemenu');
                }
                jQuery('#dashboard_id a').addClass('scp-active-menu');
                jQuery("#" + id + " a.label").addClass('scp-active-menu');
                that.toggleClass('noborder');
                jQuery('.scp-sidebar-class').not("#" + id).removeClass('noborder');
                jQuery(div).slideToggle('fast')
            }
        }
//        var getid = jQuery(this).attr('id');
//        jQuery('#dropdown_' + getid).click(function () {
//            if (jQuery('#toggle').is(":visible")) {
//                jQuery(".scp-leftpanel").toggle();
//                jQuery(this).toggleClass('toggle-icon-wrapper');
//            }
//        });

    });



    jQuery('.inner_ul li').click(function () {
        if (jQuery('#toggle').is(":visible")) {
            jQuery(".scp-leftpanel").toggle();
            jQuery(this).toggleClass('toggle-icon-wrapper');
        }
    });

    jQuery(".scp-bar-toggle").click(function () {

        // $("body").toggleClass("toggled-on");
        //jQuery(".scp-leftpanel").toggle("slide");
        jQuery("body").toggleClass("toggled-on", 500);
        jQuery(".container").toggleClass("menu-show", 500);

    });

});