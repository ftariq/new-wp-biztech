<?php

add_action('admin_post_bcp_sign_up', 'prefix_admin_bcp_sign_up'); // Sign Up      
add_action('admin_post_nopriv_bcp_sign_up', 'prefix_admin_bcp_sign_up'); // Sign Up

function prefix_admin_bcp_sign_up() {
    global $objSCP;
    if ($objSCP != 0) {
        $username_c = stripslashes_deep($_REQUEST['add-signup-username']);
        $password_c = stripslashes_deep($_REQUEST['add-signup-password']);
        $first_name = stripslashes_deep($_REQUEST['add-signup-first-name']);
        $last_name = stripslashes_deep($_REQUEST['add-signup-last-name']);
        $phone_work = stripslashes_deep($_REQUEST['add-signup-work-phone']);
        $phone_mobile = stripslashes_deep($_REQUEST['add-signup-mobile']);
        $phone_fax = stripslashes_deep($_REQUEST['add-signup-fax']);
        $title = stripslashes_deep($_REQUEST['add-signup-title']);
        $email1 = stripslashes_deep($_REQUEST['add-signup-email-address']);
        $primary_address_street = stripslashes_deep($_REQUEST['add-signup-primary-address']);
        $biztech_redirect_login = get_page_link(get_option('biztech_redirect_login'));

        $username_c = sanitize_user($username_c, true);
        if (isset($username_c) && $username_c != '') {//username is not blank then
            $addSignUp = array(
                'username_c' => $username_c,
                'password_c' => $password_c,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'phone_work' => $phone_work,
                'title' => $title,
                'phone_mobile' => $phone_mobile,
                'phone_fax' => $phone_fax,
                'email1' => $email1,
                'primary_address_street' => $primary_address_street,
            );
            //check user exists
            $checkUserExists = $objSCP->getUserInformationByUsername($username_c);
            $checkEmailExists = $objSCP->getPortalEmailExists($email1);

            //check username

            $user_id = username_exists($username_c);
            $wp_email_exist = email_exists($email1);

            if (($checkUserExists) && ($checkEmailExists) && ($user_id) && $wp_email_exist) {
                $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
                $signup_msg = "Username and Email Address already exists.";
                setcookie('bcp_signup_err', $signup_msg, time() + 30, '/');
                wp_redirect($redirect_url);
            } else if ($checkUserExists || $user_id) {
                $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
                $signup_msg = "Username already exists.";
                setcookie('bcp_signup_err', $signup_msg, time() + 30, '/');
                wp_redirect($redirect_url);
            } else if ($checkEmailExists || $wp_email_exist) {
                $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
                $signup_msg = "Email Address already exists.";
                setcookie('bcp_signup_err', $signup_msg, time() + 30, '/');
                wp_redirect($redirect_url);
            } else if (!is_email($email1)) {//check proper email validation
                $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
                $signup_msg = "This email id is invalid because it uses illegal characters. Please enter a valid email id.";
                setcookie('bcp_signup_err', $signup_msg, time() + 30, '/');
                wp_redirect($redirect_url);
            } else {
                $isSignUp = $objSCP->set_entry('Contacts', $addSignUp);
                if ($isSignUp != NULL) {
                    //create user in wp when entry entered in 

                    $new_user_id = wp_insert_user(array(
                        'user_login' => $username_c,
                        'user_pass' => $password_c,
                        'user_email' => $email1,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                            )
                    );
                    mail_user($username_c, $password_c, $first_name, $last_name, $email1); //Send user mail
                    $signup_msg = "You are successfully sign up.";
                    setcookie('bcp_signup_suc', $signup_msg, time() + 30, '/');
                    if (isset($biztech_redirect_login) && !empty($biztech_redirect_login)) {

                        $redirect_url = $biztech_redirect_login . '?signup=true';
                    } else {
                        $redirect_url = home_url() . '/portal-login?signup=true';
                    }
                    wp_redirect($redirect_url);
                }
            }
        } else {//if username is blank then
            $signup_msg = "This username is invalid because it uses illegal characters. Please enter a valid username.";
            setcookie('bcp_signup_err', $signup_msg, time() + 30, '/');
            $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
            wp_redirect($redirect_url);
        }
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        $redirect_url = $_REQUEST['scp_current_url'] . '?conerror=1';
        wp_redirect($redirect_url);
    }
}

//For Login action
add_action('admin_post_bcp_login', 'prefix_admin_bcp_login'); // login
add_action('admin_post_nopriv_bcp_login', 'prefix_admin_bcp_login'); // login

function prefix_admin_bcp_login() {

    global $objSCP, $wpdb, $sugar_crm_version;
    if ($objSCP != 0) {
        if (get_option('biztech_scp_single_signin') != '' && is_user_logged_in()) {//Added by BC on 02-aug-2016
            global $current_user;
            $scp_username = $current_user->user_login;
            $scp_password = $current_user->user_email;
            $isLogin = $objSCP->PortalLogin($scp_username, $scp_password, 1);
        } else {
            $scp_username = $_REQUEST['scp_username'];
            $scp_password = $_REQUEST['scp_password'];
            $isLogin = $objSCP->PortalLogin($scp_username, $scp_password);
        }
        if (($isLogin->records[0] != NULL) && ($scp_username != NULL) && ($scp_password != NULL)) {
            $_SESSION['scp_user_id'] = $isLogin->records[0]->id;
            $_SESSION['scp_user_account_name'] = $isLogin->records[0]->username_c;
            //////////////Added by BC on 18-jun-2016 store module layout & user timezoone in session////////////////////////////
            //modules list
            $modules_array = $objSCP->getPortal_accessibleModules(); //Added by BC on 16-jun-2016
            if (!isset($modules_array->contact_group) && $modules_array->contact_group == '') {//if contact group is blank then 
                wp_die('Please contact your administrator to request permission to use this.');
            }
            if (!empty($modules_array->layouts)) {//Insert or update layout
                foreach ($modules_array->layouts as $k_module => $v_module_list) {
                    foreach ($v_module_list as $k_view => $v_panel) {
                        $panel_data = json_encode($v_panel->panels);
                        if (!is_object($v_panel)) {
                            $panel_data = $v_panel;
                        }
                        //get record exists
                        $sql_count = "SELECT id,date,COUNT(*) as count FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $k_module . "' AND layout_view='" . $k_view . "' AND contact_group='" . $modules_array->contact_group . "'";
                        $result_count = $wpdb->get_row($sql_count) or die(mysql_error());
                        $rowCount = $result_count->count;

                        if ($rowCount > 0) {
                            $db_date = $result_count->date;
                            $record_id = $result_count->id;
                            if (strtotime($v_panel->modified_date) > strtotime($db_date)) {
                                $wpdb->query("UPDATE " . $wpdb->prefix . "module_layout SET version='$sugar_crm_version',module_name='$k_module',layout_view='$k_view',json_data='$panel_data',date='$v_panel->modified_date',contact_group ='$modules_array->contact_group' WHERE id='$record_id'");
                            }
                        } else {
                            $wpdb->insert($wpdb->prefix . 'module_layout', array(
                                'version' => $sugar_crm_version,
                                'module_name' => $k_module,
                                'layout_view' => $k_view,
                                'json_data' => $panel_data,
                                'date' => $v_panel->modified_date,
                                'contact_group' => $modules_array->contact_group
                            ));
                        }
                    }
                }
            }
            $_SESSION['module_array'] = $modules_array->accessible_modules;
            //currency array store in session
            $_SESSION['Currencies'] = $modules_array->Currencies;
            //sugar version
            $_SESSION['sugar_version'] = $modules_array->sugar_version;
            //Contact group
            $_SESSION['contact_group'] = $modules_array->contact_group;
            //texRates
            $_SESSION['TatRates'] = $modules_array->TatRates;
            //timezone
            $user_timezone = $objSCP->getUserTimezone();
            $_SESSION['user_timezone'] = $user_timezone;
            $_SESSION['user_date_format'] = $user_timezone->date;
            $_SESSION['user_time_format'] = $user_timezone->time;
            //assigned user id list
            $record_detailUsers = $objSCP->getModuleRecords("Users", "id,name");
            $_SESSION['assigned_user_list'] = $record_detailUsers;
            //////////////////////////////////////////////////////
            //get option to redirect to which page after login
            if (get_page_link(get_option('biztech_redirect_dashboard_gui')) != NULL) {
                $redirect_url = get_page_link(get_option('biztech_redirect_dashboard_gui'));
            } else {
                $redirect_url = home_url() . "/portal-dashboard-gui/";
            }
            /* if wp user is not logged in login by auth cookie  */
            if (get_option('biztech_scp_single_signin') != '' && !is_user_logged_in()) {
                // Automatic login, without any password! //
                $user = get_user_by('login', $scp_username);
                if (!is_wp_error($user)) {
                    wp_clear_auth_cookie();
                    wp_set_current_user($user->ID);
                    wp_set_auth_cookie($user->ID);
                }
            }
            /* if wp user is not logged in */
            wp_redirect($redirect_url);
            exit();
        } else {
            $ERROR_CODE = (isset($isLogin->error)) ? $isLogin->error : 1;
            $set_global_loginerr = array(
                '1' => 'Invalid Username OR Password.',
                '201' => 'Please contact your Administrator to validate License.',
                '202' => 'There seems some error while validating your license for Customer Portal Pro. Please try again later.'
            );
            setcookie('bcp_login_error', $set_global_loginerr[$ERROR_CODE], time() + 30, '/');
            $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
            if (get_option('biztech_scp_single_signin') != '' && is_user_logged_in()) {
                setcookie('bcp_auth_error', 'You do not have sufficient permission to access this page.', time() + 30, '/');
                if (!isset($_REQUEST['error'])) {
                    wp_redirect($redirect_url);
                }
            } else {
                wp_redirect($redirect_url);
            }
        }
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        $redirect_url = $_REQUEST['scp_current_url'] . '?conerror=1';
        wp_redirect($redirect_url);
    }
}

//For Forgot Password
add_action('admin_post_bcp_forgot_password', 'prefix_admin_bcp_forgot_password');
add_action('admin_post_nopriv_bcp_forgot_password', 'prefix_admin_bcp_forgot_password');

function prefix_admin_bcp_forgot_password() {
    global $objSCP;
    if ($objSCP != 0) {

        $checkUsername = stripslashes_deep($_REQUEST['forgot-password-username']);
        $checkEmialAddress = stripslashes_deep($_REQUEST['forgot-password-email-address']);

        $checkUserExists = $objSCP->getUserInformationByUsername($checkUsername);
        $username = $checkUserExists->records[0]->username_c;
        $emailAddress = $checkUserExists->records[0]->email1;
        $getAdminEmail = get_option('admin_email');
        if (($username == $checkUsername) && ($emailAddress == $checkEmialAddress)) {
            $password = $checkUserExists->records[0]->password_c;
            if (get_option('biztech_scp_name') != NULL) {
                $headers = "From: " . get_option('biztech_scp_name') . " <$getAdminEmail>' . '\r\n";
            } else {
                $headers = "From: <$getAdminEmail>' . '\r\n";
            }

            if (get_page_link(get_option('biztech_redirect_login')) != NULL) {
                $url = get_page_link(get_option('biztech_redirect_login'));
            } else {
                $url = home_url() . "/portal-login/";
            }
            // $isSendEmail = wp_mail($emailAddress, $subject, $body, $headers);
            $isSendEmail = mail_user_forgotpwd($username, $password, $url, $emailAddress); //Send user mail
            $redirect_url = $_REQUEST['scp_current_url'];
            if ($isSendEmail == true) {
                $redirect_url = $_REQUEST['scp_current_url'] . '?sucess=1';
                $disply_msg = "Your password has been sent successfully.";
                setcookie('bcp_frgtpwd', $disply_msg, time() + 30, '/');
                wp_redirect($redirect_url);
            } else {
                $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
                $disply_msg = "Failed to send your your password. Please try later or contact the administrator by another method.";
                setcookie('bcp_frgtpwd', $disply_msg, time() + 30, '/');
                wp_redirect($redirect_url);
            }
        } else if (($username == $checkUsername) && ($emailAddress != $checkEmialAddress)) {
            $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
            $disply_msg = "Your email address does not match.";
            setcookie('bcp_frgtpwd', $disply_msg, time() + 30, '/');
            wp_redirect($redirect_url);
        } else {
            $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
            $disply_msg = "Your username does not exists.";
            setcookie('bcp_frgtpwd', $disply_msg, time() + 30, '/');
            wp_redirect($redirect_url);
        }
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        $redirect_url = $_REQUEST['scp_current_url'] . '?conerror=1';
        wp_redirect($redirect_url);
    }
}

//Get user group module list
add_action('wp_ajax_get_user_group_modules', 'get_user_group_modules_callback');
add_action('wp_ajax_nopriv_get_user_group_modules', 'get_user_group_modules_callback');

function get_user_group_modules_callback() {
    global $objSCP, $sugar_crm_version;
    $sess_id = '';
    if ($sugar_crm_version == 7) {
        $sess_id = $objSCP->access_token;
    }
    if ($sess_id != '') {
        //if ($objSCP != 0) {
        $user_group = $_REQUEST['user_group'];
        $list_result = $objSCP->getRecordDetail("bc_wp_user_group", $user_group);
        $accessable_modules = $list_result->accessible_modules;
        if (count($accessable_modules) > 0) {
            $final_str = implode(',', $accessable_modules);
        } else {
            $final_str = "-";
        }
        $html = "<br><b>Accessible module list</b> : " . $final_str . "";
        echo $html;
        wp_die();
    }
}

//get module list for v7 // get related value (ex :acccount -> its names)
add_action('wp_ajax_get_module_list_v7', 'get_module_list_v7_callback');
add_action('wp_ajax_nopriv_get_module_list_v7', 'get_module_list_v7_callback');

function get_module_list_v7_callback() {
    global $objSCP;
    $sess_id = $objSCP->access_token;
    if ($sess_id != '') {
        $logged_user_id = $_SESSION['scp_user_id'];

        $module_name = $_REQUEST['parent_name'];
        $select_fields = "id,name";
        $where_condition = array();
        $record_detail = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $select_fields, $where_condition, '', '', 'date_entered:DESC');
        $html = "";
        $html .= "<select id='parent_id' name='parent_id' class='scp-form-control'><option value=''></option>";
        if (isset($record_detail->records)) {
            for ($m = 0; $m < count($record_detail->records); $m++) {
                $mod_id = $record_detail->records[$m]->id;
                $mod_name = $record_detail->records[$m]->name;
                $html .= "<option value=" . $mod_id . ">" . $mod_name . "</option>";
            }
        }
        $html .= "</select>";

        echo $html;
        wp_die();
    }
}

//get Doc Attachment action
add_action('admin_post_bcp_get_doc_attachment', 'prefix_admin_bcp_get_doc_attachment');   // Get Doc Attachment
add_action('admin_post_nopriv_bcp_get_doc_attachment', 'prefix_admin_bcp_get_doc_attachment');   // Get Doc Attachment

function prefix_admin_bcp_get_doc_attachment() {
    global $objSCP;
    if ($objSCP != 0) {
        $doc_id = $_REQUEST['scp_doc_id'];
        $objSCP->getDocumentAttachment($doc_id);
    } else {
        echo "<div class='error settings-error' id='setting-error-settings_updated'> 
            <p><strong>Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.</strong></p>
        </div>";
    }
}

//For add/edit records layout in modules
add_action('wp_ajax_bcp_add_module', 'wp_ajax_bcp_add_module');
add_action('wp_ajax_nopriv_bcp_add_module', 'wp_ajax_bcp_add_module');

function wp_ajax_bcp_add_module() {
    global $objSCP, $wpdb, $sugar_crm_version;
    $html = '';
    if ($sugar_crm_version == 7) {
        $sess_id = $objSCP->access_token;
    }
    if ($sess_id != '') {
        $module_name = $_POST['modulename'];
        if (is_array((array) $_SESSION['module_array'])) {//Added for getting module key OR lable added from sugar side
            $arry_lable = (array) $_SESSION['module_array'];
            $module_name_label = $arry_lable[$module_name];
        }
        $view = $_POST['view'];
        $current_url = $_POST['current_url'];
        if ($current_url != '' && !empty($current_url)) {
            if (strpos($current_url, 'detail') !== false) {
                $id = Null;
            } else {
                $current_url = explode('?', $current_url, 2);
                $current_url = $current_url[0];
                if (isset($_POST['id'])) {
                    $id = $_POST['id'];
                } else {
                    $id = '';
                }
            }
        }
        if (isset($_POST['success'])) {
            $success = $_POST['success'];
        } else {
            $success = '';
        }
        //Added by BC on 20-jun-2016 for list view-----------------
        $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $_SESSION["contact_group"] . "'";
        $result_count = $wpdb->get_row($sql_count) or die(mysql_error());

        $json_data = $result_count->json_data;
        $results = json_decode($json_data);
        //End by BC on 20-jun-2016-----------------
        //edit start-------
        if (isset($id) && !empty($id)) {
            $record_detail = $objSCP->getRecordDetail($module_name, $id);
        }
        //edit end---------
        $type_arr = array();
        $text_sugar_array = array('name', 'phone', 'url', 'phone_fax', 'varchar', 'file', 'datetimecombo', 'datetime','currency_id');
        $select_options_sugar_array = array('enum', 'multienum', 'dynamicenum'); //Updated by BC on 16-nov-2015 remove parent flag
        $select_relate_options_sugar_array = array('relate');
        $textarea_sugar_array = array('text');
        $check_box_sugar_array = array('bool');
        if (isset($record_detail->parent_type)) {
            $parent_type = $record_detail->parent_type;
            $parent_id = $record_detail->parent_id;
        } else {
            $parent_type = $parent_id = '';
        }if (isset($_POST['casenote'])) {
            $casenote = $_POST['casenote'];
        } else {
            $casenote = '';
        }

        $module_without_s = $module_name_label;
        if ($module_name == "Notes" && $casenote != null) {
            $html = "";
        } else {

            $html .= "<form action='" . home_url() . "/wp-admin/admin-post.php' method='post' id='doc_submit'>
            <input type='hidden' name='action' value='bcp_get_doc_attachment'>
            <input type='hidden' name='scp_doc_id' value='' id='scp_doc_id'>
            </form>";
            // note attchment
            $html .= "<form action='" . home_url() . "/wp-admin/admin-post.php' method='post' id='download_note_id'>
            <input type='hidden' name='action' value='bcp_get_note_attachment'>
            <input type='hidden' name='scp_note_id' value='' id='scp_note_id'>
            </form>";
        }

        if (!empty($results)) {
            include( TEMPLATE_PATH . 'bcp_add_page_v7.php');
        } else {
            $html.= $json_data;
        }
        //layout not set
        if ($module_name == "Notes") {//Added by BC on 11-aug-2015
            $html .= "";
        } else {
            $html .= "</div>";
        }
        //added for validation support in all browsers
        $html .= "<script type='text/javascript'>
             jQuery('#filename').bind('change', function() {

              //this.files[0].size gets the size of your file.
              if(this.files[0].size > 2097152){//2 MB size allowed
                alert('Attachment file size is not proper, Maximum file size can be 2 MB.');
                jQuery('#filename').val('');
                return false;
              }
            });
        $('#general_form_id').validate();
//        to display datetimepicker in start date and due date
         jQuery('.datetimepicker').datetimepicker();
          //to display date cakender for document
          jQuery('#active_date').datepicker({
                    dateFormat : 'yy-mm-dd'
            });
           
            jQuery('#exp_date').datepicker({
                    dateFormat : 'yy-mm-dd'
            });
            jQuery('#birthdate').datepicker({
                    dateFormat : 'yy-mm-dd'
            });
            jQuery('#duration_hours').keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        // Allow: Ctrl+A, Command+A
                        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                        // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
            });
            
jQuery('#add-doc-publish-date').datepicker({
                    dateFormat : 'yy-mm-dd'
            });
            //for add new note
            jQuery('#add-doc-expire-date').datepicker({
                    dateFormat : 'yy-mm-dd'
            });
            
//for add opportunity
jQuery('#date_closed').datepicker({
                    dateFormat : 'yy-mm-dd'
            });
            jQuery('#amount').keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        // Allow: Ctrl+A, Command+A
                        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                        // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
            });
           
         
                         </script>";

        echo $html;
        wp_die();
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        $redirect_url = $_REQUEST['scp_current_url'] . '?conerror=1';
        wp_redirect($redirect_url);
    }
}

//For list records in modules
add_action('wp_ajax_bcp_list_module', 'wp_ajax_bcp_list_module');
add_action('wp_ajax_nopriv_bcp_list_module', 'wp_ajax_bcp_list_module');

function wp_ajax_bcp_list_module() {
    global $objSCP, $wpdb, $sugar_crm_version;
    $where_con = array();
    $search_name = $deleted = '';
    if ($sugar_crm_version == 7) {
        $sess_id = $objSCP->access_token;
    }
    if ($sess_id != '') {
        $result_timezone = '';
        $module_name = $_POST['modulename'];
        if (is_array((array) $_SESSION['module_array'])) {//Added for getting module key OR lable added from sugar side
            $arry_lable = (array) $_SESSION['module_array'];
            $module_name_label = $arry_lable[$module_name];
        }
        $view = $_POST['view'];
        $limit = get_option('biztech_scp_case_per_page');
        $page_no = $_POST['page_no'];
        $offset = ($page_no * $limit) - $limit;
        $current_url = $_POST['current_url'];
        if ($current_url != '' && !empty($current_url)) {
            $current_url = explode('?', $current_url, 2);
            $current_url = $current_url[0];
        }
        //Updated by BC on 20-jun-2016 for timezone store in session
        if (empty($_SESSION['user_timezone'])) {
            $result_timezone = $objSCP->getUserTimezone();
        } else {
            $result_timezone = $_SESSION['user_timezone'];
        }
        date_default_timezone_set($result_timezone->timezone);
        $result_timezone = date_default_timezone_get();
        $order_by = trim($_POST['order_by']);
        $order = trim($_POST['order']);
        if (isset($_POST['searchval']))
            $search_name = trim($_POST['searchval']);
        if (isset($search_name) && !empty($search_name)) {
            if ($module_name == "Contacts" || $module_name == "Leads") {
                $where_con = array(
                    '$or' => array(
                        array(
                            "first_name" => array(
                                '$contains' => $search_name,
                            )
                        ),
                        array(
                            "last_name" => array(
                                '$contains' => $search_name,
                            )
                        )
                    ),
                );
            } elseif ($module_name == "Documents") {
                $where_con = array(
                    "document_name" => array(
                        '$contains' => $search_name,
                    ),
                );
            } elseif ($module_name == "KBDocuments") {
                $where_con = array(
                    "kbdocument_name" => array(
                        '$contains' => $search_name,
                    ),
                );
            } else {

                $where_con = array(
                    "name" => array(
                        '$contains' => $search_name,
                    ),
                );
            }
        }
        //Added by BC on 20-jun-2016 for list view-----------------
        $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $_SESSION["contact_group"] . "'";
        $result_count = $wpdb->get_row($sql_count) or die(mysql_error());

        $json_data = $result_count->json_data;
        $results = json_decode($json_data);
        //End by BC on 20-jun-2016-----------------
        if (!empty($results) || $module_name == 'KBDocuments') {
            if (isset($order_by) == true && !empty($order_by) && isset($order) == true && !empty($order)) {
                $order_by_query = "$order_by:$order";
            } else {
                $order_by_query = "date_entered:DESC";
            }
            $id_show = false;
            foreach ($results as $key_n => $val_n) {
                if ($key_n == 'ID') {
                    $id_show = true;
                }
                if (isset($val_n->related_module)) {
                    if ($val_n->type == 'relate' && ($val_n->related_module != 'Users' && !in_array($val_n->related_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array 
                        continue;
                    }
                }
                $n_array[] = strtolower($key_n);
                if ($val_n->type == 'relate' && $key_n == 'RELATED_DOC_NAME') {//for related doc name
                    $n_array[] = 'related_doc_id';
                }
            }
            if (!in_array('id', $n_array)) {
                array_push($n_array, 'id');
            }
            if ($module_name == 'Quotes') {
                array_push($n_array, 'currency_id'); //Added by BC on 02-jul-2016 for getting currency id
            }
            $n_array_str = implode(',', $n_array);
//            $list_result_count_all = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $n_array_str, $where_con, '', '', $order_by_query);
//            $list_result = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $n_array_str, $where_con, $limit, $offset, $order_by_query);
            if ($module_name == 'KBDocuments') {//Added by BC on 22-jun-2016 for KBDocument
                if (isset($_SESSION['sugar_version']) && $_SESSION['sugar_version'] == '7.7.0.0') {//if sugar 7 version 7.7
                    $list_result_count_all = $objSCP->get_entry_list_custom('KBContents', '', $where_con, '', '', $order_by_query);
                    $list_result = $objSCP->get_entry_list_custom('KBContents', '', $where_con, $limit, $offset, $order_by_query);
                }else{
                    $n_array_str = 'id,kbdocument_name,date_entered,description,attachment_list';
                    $list_result_count_all = $objSCP->get_entry_list_custom($module_name, $n_array_str, $where_con, '', '', $order_by_query);
                    $list_result = $objSCP->get_entry_list_custom($module_name, $n_array_str, $where_con, $limit, $offset, $order_by_query);
                }
            } else {
                $list_result_count_all = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $n_array_str, $where_con, '', '', $order_by_query);
                $list_result = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $n_array_str, $where_con, $limit, $offset, $order_by_query);
            }

            $cnt = count($list_result->records);
            $countCases = count($list_result_count_all->records);
            $html = "";
            $html .= "<form action='" . home_url() . "/wp-admin/admin-post.php' method='post' id='download_note_id'>
            <input type='hidden' name='action' value='bcp_get_note_attachment'>
            <input type='hidden' name='scp_note_id' value='' id='scp_note_id'>
            </form>";
            $html .= "
            <form action='" . home_url() . "/wp-admin/admin-post.php' method='post' id='doc_submit'>
            <input type='hidden' name='action' value='bcp_get_doc_attachment'>
            <input type='hidden' name='scp_doc_id' value='' id='scp_doc_id'>
            </form>";

            $html .= "<form action = '" . home_url() . "/wp-admin/admin-post.php' method = 'post' enctype = 'multipart/form-data' id = 'actionform'>
            <input type = 'hidden' name = 'action' value = 'bcp_module_call_add'>
            <input type = 'hidden' name = 'scp_current_url' value = '" . $current_url . "'>
            <input type = 'hidden' name = 'id' value = '' id = 'scp_id'>
            <input type = 'hidden' name = 'delete' value = '1'>
            <input type = 'hidden' name = 'module_name' value = '" . $module_name . "'>
            </form>";

            $html .="<div class='scp-action-header'><div class='scp-action-header-left'><h3 class='fa " . $module_name . " side-icon-wrapper scp-$module_name-font scp-default-font'>" . strtoupper($module_name_label) . "</h3>";
            $list_result_count_all = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], 'accounts', 'id', array(), '', '', 'date_entered:DESC');
            $countAcc = count($list_result_count_all->records);
            if (($module_name == 'Accounts' && $countAcc > 0 ) || $module_name == 'Quotes' || $module_name == 'KBDocuments') {
                
            } else {
                //for add
                $html .= "<a  class='scp-$module_name scp-default-bg-btn' href='javascript:void(0);' onclick='bcp_module_call_add(0,\"$module_name\",\"edit\",\"\",\"\",\"\");'><span class='fa fa-plus side-icon-wrapper'></span><span>ADD " . strtoupper($module_name_label) . "</span></a>";
            }
            $html .="</div>";
            if ($module_name != 'Accounts' && $module_name != 'KBDocuments') {
                //for Serach
                $html .= "<form method = 'post' enctype = 'multipart/form-data' id = 'actionform_search' onsubmit = \"return false;\" class='actionform_search'>
                    <div class='search-box'>
            <input type='text' name='search_name' value='" . $search_name . "' id='search_name' placeholder='Search $module_name_label' class='search-input'/>
            <a href='javascript:;' onclick='bcp_module_paging(0,\"$module_name\",1,\"\",\"\",\"$view\",\"$current_url\")' id='search_btn_id' class='hover active scp-button search-btn'><i class='fa fa-search' aria-hidden='true'></i>SEARCH</a>
                </div>
            <a id='clear_btn_id' onclick='bcp_clear_search_txtbox(0,\"$module_name\",\"\",\"\",\"\",\"$view\",\"$current_url\");' href='javascript:;'><i class='fa fa-remove'></i>CLEAR</a>
            </form>
            <script>
            jQuery('#search_name').keypress(function(event){
    
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                    bcp_module_paging(0,\"$module_name\",1,\"\",\"\",\"$view\",\"$current_url\");	
            }
            event.stopPropagation();
            });
            </script>";
            }


            $html .= "</div>";
            if ($cnt > 0) {
                $html .= "<div class='scp-page-action-title'>View $module_name_label </div>";
            }
            $get_param = $_SERVER['QUERY_STRING'];
            if (isset($get_param) && !empty($get_param) || $current_url != '') {
//                if (strpos($get_param, 'list') === false) {
//                    
//                } else
                {
                    if (isset($_COOKIE['bcp_add_record']) && $_COOKIE['bcp_add_record'] != '') {
                        $cookie_err = $_COOKIE['bcp_add_record'];
                        unset($_COOKIE['bcp_add_record']);
                        $html .= "<span class='success' id='succid'>" . $cookie_err . "</span>";
                    }
                }
            }
            if ($deleted == 1 && isset($success) == true) {
                $html .= "<span class='success'> " . rtrim($module_name, 's') . " Deleted Successfully.</span>";
            }
            $html .= "<div class='scp-table-responsive'>";
            if ($module_name == 'KBDocuments') {//Addded by BC on 23-jun-2016
                //Added by BC on 22-jun-2016 for KB Doc attachment
                $html .= "<form action='" . home_url() . "/wp-admin/admin-post.php' method='post' id='kbdoc_submit'>
            <input type='hidden' name='action' value='bcp_get_kbdoc_attachment'>
            <input type='hidden' name='scp_kbdoc_id' value='' id='scp_kbdoc_id'>
            </form>";
                include( TEMPLATE_PATH . 'bcp_list_page_kb.php');
            }
            if ($cnt > 0) {
                if ($module_name != 'KBDocuments') {//Addded by BC on 23-jun-2016
                    $html .= "<table id='example' class='display scp-table scp-table-striped scp-table-bordered scp-table-hover' cellspacing='0' width='100%'>";
                    $name_arry = array();
                    $module_without_s = strtolower(rtrim($module_name, 's'));
                    include( TEMPLATE_PATH . 'bcp_list_page_v7.php');
                    $html .="</table>";
                }
                $html .="</div>";
                $pagination_url = "?";
                $view1 = 'list';
                $html .= pagination($countCases, $limit, $page_no, $pagination_url, $module_name, $order_by, $order, $view1, $current_url);
            } else {
                $html .= "<strong>No Record(s) Found.</strong>";
                $html .="</div>";
            }
        } else {
            $html.= $json_data;
        }
        echo $html;
        wp_die();
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        echo "-1";
        wp_die();
    }
}

//For viewing details of records 
add_action('wp_ajax_bcp_view_module', 'wp_ajax_bcp_view_module');
add_action('wp_ajax_nopriv_bcp_view_module', 'wp_ajax_bcp_view_module');

function wp_ajax_bcp_view_module() {
    global $objSCP, $wpdb, $sugar_crm_version;
    $sess_id = $objSCP->access_token;
    if ($sess_id != '') {
        $module_name = $_POST['modulename'];
        $view = $_POST['view'];
        $id = $_POST['id'];
        $current_url = $_POST['current_url'];
        if ($current_url != '' && !empty($current_url)) {
            $current_url = explode('?', $current_url, 2);
            $current_url = $current_url[0];
        }
        $html = '';
        //Added by BC on 20-jun-2016 for list view-----------------
        $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $_SESSION["contact_group"] . "'";
        $result_count = $wpdb->get_row($sql_count) or die(mysql_error());

        $json_data = $result_count->json_data;
        $results = json_decode($json_data);
        //End by BC on 20-jun-2016-----------------
        //Updated by BC on 20-jun-2016 for timezone store in session
        if (empty($_SESSION['user_timezone'])) {
            $result_timezone = $objSCP->getUserTimezone();
        } else {
            $result_timezone = $_SESSION['user_timezone'];
        }
        date_default_timezone_set($result_timezone->timezone);
        $result_timezone = date_default_timezone_get();
        $record_detail = $objSCP->getRecordDetail($module_name, $id);
        $name = $record_detail->name;
        if ($module_name == 'Quotes') {//Added by BC on 16-july-2016 for storing currency
            $curreny_id = $record_detail->currency_id;
            if (!empty($curreny_id)) {
                $currency_symbol = $_SESSION['Currencies']->$curreny_id->symbol;
                $currency_iso4217 = $_SESSION['Currencies']->$curreny_id->iso4217;
            }
        }
        if (!empty($results)) {
            // for not set layout
            $html .= "<form action='" . home_url() . "/wp-admin/admin-post.php' method='post' id='doc_submit'>
            <input type='hidden' name='action' value='bcp_get_doc_attachment'>
            <input type='hidden' name='scp_doc_id' value='' id='scp_doc_id'>
            </form>";
            // note attchment
            $html .= "<form action='" . home_url() . "/wp-admin/admin-post.php' method='post' id='download_note_id'>
            <input type='hidden' name='action' value='bcp_get_note_attachment'>
            <input type='hidden' name='scp_note_id' value='' id='scp_note_id'>
            </form>";
            include( TEMPLATE_PATH . 'bcp_view_page_v7.php');
            if ($module_name == 'Quotes') {//Added by BC on 06-jul-2016
                $quote_entry = $objSCP->get_entry_quotes($module_name, $id);
                include( TEMPLATE_PATH . 'bcp_view_page_v7_product_bundle.php');
            }
        } else {
            $html .= $json_data;
        }
        if ($module_name == "Cases") { // allow to add  note
            $view = 'edit';
            $module_name = 'Notes';
            $html.="<script type='text/javascript'>bcp_module_add_casenote(1,\"$module_name\",\"$view\",\"$id\"); </script>";
        }
        $html .= "</div>";
        echo $html;
        wp_die();
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        echo "-1";
        wp_die();
    }
}

// for add/edit records in db
add_action('wp_ajax_prefix_admin_bcp_add_moduledata_call', 'prefix_admin_bcp_add_moduledata_call');
add_action('wp_ajax_nopriv_prefix_admin_bcp_add_moduledata_call', 'prefix_admin_bcp_add_moduledata_call');

add_action('admin_post_bcp_add_moduledata_call', 'prefix_admin_bcp_add_moduledata_call');
add_action('admin_post_nopriv_bcp_add_moduledata_call', 'prefix_admin_bcp_add_moduledata_call');

function prefix_admin_bcp_add_moduledata_call() {

    global $objSCP, $wpdb, $sugar_crm_version;
    $current_url = $_POST['current_url'];

    $sess_id = $objSCP->access_token;
    if ($sess_id != '') {
        if ($current_url != '' && !empty($current_url)) {
            if (strpos($current_url, 'detail') !== false) {
                $current_url = explode('?', $current_url, 2);
                $current_url = $current_url[0];
                $id = null;
            } else {
                $current_url = explode('?', $current_url, 2);
                $current_url = $current_url[0];
                $id = stripslashes_deep($_REQUEST['id']);
            }
        }
        $logged_user_id = $_SESSION['scp_user_id'];
        $module_name = stripslashes_deep($_REQUEST['module_name']);
        $view = stripslashes_deep($_REQUEST['view']);

        if (is_array((array) $_SESSION['module_array'])) {//Added for getting module key OR lable added from sugar side
            $arry_lable = (array) $_SESSION['module_array'];
            $module_name_label = $arry_lable[$module_name];
            $module_name_label_Cases = $arry_lable["Cases"];
        }
        $module_with_caps = $module_name_label;

        if (($module_name == 'Meetings' || $module_name == 'Calls') && array_key_exists('date_start', $_REQUEST)) {
            if ($_REQUEST['date_start'] && $_REQUEST['date_end']) {
                $date1 = new DateTime($_REQUEST['date_start']);
                $date2 = new DateTime($_REQUEST['date_end']);
                $diff = $date2->diff($date1);
                $hours = $diff->h;
                $hours = $hours + ($diff->days * 24);

                $_REQUEST['duration_hours'] = $hours;
                $_REQUEST['duration_minutes'] = $diff->i;
            } else {
                if (!isset($_REQUEST['duration_hours']) && empty($_REQUEST['duration_hours'])) {
                    $_REQUEST['duration_hours'] = 1;
                }
                if (!isset($_REQUEST['duration_minutes']) && empty($_REQUEST['duration_minutes'])) {
                    $_REQUEST['duration_minutes'] = 0;
                }
            }
        }
        if (isset($id) && !empty($id)) {
            $id = $id;
            if (isset($_REQUEST['delete']) && !empty($_REQUEST['delete']) && $_REQUEST['delete'] == 1) {
                $deleted = 1;
            }
        } else {
            $id = '';
            $deleted = 0;
        }
        //get name array
        $pass_name_arry = array();
        //Added by BC on 20-jun-2016 for edit view-----------------
        $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $_SESSION["contact_group"] . "'";
        $result_count = $wpdb->get_row($sql_count) or die(mysql_error());

        $json_data = $result_count->json_data;
        $results = json_decode($json_data);
        //End by BC on 20-jun-2016----------------
        include( TEMPLATE_PATH . 'bcp_add_records_v7.php');

        $pass_name_arry['id'] = $id;
        $pass_name_arry['deleted'] = $deleted;
        if (($module_name == 'Meetings' || $module_name == 'Calls') && array_key_exists('date_start', $_REQUEST)) {
            $pass_name_arry['duration_hours'] = stripslashes_deep($_REQUEST['duration_hours']);
            $pass_name_arry['duration_minutes'] = stripslashes_deep($_REQUEST['duration_minutes']);
        }


        if ($module_name == "Documents") {
            if (isset($pass_name_arry['related_doc_name']) && !empty($pass_name_arry['related_doc_name'])) {//Set Related document by revision id
                $record_detaildoc = $objSCP->getRecordDetail("Documents", $pass_name_arry['related_doc_name']);
                $revision_id = $record_detaildoc->document_revision_id;
                $pass_name_arry['related_doc_id'] = $pass_name_arry['related_doc_name'];
                $pass_name_arry['related_doc_rev_id'] = $revision_id;
            }
            // if document delete request
            if ($deleted == 1) {
                $new_id = $objSCP->set_entry($module_name, $pass_name_arry);
            } else {
                // Document Set Entry
                $upload_file = $_FILES['filename']['name'];
                $upload_path = $_FILES['filename']['tmp_name'];
                $new_id2 = $objSCP->createDocument($pass_name_arry, $upload_file, $upload_path);
                //Updated by BC on 21-jun-2016
                if (!is_object($new_id2)) {
                    $new_id = $new_id2;
                } else {
                    $new_id = $new_id2->record->id;
                }
            }
        } else if ($module_name == "Notes") {
            if ($deleted == 1) {
                $new_id = $objSCP->set_entry($module_name, $pass_name_arry);
            } else { // File Set Entry
                $upload_file = $_FILES['filename']['name'];
                $upload_path = $_FILES['filename']['tmp_name'];
                $new_id2 = $objSCP->createNote($pass_name_arry, $upload_file, $upload_path);
                //Updated by BC on 21-jun-2016
                if (!is_object($new_id2)) {
                    $new_id = $new_id2;
                } else {
                    $new_id = $new_id2->record->id;
                }
                if (!empty($_REQUEST['case_id'])) {
                    $rel_id = $objSCP->set_relationship('Cases', $_REQUEST['case_id'], strtolower($module_name), $new_id);
                }
            }
        } else {
            $new_id = $objSCP->set_entry($module_name, $pass_name_arry);
        }

        $rel_id = $objSCP->set_relationship('Contacts', $logged_user_id, strtolower($module_name), $new_id);

        if (isset($id) && !empty($id)) {
            if (isset($_REQUEST['delete']) && !empty($_REQUEST['delete']) && $_REQUEST['delete'] == 1) {
                $view1 = 'list';
                $redirect_url = $current_url . '?' . $view1 . '-' . $module_name . '';
                $conn_err = $module_with_caps . " Deleted Successfully";
                setcookie('bcp_add_record', $conn_err, time() + 30, '/');
                echo $redirect_url;
                wp_die();
            } else {
                $view1 = 'list';
                $redirect_url = $current_url . '?' . $view1 . '-' . $module_name . '';
                $conn_err = $module_with_caps . " Updated Successfully";
                setcookie('bcp_add_record', $conn_err, time() + 30, '/');
            }
        } else {
            if ($module_name == "Notes" && !empty($_REQUEST['case_id'])) {
                $view1 = 'list';
                $module_name1 = 'Cases';
                $redirect_url = $current_url . '?' . $view1 . '-' . $module_name1 . '';
                $conn_err = $module_with_caps . " Added Successfully In " . $module_name_label_Cases;
                setcookie('bcp_add_record', $conn_err, time() + 30, '/');
            } else {

                $view1 = 'list';
                $redirect_url = $current_url . '?' . $view1 . '-' . $module_name . '';
                $conn_err = $module_with_caps . " Added Successfully";
                setcookie('bcp_add_record', $conn_err, time() + 30, '/');
            }
        }
        wp_redirect($redirect_url);
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        $redirect_url = $current_url . '?conerror=1';
        wp_redirect($redirect_url);
    }
}

//For viewing details of records 
add_action('wp_ajax_bcp_calendar_display', 'bcp_calendar_display');
add_action('wp_ajax_nopriv_bcp_calendar_display', 'bcp_calendar_display');

// for calander view
function bcp_calendar_display() {
    global $objSCP, $sugar_crm_version;
    if ($sugar_crm_version == 7) {
        $sess_id = $objSCP->access_token;
    }
    if ($sess_id != '') {
        include( TEMPLATE_PATH . 'bcp_view_calendar.php');
        wp_die();
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        echo "-1";
        wp_die();
    }
}

//get KB Doc Attachment action
add_action('admin_post_bcp_get_kbdoc_attachment', 'prefix_admin_bcp_get_kbdoc_attachment');   // Get KB Doc Attachment
add_action('admin_post_nopriv_bcp_get_kbdoc_attachment', 'prefix_admin_bcp_get_kbdoc_attachment');   // Get KB Doc Attachment

function prefix_admin_bcp_get_kbdoc_attachment() {
    global $objSCP;
    if ($objSCP != 0) {
        $kbdoc_id = $_REQUEST['scp_kbdoc_id'];
        $objSCP->getKBDocumentAttachment($kbdoc_id);
    } else {
        echo "<div class='error settings-error' id='setting-error-settings_updated'> 
            <p><strong>Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.</strong></p>
        </div>";
    }
}

//for case updates
add_action('wp_ajax_prefix_admin_bcp_case_updates', 'prefix_admin_bcp_case_updates');
add_action('wp_ajax_nopriv_prefix_admin_bcp_case_updates', 'prefix_admin_bcp_case_updates');

function prefix_admin_bcp_case_updates() {
    global $objSCP, $sugar_crm_version;
    $sess_id = $objSCP->access_token;
    if ($sess_id != '') {
        $html = '';
        if (empty($_SESSION['user_timezone'])) {
            $result_timezone = $objSCP->getUserTimezone();
        } else {
            $result_timezone = $_SESSION['user_timezone'];
        }
        date_default_timezone_set($result_timezone->timezone);
        $result_timezone = date_default_timezone_get();

        $pass_name_arry = array();
        $id = $_REQUEST['id'];
        $update_text = stripslashes_deep($_REQUEST['update_text']);
        $pass_name_arry['case_id'] = $id;
        $pass_name_arry['name'] = $update_text;
        $pass_name_arry['description'] = $update_text;
        $pass_name_arry['internal'] = 0;
        $pass_name_arry['contact_id_c'] = $_SESSION['scp_user_id'];
        $new_id = $objSCP->set_entry('bc_case_updates', $pass_name_arry);
        $rel_id = $objSCP->set_relationship('Cases', $id, "bc_case_updates_cases", $new_id);
        include( TEMPLATE_PATH . 'bcp_case_updates_v7.php');
        echo $html;
        wp_die();
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        echo "-1";
        wp_die();
    }
}
