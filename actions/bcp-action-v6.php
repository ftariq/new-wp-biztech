<?php

add_action('admin_post_bcp_sign_up', 'prefix_admin_bcp_sign_up'); // Sign Up      
add_action('admin_post_nopriv_bcp_sign_up', 'prefix_admin_bcp_sign_up'); // Sign Up

function prefix_admin_bcp_sign_up() {
    global $objSCP;
    //20-aug-2016
    $sess_id = $objSCP->session_id;
    if ($sess_id != '') {
        $username_c = stripslashes_deep($_REQUEST['add-signup-username']);
        $password_c = stripslashes_deep($_REQUEST['add-signup-password']);
        $first_name = stripslashes_deep($_REQUEST['add-signup-first-name']);
        $last_name = stripslashes_deep($_REQUEST['add-signup-last-name']);
        $phone_work = stripslashes_deep($_REQUEST['add-signup-work-phone']);
        $phone_mobile = stripslashes_deep($_REQUEST['add-signup-mobile']);
        $phone_fax = stripslashes_deep($_REQUEST['add-signup-fax']);
        $title = stripslashes_deep($_REQUEST['add-signup-title']);
        $email1 = stripslashes_deep($_REQUEST['add-signup-email-address']);
        $primary_address_street = stripslashes_deep($_REQUEST['add-signup-primary-address']);
        $biztech_redirect_login = get_page_link(get_option('biztech_redirect_login'));

        $username_c = sanitize_user($username_c, true);
        if (isset($username_c) && $username_c != '') {//username is not blank then
            $addSignUp = array(
                'username_c' => $username_c,
                'password_c' => $password_c,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'phone_work' => $phone_work,
                'title' => $title,
                'phone_mobile' => $phone_mobile,
                'phone_fax' => $phone_fax,
                'email1' => $email1,
                'primary_address_street' => $primary_address_street,
            );
            //check user exists
            $checkUserExists = $objSCP->getPortalUserExists($username_c);
            $checkEmailExists = $objSCP->getPortalEmailExists($email1);

            //check username
            $user_id = username_exists($username_c);
            $wp_email_exist = email_exists($email1);

            if (($checkUserExists) && ($checkEmailExists) && ($user_id) && $wp_email_exist) {
                $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
                $signup_msg = "Username and Email Address already exists.";
                setcookie('bcp_signup_err', $signup_msg, time() + 30, '/');
                wp_redirect($redirect_url);
            } else if ($checkUserExists || $user_id) {
                $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
                $signup_msg = "Username already exists.";
                setcookie('bcp_signup_err', $signup_msg, time() + 30, '/');
                wp_redirect($redirect_url);
            } else if ($checkEmailExists || $wp_email_exist) {
                $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
                $signup_msg = "Email Address already exists.";
                setcookie('bcp_signup_err', $signup_msg, time() + 30, '/');
                wp_redirect($redirect_url);
            } else if (!is_email($email1)) {//check proper email validation
                $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
                $signup_msg = "This email id is invalid because it uses illegal characters. Please enter a valid email id.";
                setcookie('bcp_signup_err', $signup_msg, time() + 30, '/');
                wp_redirect($redirect_url);
            } else {
                $isSignUp = $objSCP->set_entry('Contacts', $addSignUp);
                if ($isSignUp != NULL) {
                    //create user in wp when entry entered in 

                    $new_user_id = wp_insert_user(array(
                        'user_login' => $username_c,
                        'user_pass' => $password_c,
                        'user_email' => $email1,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                            )
                    );
                    mail_user($username_c, $password_c, $first_name, $last_name, $email1); //Send user mail

                    $signup_msg = "You are successfully sign up.";
                    setcookie('bcp_signup_suc', $signup_msg, time() + 30, '/');
                    if (isset($biztech_redirect_login) && !empty($biztech_redirect_login)) {

                        $redirect_url = $biztech_redirect_login . '?signup=true';
                    } else {
                        $redirect_url = home_url() . '/portal-login?signup=true';
                    }
                    wp_redirect($redirect_url);
                }
            }
        } else {//if username is blank then
            $signup_msg = "This username is invalid because it uses illegal characters. Please enter a valid username.";
            setcookie('bcp_signup_err', $signup_msg, time() + 30, '/');
            $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
            wp_redirect($redirect_url);
        }
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        $redirect_url = $_REQUEST['scp_current_url'] . '?conerror=1';
        wp_redirect($redirect_url);
    }
}

//For Login action
add_action('admin_post_bcp_login', 'prefix_admin_bcp_login'); // login
add_action('admin_post_nopriv_bcp_login', 'prefix_admin_bcp_login'); // login

function prefix_admin_bcp_login() {

    global $objSCP, $wpdb, $sugar_crm_version;
    //20-aug-2016
    $sess_id = $objSCP->session_id;
    if ($sess_id != '') {
        if (get_option('biztech_scp_single_signin') != '' && is_user_logged_in()) {//Added by BC on 02-aug-2016
            global $current_user;
            $scp_username = $current_user->user_login;
            $scp_password = $current_user->user_email;
            $isLogin = $objSCP->PortalLogin($scp_username, $scp_password, 1);
        } else {
            $scp_username = $_REQUEST['scp_username'];
            $scp_password = $_REQUEST['scp_password'];
            $isLogin = $objSCP->PortalLogin($scp_username, $scp_password);
        }

        if (($isLogin->entry_list[0] != NULL) && ($scp_username != NULL) && ($scp_password != NULL)) {
            $_SESSION['scp_user_id'] = $isLogin->entry_list[0]->id;
            $_SESSION['scp_user_account_name'] = $isLogin->entry_list[0]->name_value_list->username_c->value;
            //////////////Added by BC on 18-jun-2016 store module layout & user timezoone in session////////////////////////////
            //modules list
            $modules_array = $objSCP->getPortal_accessibleModules(); //Added by BC on 16-jun-2016
            if (!isset($modules_array->contact_group) && $modules_array->contact_group == '') {//if contact group is blank then 
                wp_die('Please contact your administrator to request permission to use this.');
            }
            if (!empty($modules_array->layouts)) {//Insert or update layout
                foreach ($modules_array->layouts as $k_module => $v_module_list) {
                    foreach ($v_module_list as $k_view => $v_panel) {
                        $panel_data = json_encode($v_panel->panels);
                        if (!is_object($v_panel)) {
                            $panel_data = $v_panel;
                        }

                        //get record exists
                        $sql_count = "SELECT id,date,COUNT(*) as count FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $k_module . "' AND layout_view='" . $k_view . "' AND contact_group='" . $modules_array->contact_group . "'";
                        $result_count = $wpdb->get_row($sql_count) or die(mysql_error());
                        $rowCount = $result_count->count;

                        if ($rowCount > 0) {
                            $db_date = $result_count->date;
                            $record_id = $result_count->id;
                            if (strtotime($v_panel->modified_date) > strtotime($db_date)) {
                                $update_qry = "UPDATE " . $wpdb->prefix . "module_layout SET version='$sugar_crm_version',module_name='$k_module',layout_view='$k_view',json_data='$panel_data',date='$v_panel->modified_date',contact_group ='$modules_array->contact_group' WHERE id=$record_id";
                                $wpdb->query($update_qry);
                            }
                        } else {
                            $wpdb->insert($wpdb->prefix . 'module_layout', array(
                                'version' => $sugar_crm_version,
                                'module_name' => $k_module,
                                'layout_view' => $k_view,
                                'json_data' => $panel_data,
                                'date' => $v_panel->modified_date,
                                'contact_group' => $modules_array->contact_group
                            ));
                        }
                    }
                }
                //exit();
            }
            $_SESSION['module_array'] = $modules_array->accessible_modules;
            //currency array store in session
            $_SESSION['Currencies'] = $modules_array->Currencies;
            //Contact group
            $_SESSION['contact_group'] = $modules_array->contact_group;
            //timezone
            $user_timezone = $objSCP->getUserTimezone();
            $_SESSION['user_timezone'] = $user_timezone;
            //////////////////////////////////////////////////////
            //get option to redirect to which page after login
            $biztech_redirect_manage = get_page_link(get_option('biztech_redirect_dashboard_gui'));
            if ($biztech_redirect_manage != NULL) {
                $redirect_url = $biztech_redirect_manage;
            } else {
                $redirect_url = home_url() . "/portal-dashboard-gui/";
            }
            /* if wp user is not logged in login by auth cookie  */
            if (get_option('biztech_scp_single_signin') != '' && !is_user_logged_in()) {
                // Automatic login, without any password! //
                $user = get_user_by('login', $scp_username);
                if (!is_wp_error($user)) {
                    wp_clear_auth_cookie();
                    wp_set_current_user($user->ID);
                    wp_set_auth_cookie($user->ID);
                }
            }
            /* if wp user is not logged in */
            wp_redirect($redirect_url);
        } else {
            $ERROR_CODE = (isset($isLogin->error)) ? $isLogin->error : 1;
            $set_global_loginerr = array(
                '1' => 'Invalid Username Or Password.',
                '201' => 'Please contact your Administrator to validate License.',
                '202' => 'There seems some error while validating your license for Customer Portal Pro. Please try again later.'
            );
            setcookie('bcp_login_error', $set_global_loginerr[$ERROR_CODE], time() + 30, '/');

            $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
            if (get_option('biztech_scp_single_signin') != '' && is_user_logged_in()) {
                setcookie('bcp_auth_error', 'You do not have sufficient permission to access this page.', time() + 3600, '/');
                if (!isset($_REQUEST['error'])) {
                    wp_redirect($redirect_url);
                }
            } else {
                wp_redirect($redirect_url);
            }
        }
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        $redirect_url = $_REQUEST['scp_current_url'] . '?conerror=1';
        wp_redirect($redirect_url);
    }
}

//For Forgot Password
add_action('admin_post_bcp_forgot_password', 'prefix_admin_bcp_forgot_password');   // Forgot Password
add_action('admin_post_nopriv_bcp_forgot_password', 'prefix_admin_bcp_forgot_password');   // Forgot Password

function prefix_admin_bcp_forgot_password() {
    global $objSCP;
    //20-aug-2016
    $sess_id = $objSCP->session_id;
    if ($sess_id != '') {

        $biztech_redirect_forgotpwd = get_page_link(get_option('biztech_redirect_forgotpwd'));
        $checkUsername = stripslashes_deep($_REQUEST['forgot-password-username']);
        $checkEmialAddress = stripslashes_deep($_REQUEST['forgot-password-email-address']);

        $checkUserExists = $objSCP->getPortalUserInformationByUsername($checkUsername);
        $username = $checkUserExists->entry_list[0]->name_value_list->username_c->value;
        $emailAddress = $checkUserExists->entry_list[0]->name_value_list->email1->value;
        $getAdminEmail = get_option('admin_email');
        if (($username == $checkUsername) && ($emailAddress == $checkEmialAddress)) {
            $password = $checkUserExists->entry_list[0]->name_value_list->password_c->value;
            if (get_option('biztech_scp_name') != NULL) {
                $headers = "From: " . get_option('biztech_scp_name') . " <$getAdminEmail>' . '\r\n";
            } else {
                $headers = "From: <$getAdminEmail>' . '\r\n";
            }
            $isSendEmail = mail_user_forgotpwd($username, $password, $url, $emailAddress); //Send user mail
            if ($isSendEmail == true) {
                $redirect_url = $_REQUEST['scp_current_url'] . '?sucess=1';
                $disply_msg = "Your password has been sent successfully.";
                setcookie('bcp_frgtpwd', $disply_msg, time() + 30, '/');
                wp_redirect($redirect_url);
            } else {
                $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
                $disply_msg = "Failed to send your your password. Please try later or contact the administrator by another method.";
                setcookie('bcp_frgtpwd', $disply_msg, time() + 30, '/');
                wp_redirect($redirect_url);
            }
        } else if (($username == $checkUsername) && ($emailAddress != $checkEmialAddress)) {
            $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
            $disply_msg = "Your email address does not match.";
            setcookie('bcp_frgtpwd', $disply_msg, time() + 30, '/');
            wp_redirect($redirect_url);
        } else {
            $redirect_url = $_REQUEST['scp_current_url'] . '?error=1';
            $disply_msg = "Your username does not exists.";
            setcookie('bcp_frgtpwd', $disply_msg, time() + 30, '/');
            wp_redirect($redirect_url);
        }
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        $redirect_url = $_REQUEST['scp_current_url'] . '?conerror=1';
        wp_redirect($redirect_url);
    }
}

//Get user group module list
add_action('wp_ajax_get_user_group_modules', 'get_user_group_modules_callback');
add_action('wp_ajax_nopriv_get_user_group_modules', 'get_user_group_modules_callback');

function get_user_group_modules_callback() {
    global $objSCP;
    //20-aug-2016
    $sess_id = $objSCP->session_id;
    if ($sess_id != '') {
        $user_group = $_REQUEST['user_group'];
        $list_result = $objSCP->get_entry("bc_user_group", $user_group);
        $accessable_modules = $list_result->entry_list[0]->name_value_list->accessible_modules->value;
        $str = $objSCP->unencodeMultienum($accessable_modules);
        if (count($accessable_modules) > 0) {
            $final_str = implode(',', $str);
        } else {
            $final_str = "-";
        }
        $html = "<br><b>Accessible module list</b> : " . $final_str . "";
        echo $html;
        wp_die();
    }
}

//get Doc Attachment action
add_action('admin_post_bcp_get_doc_attachment', 'prefix_admin_bcp_get_doc_attachment');   // Get Doc Attachment
add_action('admin_post_nopriv_bcp_get_doc_attachment', 'prefix_admin_bcp_get_doc_attachment');   // Get Doc Attachment

function prefix_admin_bcp_get_doc_attachment() {
    global $objSCP;
    //20-aug-2016
    $sess_id = $objSCP->session_id;
    if ($sess_id != '') {
        $doc_id = $_REQUEST['scp_doc_id'];
        $objSCP->getDocument($doc_id);
    } else {
        echo "<div class='error settings-error' id='setting-error-settings_updated'> 
            <p><strong>Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.</strong></p>
        </div>";
    }
}

//For list records in modules
add_action('wp_ajax_bcp_list_module', 'wp_ajax_bcp_list_module');
add_action('wp_ajax_nopriv_bcp_list_module', 'wp_ajax_bcp_list_module');

function wp_ajax_bcp_list_module() {
    global $objSCP, $wpdb, $sugar_crm_version;
    $current_url = $_POST['current_url'];
    //20-aug-2016
    $sess_id = $objSCP->session_id;
    if ($sess_id != '') {
        $module_name = $_POST['modulename'];
        if (is_array((array) $_SESSION['module_array'])) {//Added for getting module key OR lable added from sugar side
            $arry_lable = (array) $_SESSION['module_array'];
            $module_name_label = $arry_lable[$module_name];
        }
        $view = $_POST['view'];
        $limit = get_option('biztech_scp_case_per_page');
        $page_no = $_POST['page_no'];
        $offset = ($page_no * $limit) - $limit;
        //Updated by BC on 20-jun-2016 for timezone store in session
        if (empty($_SESSION['user_timezone'])) {
            $result_timezone = $objSCP->getUserTimezone();
        } else {
            $result_timezone = $_SESSION['user_timezone'];
        }
        date_default_timezone_set($result_timezone);
        $result_timezone = date_default_timezone_get();
        $order_by = trim($_POST['order_by']);
        $order = trim($_POST['order']);
        //$search_name = trim($_POST['searchval']);
        $search_name = array_key_exists('searchval', $_POST) ? trim($_POST['searchval']) : null;
        //$AOK_Knowledge_Base_Categories = trim($_POST['AOK_Knowledge_Base_Categories']); //Added by BC on 20-may-2016
        $AOK_Knowledge_Base_Categories = array_key_exists('AOK_Knowledge_Base_Categories', $_POST) ? trim($_POST['AOK_Knowledge_Base_Categories']) : null;
        $lcfirst_module_name = strtolower($module_name);
        $where_con = '';
        if (isset($search_name) && !empty($search_name)) {
            if ($module_name == "Contacts" || $module_name == "Leads") {
                $where_con = "(" . $lcfirst_module_name . ".first_name like '%{$search_name}%' OR " . $lcfirst_module_name . ".last_name like '%{$search_name}%'" . ")";
            } else if ($module_name == "Documents") {
                $where_con = $lcfirst_module_name . ".document_name like '%{$search_name}%'";
            } else {
                $where_con = $lcfirst_module_name . ".name like '%{$search_name}%'";
            }
        }
        //Added by BC on 20-jun-2016 for list view-----------------
        $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $_SESSION["contact_group"] . "'";
        $result_count = $wpdb->get_row($sql_count) or die(mysql_error());

        $json_data = $result_count->json_data;
        $results = json_decode($json_data);
        //End by BC on 20-jun-2016-----------------
        if (!empty($results) || $module_name == 'AOK_KnowledgeBase') {
            if (isset($order_by) == true && !empty($order_by) && isset($order) == true && !empty($order)) {
                $order_by_query = "$order_by $order";
            } else {
                $order_by_query = "date_entered desc";
            }
            $id_show = false;
            foreach ($results as $key_n => $val_n) {
                if ($key_n == 'ID') {
                    $id_show = true;
                }
                if (!empty($val_n->related_module)) {
                    if ($val_n->type == 'relate' && ($val_n->related_module != 'Users' && !in_array($val_n->related_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array 
                        continue;
                    }
                }
                $n_array[] = strtolower($key_n);
            }
            if (!in_array('id', $n_array)) {
                array_push($n_array, 'id');
            }
            array_push($n_array, 'currency_id'); //Added by BC on 02-jul-2016 for getting currency id
            if ($module_name == 'AOK_KnowledgeBase') {//Added by BC on 19-may-2016
                $n_array = array('id', 'name', 'date_entered', 'description', 'currency_id');
                if (isset($AOK_Knowledge_Base_Categories) && !empty($AOK_Knowledge_Base_Categories)) {
                    $list_result_count_all = $objSCP->get_relationships('AOK_Knowledge_Base_Categories', $AOK_Knowledge_Base_Categories, 'aok_knowledgebase_categories', $n_array, $where_con, $order_by_query, 0);
                    $list_result = $objSCP->get_relationships('AOK_Knowledge_Base_Categories', $AOK_Knowledge_Base_Categories, 'aok_knowledgebase_categories', $n_array, $where_con, $order_by_query, 0, $offset, $limit);
                } else {
                    $list_result_count_all = $objSCP->get_entry_list($module_name, $where_con, $n_array, $order_by_query, 0);
                    $list_result = $objSCP->get_entry_list($module_name, $where_con, $n_array, $order_by_query, 0, $limit, $offset);
                }
            } else {
                $list_result_count_all = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $n_array, $where_con, $order_by_query, 0);
                $list_result = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $n_array, $where_con, $order_by_query, 0, $offset, $limit);
            }
            $cnt = count($list_result->entry_list);
            if (!empty($list_result_count_all->entry_list)) {
                $countCases = count($list_result_count_all->entry_list);
            } else {
                $countCases = 0;
            }
            $html = "";
            //  note attchment
            $html .= "<form action='" . home_url() . "/wp-admin/admin-post.php' method='post' id='download_note_id'>
            <input type='hidden' name='action' value='bcp_get_note_attachment'>
            <input type='hidden' name='scp_note_id' value='' id='scp_note_id'>
            </form>";
            $html .= "
            <form action='" . home_url() . "/wp-admin/admin-post.php' method='post' id='doc_submit'>
            <input type='hidden' name='action' value='bcp_get_doc_attachment'>
            <input type='hidden' name='scp_doc_id' value='' id='scp_doc_id'>
            </form>";
            $html .= "<form action = '" . home_url() . "/wp-admin/admin-post.php' method = 'post' enctype = 'multipart/form-data' id = 'actionform'>
            <input type = 'hidden' name = 'action' value = 'bcp_module_call_add'>
            <input type = 'hidden' name = 'scp_current_url' value = '" . $current_url . "'>
            <input type = 'hidden' name = 'id' value = '' id = 'scp_id'>
            <input type = 'hidden' name = 'delete' value = '1'>
            <input type = 'hidden' name = 'module_name' value = '" . $module_name . "'>
            </form>";
            if ($module_name == 'AOK_KnowledgeBase') {
                $kb_class = 'scp-kb-btn';
            } else {
                $kb_class = '';
            }
            $html .="<div class='scp-action-header $kb_class'><div class='scp-action-header-left'><h3 class='fa " . $module_name . " side-icon-wrapper scp-$module_name-font scp-default-font' >" . strtoupper($module_name_label) . "</h3>";
            $list_result_count_all = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], 'accounts', array('id'), '', '', 0);
            $countAcc = count($list_result_count_all->entry_list);
            if (($module_name == 'Accounts' && $countAcc > 0) || $module_name == 'AOS_Quotes') {
                
            } else {
                //for add
                if ($module_name != 'Accounts' && $module_name != 'AOK_KnowledgeBase' && $module_name != 'AOS_Quotes' && $module_name != 'AOS_Invoices' && $module_name != 'AOS_Contracts') {
                    $html .= "<a class='scp-$module_name scp-default-bg-btn' href='javascript:void(0);' onclick='bcp_module_call_add(0,\"$module_name\",\"edit\",\"\",\"\",\"\");'><span class='fa fa-plus side-icon-wrapper'></span><span>ADD " . strtoupper($module_name_label) . "</span></a>";
                }
            }
            $html .="</div>";


            if ($module_name != 'Accounts' && $module_name != 'AOK_KnowledgeBase') {
                //for Serach
                $html .= "<form method = 'post' enctype = 'multipart/form-data' id = 'actionform_search' onsubmit = \"return false;\" class='actionform_search'>";
                $html .= " <div class='search-box'><input type='text' name='search_name' value='" . $search_name . "' id='search_name' placeholder='Search $module_name_label' class='search-input'/>
            <a href='javascript:;' onclick='bcp_module_paging(0,\"$module_name\",1,\"\",\"\",\"$view\",\"$current_url\")' id='search_btn_id' class='hover active scp-button search-btn'><i class='fa fa-search' aria-hidden='true'></i>SEARCH</a></div>
            <a id='clear_btn_id' onclick='bcp_clear_search_txtbox(0,\"$module_name\",\"\",\"\",\"\",\"$view\",\"$current_url\");' href='javascript:;'><i class='fa fa-remove'></i>CLEAR</a>

            </form>
            <script>
            jQuery('#search_name').keypress(function(event){
    
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                    bcp_module_paging(0,\"$module_name\",1,\"\",\"\",\"$view\",\"$current_url\");	
            }
            event.stopPropagation();
            });
            </script>";
            }
            $html .= "</div>";

            if ($cnt > 0) {
                $html .= "<div class='scp-page-action-title'>View $module_name_label </div>";
            }
            $get_param = $_SERVER['QUERY_STRING'];
            if (isset($get_param) && !empty($get_param) || $current_url != '') {
//                if (strpos($get_param, 'list') === false) {
//                    
//                } else
                {
                    if (isset($_COOKIE['bcp_add_record']) && $_COOKIE['bcp_add_record'] != '') {
                        $cookie_err = $_COOKIE['bcp_add_record'];
                        unset($_COOKIE['bcp_add_record']);
                        $html .= "<span class='success' id='succid'>" . $cookie_err . "</span>";
                    }
                }
            }
            $html .= "<div class='scp-table-responsive'>";
            if ($module_name == 'AOK_KnowledgeBase') {//Addded by BC on 20-may-2016
                include( TEMPLATE_PATH . 'bcp_list_page_kb.php');
            }
            if ($cnt > 0) {
                if ($module_name != 'AOK_KnowledgeBase') {//Addded by BC on 20-may-2016
                    $html .= "<table id='example' class='display scp-table scp-table-striped scp-table-bordered scp-table-hover' cellspacing='0' width='100%'>";
                    $name_arry = array();
                    $fields_meta = array();
                    $module_without_s = strtolower(rtrim($module_name, 's'));
                    ///start////
                    //check if add on plugin activated or not 
                    if (is_plugin_active('crm-portal-transection/index.php')) {
                        if (get_option('biztech_payment_status') == 1) {//enable
                            $get_modulename = get_option('biztech_amount_module'); // check module name
                            if (ltrim($get_modulename) != '') {
                                foreach ($_SESSION['module_array'] as $key => $values) {
                                    $modulelist[] = $key;
                                }
                                if (in_array($get_modulename, $modulelist) && $get_modulename == $module_name) { // check if module name is in array or not AND if that module is selected or not
                                    include(ABSPATH . '/wp-content/plugins/crm-portal-transection/bcp_list_page_transaction6.php');
                                } else {
                                    include( TEMPLATE_PATH . 'bcp_list_page_v6.php');
                                }
                            } else {
                                include( TEMPLATE_PATH . 'bcp_list_page_v6.php');
                            }
                        } else {
                            include( TEMPLATE_PATH . 'bcp_list_page_v6.php');
                        }
                    } else {
                        include( TEMPLATE_PATH . 'bcp_list_page_v6.php');
                    }

                    ////end///
                    $html.=$html1;
                    $html .="</table>";
                }
                $html .="</div>";
                $pagination_url = "?";
                $view1 = 'list';
                $html .= pagination($countCases, $limit, $page_no, $pagination_url, $module_name, $order_by, $order, $view1, $current_url);
            } else {
                $html .= "<strong>No Record(s) Found.</strong>";
                $html .="</div>";
            }
        } else {
            $html.= $json_data;
        }
        echo $html;
        wp_die();
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        echo "-1";
        wp_die();
    }
}

//For viewing details of records 
add_action('wp_ajax_bcp_view_module', 'wp_ajax_bcp_view_module');
add_action('wp_ajax_nopriv_bcp_view_module', 'wp_ajax_bcp_view_module');

function wp_ajax_bcp_view_module() {
    global $objSCP, $wpdb, $sugar_crm_version;
    //20-aug-2016
    $sess_id = $objSCP->session_id;
    if ($sess_id != '') {
        $module_name = $_POST['modulename'];
        $view = $_POST['view'];
        $id = $_POST['id'];
        $current_url = $_POST['current_url'];
        $html = '';
        //Added by BC on 20-jun-2016 for edit view-----------------
        $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $_SESSION["contact_group"] . "'";
        $result_count = $wpdb->get_row($sql_count) or die(mysql_error());

        $json_data = $result_count->json_data;
        $results = json_decode($json_data);
        //End by BC on 20-jun-2016-----------------
        //Updated by BC on 17-jun-2016 for timezone store in session
        if (empty($_SESSION['user_timezone'])) {
            $result_timezone = $objSCP->getUserTimezone();
        } else {
            $result_timezone = $_SESSION['user_timezone'];
        }
        date_default_timezone_set($result_timezone);
        $result_timezone = date_default_timezone_get();

        $where_con = strtolower($module_name) . ".id = '{$id}'";
        $record_detail = $objSCP->get_entry_list($module_name, $where_con);
        $name = $record_detail->entry_list[0]->name_value_list->name->value;
        if ($module_name == 'AOS_Quotes' || $module_name == 'AOS_Invoices' || $module_name == 'AOS_Contracts') {//Added by BC on 02-july-2016 for storing currency
            $curreny_id = $record_detail->entry_list[0]->name_value_list->currency_id->value;
            if (!empty($curreny_id)) {
                $currency_symbol = $_SESSION['Currencies']->$curreny_id->symbol;
            }
        }
        if (!empty($results)) {
            // for not set layout
            $html .= "<form action='" . home_url() . "/wp-admin/admin-post.php' method='post' id='doc_submit'>
            <input type='hidden' name='action' value='bcp_get_doc_attachment'>
            <input type='hidden' name='scp_doc_id' value='' id='scp_doc_id'>
            </form>";
            // note attchment
            $html .= "<form action='" . home_url() . "/wp-admin/admin-post.php' method='post' id='download_note_id'>
            <input type='hidden' name='action' value='bcp_get_note_attachment'>
            <input type='hidden' name='scp_note_id' value='' id='scp_note_id'>
            </form>";
            include( TEMPLATE_PATH . 'bcp_view_page_v6.php');
            if ($module_name == 'AOS_Quotes' || $module_name == 'AOS_Invoices' || $module_name == 'AOS_Contracts') {//Added by BC on 24-may-2016
                $quote_entry = $objSCP->get_entry_quotes($module_name, $id);
                include( TEMPLATE_PATH . 'bcp_view_page_v6_product_bundle.php');
            }
        } else {
            $html .= $json_data;
        }
        if ($module_name == "Cases") { // allow to add  note
            $view = 'edit';
            $module_name = 'Notes';
            $html.="<script type='text/javascript'>$(window).load(bcp_module_add_casenote(1,\"$module_name\",\"$view\",\"$id\")); </script>";
        }
        $html .= "</div>";
        echo $html;
        wp_die();
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        echo "-1";
        wp_die();
    }
}

//getting related module list 
add_action('wp_ajax_get_module_list', 'get_module_list_callback');
add_action('wp_ajax_nopriv_get_module_list', 'get_module_list_callback');

function get_module_list_callback() {
    global $objSCP;
    $scp_sugar_rest_url = get_option('biztech_scp_rest_url');
    $scp_sugar_username = get_option('biztech_scp_username');
    $scp_sugar_password = get_option('biztech_scp_password');

    $objSCP = new SugarRestApiCall($scp_sugar_rest_url, $scp_sugar_username, $scp_sugar_password);
    $logged_user_id = $_SESSION['scp_user_id'];

    $module_name = $_REQUEST['parent_type'];
    $select_fields = array('id', 'name');

    $record_detail = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $select_fields);
    $html = "";
    $html .= "<select id='parent_id' name='parent_id' class='scp-form-control'><option value=''></option>";
    for ($m = 0; $m < count($record_detail->entry_list); $m++) {
        $mod_id = $record_detail->entry_list[$m]->name_value_list->id->value;
        $mod_name = $record_detail->entry_list[$m]->name_value_list->name->value;
        $html .= "<option value=" . $mod_id . ">" . $mod_name . "</option>";
    }
    $html .= "</select>";

    echo $html;
    wp_die();
}

//For add/edit records layout in modules
add_action('wp_ajax_bcp_add_module', 'wp_ajax_bcp_add_module');
add_action('wp_ajax_nopriv_bcp_add_module', 'wp_ajax_bcp_add_module');

function wp_ajax_bcp_add_module() {
    global $objSCP, $wpdb, $sugar_crm_version;
    //20-aug-2016
    $sess_id = $objSCP->session_id;
    if ($sess_id != '') {
        $module_name = $_POST['modulename'];
        if (is_array((array) $_SESSION['module_array'])) {//Added for getting module key OR lable added from sugar side
            $arry_lable = (array) $_SESSION['module_array'];
            $module_name_label = $arry_lable[$module_name];
        }
        $view = $_POST['view'];
        $current_url = $_POST['current_url'];
        $html = "";
        if ($current_url != '' && !empty($current_url)) {
            if (strpos($current_url, 'detail') !== false) {
                $id = Null;
            } else {
                $current_url = explode('?', $current_url, 2);
                $current_url = $current_url[0];
                $id = array_key_exists('id', $_POST) ? $_POST['id'] : null;
            }
        }
        $success = array_key_exists('success', $_POST) ? trim($_POST['success']) : null;
        $casenote = array_key_exists('casenote', $_POST) ? trim($_POST['casenote']) : null;
        //Added by BC on 20-jun-2016 for edit view-----------------
        $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $_SESSION["contact_group"] . "'";
        $result_count = $wpdb->get_row($sql_count) or die(mysql_error());

        $json_data = $result_count->json_data;
        $results = json_decode($json_data);
        //End by BC on 20-jun-2016-----------------
        //edit start-------
        if (isset($id) && !empty($id)) {
            $where_con = strtolower($module_name) . ".id = '{$id}'";
            $record_detail = $objSCP->get_entry_list($module_name, $where_con);
        }
        //edit end---------
        $type_arr = array();
        $text_sugar_array = array('name', 'phone', 'url', 'phone_fax', 'varchar', 'file', 'datetimecombo', 'datetime', 'id');
        //$select_options_sugar_array = array('enum', 'parent', 'multienum');
        $select_options_sugar_array = array('enum', 'multienum', 'dynamicenum');
        $select_relate_options_sugar_array = array('relate');
        $textarea_sugar_array = array('text');
        $check_box_sugar_array = array('bool');
        /////////////////////
        if (isset($record_detail->entry_list[0]->name_value_list->parent_type->value)) {
            $parent_type = $record_detail->entry_list[0]->name_value_list->parent_type->value;
        }
        if (isset($record_detail->entry_list[0]->name_value_list->parent_id->value)) {
            $parent_id = $record_detail->entry_list[0]->name_value_list->parent_id->value;
        }

        /////////////////////

        $module_without_s = $module_name_label;
        if ($module_name == "Notes" && $casenote != null) {
            $html = "";
        } else {
            $html .= "<form action='" . home_url() . "/wp-admin/admin-post.php' method='post' id='doc_submit'>
            <input type='hidden' name='action' value='bcp_get_doc_attachment'>
            <input type='hidden' name='scp_doc_id' value='' id='scp_doc_id'>
            </form>";
            // note attchment
            $html .= "<form action='" . home_url() . "/wp-admin/admin-post.php' method='post' id='download_note_id'>
            <input type='hidden' name='action' value='bcp_get_note_attachment'>
            <input type='hidden' name='scp_note_id' value='' id='scp_note_id'>
            </form>";
        }

        if (!empty($results)) {
            include( TEMPLATE_PATH . 'bcp_add_page_v6.php');
        } else {
            $html.= $json_data;
        }
        //layout not set
        if ($module_name == "Notes" && $casenote != null) {
            $html .= "";
        } else {
            $html .= "</div>";
        }
//added for validation support in all browsers
        $html .= "<script type='text/javascript'>
             jQuery('#filename').bind('change', function() {

              //this.files[0].size gets the size of your file.
              if(this.files[0].size > 2097152){//2 MB size allowed
                alert('Attachment file size is not proper, Maximum file size can be 2 MB.');
                jQuery('#filename').val('');
                return false;
              }
            });

        $('#general_form_id').validate();
//        to display datetimepicker in start date and due date
         jQuery('.datetimepicker').datetimepicker();
          //to display date cakender for document
          jQuery('#active_date').datepicker({
                    dateFormat : 'yy-mm-dd'
            });
           
            jQuery('#exp_date').datepicker({
                    dateFormat : 'yy-mm-dd'
            });
            jQuery('#birthdate').datepicker({
                    dateFormat : 'yy-mm-dd'
            });
            jQuery('#duration_hours').keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        // Allow: Ctrl+A, Command+A
                        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                        // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
            });
            
jQuery('#add-doc-publish-date').datepicker({
                    dateFormat : 'yy-mm-dd'
            });
            //for add new note
            jQuery('#add-doc-expire-date').datepicker({
                    dateFormat : 'yy-mm-dd'
            });
            
//for add opportunity
jQuery('#date_closed').datepicker({
                    dateFormat : 'yy-mm-dd'
            });
            jQuery('#amount').keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        // Allow: Ctrl+A, Command+A
                        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                        // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
            });
                            </script>";

        echo $html;
        wp_die();
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        $redirect_url = $_REQUEST['scp_current_url'] . '?conerror=1';
        wp_redirect($redirect_url);
    }
}

add_action('wp_ajax_prefix_admin_bcp_add_moduledata_call', 'prefix_admin_bcp_add_moduledata_call');
add_action('wp_ajax_nopriv_prefix_admin_bcp_add_moduledata_call', 'prefix_admin_bcp_add_moduledata_call');
add_action('admin_post_bcp_add_moduledata_call', 'prefix_admin_bcp_add_moduledata_call');
add_action('admin_post_nopriv_bcp_add_moduledata_call', 'prefix_admin_bcp_add_moduledata_call');

function prefix_admin_bcp_add_moduledata_call() {
    global $objSCP, $wpdb, $sugar_crm_version;
    $current_url = $_POST['current_url'];
    
    
    
    //20-aug-2016
    $sess_id = $objSCP->session_id;
    
    if ($sess_id != '') {        
        if (isset($current_url)  && $current_url != '' ) {
            if (strpos($current_url, 'detail') !== false) {               
                $current_url = explode('?', $current_url, 2);
                $current_url = $current_url[0];
                
            } else {
                
                $current_url = explode('?', $current_url, 2);
                $current_url = $current_url[0];
                $id = stripslashes_deep($_REQUEST['id']);
            }
        }
        $logged_user_id = $_SESSION['scp_user_id'];
        $module_name = stripslashes_deep($_REQUEST['module_name']);
        $view = stripslashes_deep($_REQUEST['view']);
        if (is_array((array) $_SESSION['module_array'])) {//Added for getting module key OR lable added from sugar side
            $arry_lable = (array) $_SESSION['module_array'];
            $module_name_label = $arry_lable[$module_name];
            $module_name_label_Cases = $arry_lable["Cases"];
        }
        $module_with_caps = $module_name_label;


        if (( $module_name == 'Meetings' || $module_name == 'Calls') && array_key_exists('date_start', $_REQUEST)) {
            if ($_REQUEST['date_start'] && $_REQUEST['date_end']) {
                $date1 = new DateTime($_REQUEST['date_start']);
                $date2 = new DateTime($_REQUEST['date_end']);
                $diff = $date2->diff($date1);
                $hours = $diff->h;
                $hours = $hours + ($diff->days * 24);

                $_REQUEST['duration_hours'] = $hours;
                $_REQUEST['duration_minutes'] = $diff->i;
            }
        }
     
        if (isset($id) && !empty($id)) {
            $id = $id;         
            if (isset($_REQUEST ['delete']) && !empty($_REQUEST ['delete']) && $_REQUEST['delete'] == 1) {
                $deleted = 1;
            }
        } else {            
            $deleted = 0;
        }
        //get name array
        $pass_name_arry = array();
        //Added by BC on 20-jun-2016 for edit view-----------------
        $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $_SESSION["contact_group"] . "'";
        $result_count = $wpdb->get_row($sql_count) or die(mysql_error());

        $json_data = $result_count->json_data;
        $results = json_decode($json_data);
        //End by BC on 20-jun-2016----------------
        include( TEMPLATE_PATH . 'bcp_add_records_v6.php');

        $pass_name_arry['id'] = $id;
        $pass_name_arry['deleted'] = $deleted;
        
        if (($module_name == 'Meetings' || $module_name == 'Calls') && array_key_exists('date_start', $_REQUEST)) {
            $pass_name_arry['duration_hours'] = $_REQUEST['duration_hours'];
            $pass_name_arry['duration_minutes'] = $_REQUEST['duration_minutes'];
        }
        if ($module_name == "Documents") {
            if (isset($pass_name_arry['related_doc_name']) && !empty($pass_name_arry['related_doc_name'])) {//Set Related document by revision id
                $select_field_array = array('document_revision_id');
                $document_result = $objSCP->get_entry('Documents', $pass_name_arry['related_doc_name'], $select_field_array);
                $revision_id = $document_result->entry_list[0]->name_value_list->document_revision_id->value;
                $pass_name_arry['related_doc_id'] = $pass_name_arry['related_doc_name'];
                $pass_name_arry['related_doc_rev_id'] = $revision_id;
            }
            if ($deleted == 1) {
                $new_id = $objSCP->set_entry($module_name, $pass_name_arry);
            } else {
                // Document Set Entry
                $upload_file = $_FILES['filename']['name'];
                $upload_path = $_FILES['filename']['tmp_name'];
                $new_id = $objSCP->set_Document($pass_name_arry, $upload_file, $upload_path);
            }
        } else if ($module_name == "Notes") {
            if ($deleted == 1) {
                $new_id = $objSCP->set_entry($module_name, $pass_name_arry);
            } else {
                // File Set Entry
                $upload_file = $_FILES['filename']['name'];
                $upload_path = $_FILES['filename']['tmp_name'];
                $new_id = $objSCP->createNote($pass_name_arry, $upload_file, $upload_path);
                $rel_id = $objSCP->set_relationship('Cases', $_REQUEST['case_id'], strtolower($module_name), array($new_id));
            }
        } else {
            $new_id = $objSCP->set_entry($module_name, $pass_name_arry);
        }
        //Set relationship with contact
        $rel_id = $objSCP->set_relationship('Contacts', $logged_user_id, strtolower($module_name), array($new_id));
        if (isset($id) && !empty($id)) {
            if (isset($_REQUEST ['delete']) && !empty($_REQUEST ['delete']) && $_REQUEST['delete'] == 1) {
                $view1 = 'list';
                $redirect_url = $current_url . '?' . $view1 . '-' . $module_name . '';
                $conn_err = $module_with_caps . " Deleted Successfully";
                setcookie('bcp_add_record', $conn_err, time() + 30, '/');
                echo $redirect_url;
                wp_die();
            } else {
                $view1 = 'list';
                $redirect_url = $current_url . '?' . $view1 . '-' . $module_name . '';
                $conn_err = $module_with_caps . " Updated Successfully";
                setcookie('bcp_add_record', $conn_err, time() + 30, '/');
            }
        } else {
            if ($module_name == "Notes" && !empty($_REQUEST['case_id'])) {
                $view1 = 'list';
                $module_name1 = 'Cases';
                $redirect_url = $current_url . '?' . $view1 . '-' . $module_name1 . '';
                $conn_err = $module_with_caps . " Added Successfully In " . $module_name_label_Cases;
                setcookie('bcp_add_record', $conn_err, time() + 30, '/');
            } else {
                $view1 = 'list';
                $redirect_url = $current_url . '?' . $view1 . '-' . $module_name . '';
                $conn_err = $module_with_caps . " Added Successfully";
                setcookie('bcp_add_record', $conn_err, time() + 30, '/');
            }
        }
        wp_redirect($redirect_url);
        exit;
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        $redirect_url = $current_url . '?conerror=1';
        wp_redirect($redirect_url);
        exit;
    }
}

//For viewing details of records 
add_action('wp_ajax_bcp_calendar_display', 'bcp_calendar_display');
add_action('wp_ajax_nopriv_bcp_calendar_display', 'bcp_calendar_display');

// for calander view
function bcp_calendar_display() {
    global $objSCP, $sugar_crm_version;
    //20-aug-2016
    $sess_id = $objSCP->session_id;
    if ($sess_id != '') {
        include( TEMPLATE_PATH . 'bcp_view_calendar.php');
        wp_die();
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        echo "-1";
        wp_die();
    }
}

//for case updates
add_action('wp_ajax_prefix_admin_bcp_case_updates', 'prefix_admin_bcp_case_updates');
add_action('wp_ajax_nopriv_prefix_admin_bcp_case_updates', 'prefix_admin_bcp_case_updates');

function prefix_admin_bcp_case_updates() {
    global $objSCP, $sugar_crm_version;
    //20-aug-2016
    $sess_id = $objSCP->session_id;
    if ($sess_id != '') {
        $html = '';
        if (empty($_SESSION['user_timezone'])) {
            $result_timezone = $objSCP->getUserTimezone();
        } else {
            $result_timezone = $_SESSION['user_timezone'];
        }
        date_default_timezone_set($result_timezone);
        $result_timezone = date_default_timezone_get();

        $pass_name_arry = array();
        $id = $_REQUEST['id'];
        $update_text = stripslashes_deep($_REQUEST['update_text']);
        $pass_name_arry['name'] = $update_text;
        $pass_name_arry['description'] = $update_text;
        $pass_name_arry['internal'] = 0;
        $pass_name_arry['assigned_user_id'] = 1;
        $pass_name_arry['contact_id'] = $_SESSION['scp_user_id'];
        $pass_name_arry['date_entered'] = date('Y-m-d H:i:s');
        if ($sugar_crm_version == 6) {//custom case updates call for sugar 6
            $new_id = $objSCP->set_entry('bc_case_updates', $pass_name_arry);
            $rel_id = $objSCP->set_relationship('Cases', $id, "bc_case_updates_cases", array($new_id));
        }
        if ($sugar_crm_version == 5) {
            $new_id = $objSCP->set_entry('AOP_Case_Updates', $pass_name_arry);
            $rel_id = $objSCP->set_relationship('Cases', $id, "aop_case_updates", array($new_id));
        }
        include( TEMPLATE_PATH . 'bcp_case_updates_v6.php');
        echo $html;
        wp_die();
    } else {
        $objSCP = 0;
        $conn_err = "Connection with SugarCRM not successful. Please check SugarCRM Version, REST URL, Username and Password.";
        setcookie('bcp_connection_error', $conn_err, time() + 30, '/');
        echo "-1";
        wp_die();
    }
}
