<?php

Class SugarRestApiCall {

    var $username;
    var $password;
    var $url;
    var $session_id;
    //Added by BC on 05-aug-2015 for login with admin user
    var $user_id;
    var $date_format;
    var $time_format;

    function __construct($url, $username, $password) {
        $custom_api_url = "/custom/service/v4_1_custom/rest.php"; //Added on 05-dec-2015 appended rest url
        $this->url = rtrim($url,'/') . $custom_api_url;//remove back slash if any in url
        $this->username = $username;
        $this->password = $password;
        $login_response = $this->login();
        $this->session_id = $login_response->id;
        $this->user_id = $login_response->name_value_list->user_id->value;
        $this->date_format = $login_response->name_value_list->user_default_dateformat->value;
        $this->time_format = $login_response->name_value_list->user_default_timeformat->value;
    }

    public function call($method, $parameters, $url) {
        ob_start();
        $curl_request = curl_init();

        curl_setopt($curl_request, CURLOPT_URL, $url);
        curl_setopt($curl_request, CURLOPT_POST, 1);
        curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl_request, CURLOPT_HEADER, 1);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

        $jsonEncodedData = json_encode($parameters);

        $post = array(
            "method" => $method,
            "input_type" => "JSON",
            "response_type" => "JSON",
            "rest_data" => $jsonEncodedData
        );

        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($curl_request);
        curl_close($curl_request);

        $result = explode("\r\n\r\n", $result, 2);
        $response = json_decode($result[1]);
        ob_end_flush();

        return $response;
    }

    //Added by BC on 05-aug-2015 for login with admin user
    public function login() {
        $login_parameters = array(
            "user_auth" => array(
                "user_name" => $this->username,
                "password" => md5($this->password),
            ),
        );
        $login_response = $this->call('login', $login_parameters, $this->url);
        return $login_response;
    }

    // login into Portal (login call in Users module, it give all information about contact)
    public function PortalLogin($username, $password, $login_by_email = 0) {
        /* $username and $password are passed from login page */
        $response = $this->call("IsActivate", array(), $this->url);
        if ($response->success) {
            if ($login_by_email == 1) {//Added by BC on 03-aug-2016
                $query_pass = "contacts_cstm.username_c ='{$username}' AND contacts.id in (
                SELECT eabr.bean_id
                FROM email_addr_bean_rel eabr JOIN email_addresses ea
                ON (ea.id = eabr.email_address_id)
                WHERE eabr.deleted=0 AND ea.email_address = '{$password}')";
                ;
            } else {
                $query_pass = "username_c = '{$username}' AND  password_c = '{$password}'";
            }
            $get_entry_list = array(
                'session' => $this->session_id,
                'module_name' => 'Contacts',
                'query' => $query_pass,
                'order_by' => '',
                'offset' => 0,
                'select_fields' => array('id', 'username_c', 'password_c', 'salutation', 'first_name', 'last_name', 'email1', 'account_id', 'title', 'phone_work', 'phone_mobile', 'phone_fax'),
                'max_results' => 0,
            );
            $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);
            return $get_entry_list_result;
        } else {
            return $response;
        }
    }

    public function set_entry($module_name, $set_entry_dataArray) { // create a new record
        if ($this->session_id) {
            $nameValueListArray = array();
            $i = 0;
            foreach ($set_entry_dataArray as $field => $value) {
                $nameValueListArray[$i]['name'] = $field;
                $nameValueListArray[$i]['value'] = $value;
                $i++;
            }
            $set_entry_parameters = array(
                "session" => $this->session_id,
                "module_name" => $module_name,
                "name_value_list" => $nameValueListArray
            );
            $set_entry_result = $this->call("set_entry", $set_entry_parameters, $this->url);
            $recordID = $set_entry_result->id;
            return $recordID;
        }
    }

    function get_module_fields($module_name, $fields = array()) {
        if ($this->session_id) {
            $parameters = array(
                'session' => $this->session_id,
                'module_name' => $module_name,
                'fields' => $fields
            );
            $result = $this->call("get_module_fields", $parameters, $this->url);
            return $result;
        }
    }

    //Added by BC on 03-jul-2015 updated function
    function createNote($Note_set_entry_dataArray, $upload_file, $upload_path) {

        if ($this->session_id) {
            if (empty($upload_file)) {//Added by BC on 21-jun-2016
                $upload_file = $_REQUEST['edit-file'];
                $Note_set_entry_dataArray['filename'] = $upload_file;
            }
            $NoteId = $this->set_entry('Notes', $Note_set_entry_dataArray);
            $attachment = array(
                'id' => $NoteId,
                'filename' => $upload_file,
                'file' => base64_encode(file_get_contents($upload_path)),
                'revision' => '1',
            );
            //set attachment call
            $note_attachment = array(
                'session' => $this->session_id,
                'note' => $attachment
            );
            $attachment = $this->call('set_note_attachment', $note_attachment, $this->url);
            //return $set_document_revision_result;
            return $NoteId;
        }
    }

    public function get_entry_list($module_name, $where_condition = '', $select_fields_array = array(), $order_by = '', $deleted = 0, $limit = '', $offset = '') {
        if ($this->session_id) {
            $get_entry_list_parameters = array(
                'session' => $this->session_id,
                'module_name' => $module_name,
                'query' => $where_condition,
                'order_by' => $order_by,
                "offset" => $offset,
                'select_fields' => $select_fields_array,
                'link_name_to_fields_array' => array(),
                'max_results' => $limit,
                'deleted' => $deleted,
            );
            $get_entry_list_result = $this->call("get_entry_list", $get_entry_list_parameters, $this->url);
            return $get_entry_list_result;
        }
    }

    public function get_entry($module_name, $record_id, $select_fields_array = array()) {
        if ($this->session_id) {
            $get_entry_parameters = array(
                'session' => $this->session_id,
                'module_name' => $module_name,
                'id' => $record_id,
                'select_fields' => $select_fields_array,
            );
            $get_entry_result = $this->call("get_entry", $get_entry_parameters, $this->url);
            return $get_entry_result;
        }
    }

    public function set_relationship($module_name, $module_id, $relationship_name, $related_ids_array = array(), $deleted = 0) {// set reletion between task and contact
        if ($this->session_id) {
            $set_relationships_parameters = array(
                'session' => $this->session_id,
                'module_name' => $module_name,
                'module_id' => $module_id,
                'link_field_name' => $relationship_name,
                'related_ids' => $related_ids_array,
                'delete' => $deleted,
            );
            $set_relationships_result = $this->call("set_relationship", $set_relationships_parameters, $this->url);
            return $set_relationships_result;
        }
    }

    public function get_relationships($module_name, $module_id, $relationship_name, $related_fields_array = array(), $where_condition = '', $order_By = '', $deleted = 0, $offset = 0, $limit = '') {
        if ($this->session_id) {
            $get_relationships_parameters = array(
                'session' => $this->session_id,
                'module_name' => $module_name,
                'module_id' => $module_id,
                'link_field_name' => $relationship_name, // relationship name
                'related_module_query' => $where_condition, // where condition
                'related_fields' => $related_fields_array,
                'related_module_link_name_to_fields_array' => array(),
                'deleted' => $deleted,
                'order_by' => $order_By,
                'offset' => $offset,
                'limit' => $limit
            );

            $get_relationships_result = $this->call("get_relationships", $get_relationships_parameters, $this->url);
            /* echo "<pre>";
              print_r($get_relationships_result);
              echo "</pre>";
              wp_die(); */
            return $get_relationships_result;
        }
    }

    function getNoteAttachment($note_id) {

        $note_parameters = array(
            'session' => $this->session_id,
            'module_name' => 'Notes',
            'id' => $note_id,
            'select_fields' => array('file_mime_type'),
        );
        $get_entry_result = $this->call("get_entry", $note_parameters, $this->url);
        $note_params = array(
            'session' => $this->session_id,
            'id' => $note_id,
        );
        $file_mime_type = $get_entry_result->entry_list[0]->name_value_list->file_mime_type->value;
        $noteResult = $this->call('get_note_attachment', $note_params, $this->url);
        $filename = $noteResult->note_attachment->filename;
        $content = base64_decode($noteResult->note_attachment->file);
        header("Content-type: {$file_mime_type}");
        header('Content-Disposition: attachment; filename=' . basename($filename));
        header('Content-Description: File Transfer');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        echo $content;
        exit;
    }

    function getUser_accessibleModules() {//Added by BC on 05-aug-2015 for login with admin user
        if ($this->session_id) {
            $parameters = array(
                'session' => $this->session_id,
                'module_name' => 'Users',
                'user_id' => $this->user_id,
            );
            if ($this->session_id) {
                $get_entry_result = $this->call("getUser_accessibleModules", $parameters, $this->url);
            }
            return $get_entry_result;
        }
    }

    function set_Document($Document_set_entry_dataArray, $upload_file, $upload_path) {

        if ($this->session_id) {
            if (empty($upload_file)) {//Added by BC on 07-jul-2016
                $upload_file = $_REQUEST['edit-file'];
                $Document_set_entry_dataArray['filename'] = $upload_file;
            }
            $DocId = $this->set_entry('Documents', $Document_set_entry_dataArray);
            $set_document_revision_parameters = array(
                'id' => $DocId,
                'document_name' => $upload_file,
                'filename' => $upload_file,
                'file' => base64_encode(file_get_contents($upload_path)),
                'revision' => '1',
                    //'file_mime_type' => $Document_set_entry_dataArray['file_mime_type'],//Added by BC on 29-jun-2015
            );
            //set attachment call
            $note_attachment = array(
                'session' => $this->session_id,
                'note' => $set_document_revision_parameters
            );
            $set_document_revision_result = $this->call('set_document_revision', $note_attachment, $this->url);
            //return $set_document_revision_result;
            return $DocId;
        }
    }

    function getDocument($document_id) {
        //debugbreak();
        if ($this->session_id) {
            $select_field_array = array('document_revision_id');
            $document_result = $this->get_entry('Documents', $document_id, $select_field_array);
            $revision_id = $document_result->entry_list[0]->name_value_list->document_revision_id->value;
            $revision_params = array(
                'session' => $this->session_id,
                'id' => $revision_id,
            );
            $select_revision_field = array('file_mime_type');
            $revision_result = $this->get_entry('DocumentRevisions', $revision_id, $select_revision_field);
            $file_mime_type = $revision_result->entry_list[0]->name_value_list->file_mime_type->value;
            $DocRevisionResult = $this->call('get_document_revision', $revision_params, $this->url);
            $filename = $DocRevisionResult->document_revision->filename;
            $content = base64_decode($DocRevisionResult->document_revision->file);
            header("Content-type: {$file_mime_type}");
            header('Content-Disposition: attachment; filename=' . basename($filename));
            header('Content-Description: File Transfer');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            echo $content;
            exit;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    //************start old functions************************
    // Get User Information
    public function getUserInformation($contact_id) {
        $get_entry_parameters = array(
            'session' => $this->session_id,
            'module_name' => 'Contacts',
            'id' => $contact_id,
        );
        $get_entry_result = $this->call("get_entry", $get_entry_parameters, $this->url);
        return $get_entry_result;
    }

    // Check user exists or not
    public function getUserExists($username) {
        $get_entry_list = array(
            'session' => $this->session_id,
            'module_name' => 'Contacts',
            'query' => "username_c = '{$username}'",
            'order_by' => '',
            'offset' => 0,
            'select_fields' => array('id', 'username_c'),
            'max_results' => 0,
        );
        $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);
        $isUser = $get_entry_list_result->entry_list[0]->name_value_list->username_c->value;
        if ($isUser == $username) {
            return true;
        } else {
            return false;
        }
    }

    // Check user information by username
    public function getUserInformationByUsername($username) {
        $get_entry_list = array(
            'session' => $this->session_id,
            'module_name' => 'Contacts',
            'query' => "username_c = '{$username}'",
            'order_by' => '',
            'offset' => 0,
            'select_fields' => array('id', 'username_c', 'password_c', 'email1'),
            'max_results' => 0,
        );
        $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);
        $isUser = $get_entry_list_result->entry_list[0]->name_value_list->username_c->value;
        if ($isUser == $username) {
            return $get_entry_list_result;
        } else {
            return false;
        }
    }

    // Get contact all email address
    public function getContactAllEmail() {
        $get_entry_list = array(
            'session' => $this->session_id,
            'module_name' => 'Contacts',
            'query' => "",
            'order_by' => '',
            'offset' => 0,
            'select_fields' => array('id', 'email1'),
            'max_results' => 0,
        );
        $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);
        $getAllEmailsData = $get_entry_list_result->entry_list;

        foreach ($getAllEmailsData as $getAllEmailsObj) {
            $getEmails[] = $getAllEmailsObj->name_value_list->email1->value;
        }
        return $getEmails;
    }

    //************end old functions**************************
    // Check user exists or not
    public function getPortalUserExists($username) {//Updated by BC on 11-aug-2015
        $get_entry_list = array(
            'session' => $this->session_id,
            'module_name' => 'Contacts',
            'query' => "contacts_cstm.username_c = '{$username}'",
            'order_by' => '',
            'offset' => 0,
            'select_fields' => array('id', 'username_c'),
            'max_results' => 0,
        );
        $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);
        $isUser = $get_entry_list_result->entry_list[0]->name_value_list->username_c->value;
        if ($isUser == $username) {
            return true;
        } else {
            return false;
        }
    }

    // Get Users all email address
    public function getPortalUsersAllEmail() {
        $get_entry_list = array(
            'session' => $this->session_id,
            'module_name' => 'Users',
            'query' => "",
            'order_by' => '',
            'offset' => 0,
            'select_fields' => array('id', 'email1'),
            'max_results' => 0,
        );
        $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);
        $getAllEmailsData = $get_entry_list_result->entry_list;

        foreach ($getAllEmailsData as $getAllEmailsObj) {
            $getEmails[] = $getAllEmailsObj->name_value_list->email1->value;
        }
        return $getEmails;
    }

    // Get user information by email address //Added by BC on 11-sep-2015
    public function getPortalEmailExists($email1, $portal_username = '') {//Updated by BC on 11-aug-2015
        if ($portal_username != '') {
            $query_pass = "(contacts_cstm.username_c != '{$portal_username}' OR contacts_cstm.username_c IS NULL )AND contacts.id in (
            SELECT eabr.bean_id
                FROM email_addr_bean_rel eabr JOIN email_addresses ea
                    ON (ea.id = eabr.email_address_id)
                WHERE eabr.deleted=0 AND ea.email_address ='{$email1}')";
        } else {
            $query_pass = "contacts.id in (
                SELECT eabr.bean_id
                FROM email_addr_bean_rel eabr JOIN email_addresses ea
                ON (ea.id = eabr.email_address_id)
                WHERE eabr.deleted=0 AND ea.email_address = '{$email1}')";
        }
        
        $get_entry_list = array(
            'session' => $this->session_id,
            'module_name' => 'Contacts',
            'query' => $query_pass,
            'order_by' => '',
            'offset' => 0,
            'select_fields' => array('id', 'email1'),
            //'max_results' => 0,
            //A list of link names and the fields to be returned for each link name.
            'link_name_to_fields_array' => array(
            ),
            //The maximum number of results to return.
            'max_results' => '',
            //If deleted records should be included in results.
            'deleted' => 0,
            //If only records marked as favorites should be returned.
            'favorites' => false,
        );

        $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);

        $isEmailExist = $get_entry_list_result->entry_list[0]->name_value_list->email1->value;
        if ($isEmailExist) {
            return true;
        } else {
            return false;
        }
    }

    // Check user information by username
    public function getPortalUserInformationByUsername($username) {
        $get_entry_list = array(
            'session' => $this->session_id,
            'module_name' => 'Contacts',
            'query' => "contacts_cstm.username_c = '{$username}'",
            'order_by' => '',
            'offset' => 0,
            'select_fields' => array('id', 'username_c', 'password_c', 'email1'),
            'max_results' => 0,
        );
        $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);
        $isUser = $get_entry_list_result->entry_list[0]->name_value_list->username_c->value;
        if ($isUser == $username) {
            return $get_entry_list_result;
        } else {
            return false;
        }
    }

    // Get User Information
    public function getPortalUserInformation($user_id) {
        $get_entry_parameters = array(
            'session' => $this->session_id,
            'module_name' => 'Contacts',
            'id' => $_SESSION['scp_user_id'],
        );
        $get_entry_result = $this->call("get_entry", $get_entry_parameters, $this->url);
        return $get_entry_result;
    }

    //Added by BC on 20-jul-2015 for getting fields
    public function get_module_layout($module_name, $view) {
        //Session id
        $params = array('session' => $this->session_id,
            'modules' => $module_name,
            'types' => array('default'),
            'views' => $view,
            'acl_check' => false,
            'md5' => false,
        );
        $layout = $this->call("get_module_layout", $params, $this->url);
        return $layout;
    }

    //Added by BC on 03-aug-2015 for custom function made in sugar to change layout
    public function get_customListlayout($module_name, $view) {
        //Session id
        $params = array(
            'session' => $this->session_id,
            'module_name' => $module_name,
            'view' => $view,
        );
        $layout = $this->call("get_customListlayout", $params, $this->url);
        return $layout;
    }

    //Added by BC on 03-aug-2015 for store scp_key at sugarside
    public function store_wpkey($key) {
        //Session id
        $params = array(
            'session' => $this->session_id,
            'key' => $key
        );
        $message = $this->call("store_wpkey", $params, $this->url);
        return $message;
    }

    //Added by BC on 11-aug-2015 for get user timezone
    public function getUserTimezone() {
        if ($this->session_id) {
            $parameters = array(
                'session' => $this->session_id,
                'module_name' => 'Users',
                'user_id' => $this->user_id,
            );
            if ($this->session_id) {
                $get_entry_result = $this->call("getUserTimezone", $parameters, $this->url);
            }
            return $get_entry_result;
        }
    }

    //Added by BC on 05-dec-2015 for uncode string like ^module1^,^module2^...
    function unencodeMultienum($string) {
        if (is_array($string)) {
            return $string;
        }
        if (substr($string, 0, 1) == "^" && substr($string, -1) == "^") {
            $string = substr(substr($string, 1), 0, strlen($string) - 2);
        }

        return explode('^,^', $string);
    }

    //Added by BC on 05-dec-2015 get Portal user's accessible Modules.
    function getPortal_accessibleModules() {
        if ($this->session_id) {

            $parameters = array(
                'session' => $this->session_id,
                'contact_id' => $_SESSION['scp_user_id']
            );

            if ($this->session_id) {
                $get_entry_result = $this->call("getPortal_accessibleModules", $parameters, $this->url);
            }
            return $get_entry_result;
        }
    }

    //Added by BC on 23-may-2016
    //For get entry quotes & invoice
    public function get_entry_quotes($module_name, $record_id) {
        if ($this->session_id) {
            $get_entry_parameters = array(
                'session' => $this->session_id,
                'module_name' => $module_name,
                'id' => $record_id,
            );
            $get_entry_result = $this->call("get_entry_quotes", $get_entry_parameters, $this->url);
            return $get_entry_result;
        }
    }
    //Added for default group lable
    public function get_all_modules() {

        $get_available_modules_parameters = array(
            //Session id
            "session" => $this->session_id,
            //Module filter. Possible values are 'default', 'mobile', 'all'.
            "filter" => 'all',
        );

        $set_entry_result = $this->call('get_available_modules', $get_available_modules_parameters, $this->url);

        return $set_entry_result;
    }

}
