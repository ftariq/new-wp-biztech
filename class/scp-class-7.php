<?php

//ini_set('display_errors', E_ALL);

Class SugarRestApiCall {

    var $base_url;
    var $username;
    var $password;
    var $access_token;
    var $user_id;

    //Added by BC on 16-nov-2015
    //var $user_datetime_format;

    function __construct($base_url, $username, $password) {
        $custom_api_url = "/rest/v10"; //Added on 07-dec-2015 append rest url
        $this->base_url = rtrim($base_url, '/') . $custom_api_url; //remove back slash if any in url
        $this->username = $username;
        $this->password = $password;
        $login_response = $this->login();
        if (!empty($login_response->access_token)) {
            $this->access_token = $login_response->access_token;
        }
        //Added by BC on 16-nov-2015
        // $this->user_datetime_format = $this->getUserTimezone();
    }

    /**
     * Generic function to make cURL request.
     * @param $url - The URL route to use.
     * @param string $oauthtoken - The oauth token.
     * @param string $type - GET, POST, PUT, DELETE. Defaults to GET.
     * @param array $arguments - Endpoint arguments.
     * @param array $encodeData - Whether or not to JSON encode the data.
     * @param array $returnHeaders - Whether or not to return the headers.
     * @return mixed
     */
    function call($url, $oauthtoken = '', $type = 'GET', $arguments = array(), $encodeData = true, $returnHeaders = false) {
        $type = strtoupper($type);

        if ($type == 'GET') {
            $url .= "?" . http_build_query($arguments);
        }

        $curl_request = curl_init($url);

        if ($type == 'POST') {
            curl_setopt($curl_request, CURLOPT_POST, 1);
        } elseif ($type == 'PUT') {
            curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, "PUT");
        } elseif ($type == 'DELETE') {
            curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, "DELETE");
        }

        curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl_request, CURLOPT_HEADER, $returnHeaders);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

        if (!empty($oauthtoken)) {
            $token = array("oauth-token: {$oauthtoken}");
            curl_setopt($curl_request, CURLOPT_HTTPHEADER, $token);
        }

        if (!empty($arguments) && $type !== 'GET') {
            if ($encodeData) {
                //encode the arguments as JSON
                $arguments = json_encode($arguments);
            }
            curl_setopt($curl_request, CURLOPT_POSTFIELDS, $arguments);
        }

        $result = curl_exec($curl_request);

        if ($returnHeaders) {
            //set headers from response
            list($headers, $content) = explode("\r\n\r\n", $result, 2);
            foreach (explode("\r\n", $headers) as $header) {
                header($header);
            }

            //return the nonheader data
            return trim($content);
        }

        curl_close($curl_request);

        //decode the response from JSON
        $response = json_decode($result);

        return $response;
    }

    function login() {
        //Login - POST /oauth2/token

        $url = $this->base_url . "/oauth2/token";

        $oauth2_token_arguments = array(
            "grant_type" => "password",
            //client id/secret you created in Admin > OAuth Keys
            "client_id" => "sugar",
            "client_secret" => "",
            "username" => $this->username,
            "password" => $this->password,
            "platform" => "base"
        );

        $oauth2_token_response = $this->call($url, '', 'POST', $oauth2_token_arguments);
        return $oauth2_token_response;
    }

    function get_customLayout($module_name, $view) {
        if ($this->access_token != '') {
            $url = $this->base_url . "/get_customLayout";

            $layout_arguments = array(
                "module_name" => $module_name,
                "view" => $view
            );

            $layout_response = $this->call($url, $this->access_token, 'GET', $layout_arguments);
            return $layout_response;
        }
    }

    function store_wpkey($wp_key) {
        if ($this->access_token != '') {
            $wpkey_url = $this->base_url . "/store_wpkey";
            $store_wpkey_arguments = array(
                "key" => $wp_key,
            );

            $wpkey_response = $this->call($wpkey_url, $this->access_token, 'GET', $store_wpkey_arguments);
            return $wpkey_response;
        }
    }

    function getUser_accessibleModules() {
        if ($this->access_token != '') {
            $filter_arguments = array(
                "filter" => array(
                    array(
                        "user_name" => $this->username
                    )
                ),
                "max_num" => 1,
                "offset" => 0,
                "fields" => "id,first_name,last_name",
            );

            $url = $this->base_url . "/Users/filter";

            $filter_response = $this->call($url, $this->access_token, 'POST', $filter_arguments);
            $this->user_id = $filter_response->records[0]->id;
            $url = $this->base_url . "/getUser_accessibleModules";
            $parameters = array(
                'module_name' => 'Users',
                'user_id' => $this->user_id,
            );
            $module_result = $this->call($url, $this->access_token, 'GET', $parameters);
            return $module_result;
        }
    }

    //get UserTimzone
    function getUserTimezone() {
        if ($this->access_token != '') {
            $filter_arguments = array(
                "filter" => array(
                    array(
                        "user_name" => $this->username
                    )
                ),
                "max_num" => 1,
                "offset" => 0,
                "fields" => "id,first_name,last_name",
            );

            $url = $this->base_url . "/Users/filter";

            $filter_response = $this->call($url, $this->access_token, 'POST', $filter_arguments);
            $this->user_id = $filter_response->records[0]->id;

            $url = $this->base_url . "/getUserTimezone";
            $parameters = array(
                'module_name' => 'Users',
                'user_id' => $this->user_id,
            );
            $Timezone_result = $this->call($url, $this->access_token, 'GET', $parameters);
            return $Timezone_result;
        }
    }

    //get relationship between two modules
    function getRelationship($module_name, $module_id, $relationship_name, $fields = '', $where_condition = array(), $limit = '', $offset = '', $order_by = '') {
        if ($this->access_token != '') {
            $filter_arguments = array(
                "filter" => array(
                    $where_condition,
                ),
                "offset" => $offset,
                "order_by" => $order_by,
                "max_num" => $limit,
                "fields" => $fields
            );
            $url = "$this->base_url/{$module_name}/{$module_id}/link/{$relationship_name}";
            $link_response = $this->call($url, $this->access_token, 'GET', $filter_arguments);

            return $link_response;
        }
    }

    // Check Portal user information by username
    function getPortalUserExists($username) {
        if ($this->access_token != '') {
            $filter_arguments = array(
                "filter" => array(
                    array(
                        "username_c" => array(
                            '$equals' => "{$username}",
                        ),
                    ),
                ),
                "offset" => 0,
                "fields" => "id,username_c,password_c,email1",
            );
            $url = $this->base_url . "/Contacts/filter";
            $response = $this->call($url, $this->access_token, 'GET', $filter_arguments);
        }
        $isUser = $response->records[0]->username_C;
        if ($isUser == $username) {
            return $response;
        } else {
            return false;
        }
    }

    //Set entry
    function set_entry($module_name, $set_entry_dataArray) {
        $url = $this->base_url;
        if (isset($set_entry_dataArray['id']) && $set_entry_dataArray['id'] != '') {//Added by BC on 28-oct-2015 $set_entry_dataArray['id'] != ''
            $isUpdate = true;
        } else {
            $isUpdate = false;
        }
        if ($this->access_token != '') {
            if ($isUpdate == true) {
                $url = $url . "/{$module_name}/{$set_entry_dataArray['id']}";
                unset($set_entry_dataArray['id']);
                $response = $this->call($url, $this->access_token, 'PUT', $set_entry_dataArray);
                $return_id = (isset($response->id)) ? $response->id : NULL;
                return $return_id;
            } else {
                $url = $url . "/{$module_name}";
                $response = $this->call($url, $this->access_token, 'POST', $set_entry_dataArray);
                $return_id = (isset($response->id)) ? $response->id : NULL;
                return $return_id;
            }
        }
    }

    //Set Relationship
    function set_relationship($module, $module_id, $relate_module, $relate_id) {
        $url = $this->base_url;
        if ($this->access_token != '') {
            $nameValueList = array(
                'link_name' => $relate_module,
                'ids' => array($relate_id), // second module id.
                'sugar_id' => $module_id // first module id
            );
            $url = $url . "/{$module}/{$nameValueList['sugar_id']}/link";
            unset($nameValueList['sugar_id']);
            $response = $this->call($url, $this->access_token, 'POST', $nameValueList);
            return $response;
        }
    }

    //Crate Note with attachment and other details
    function createNote($note_name, $file, $filepath) {//Updated by BC on 10-nov-2015
        if (isset($note_name['id']) && $note_name['id'] != '') {
            $isUpdate = true;
        } else {
            $isUpdate = false;
        }
        $url = $this->base_url;
        if ($this->access_token != '') {
            if ($isUpdate == true) {
                $url_note = $url . "/Notes/{$note_name['id']}";
                //unset($note_name['id']);
                if (empty($file)) {//Added by BC on 21-jun-2016
                    $file = $_REQUEST['edit-file'];
                    $note_name['filename'] = $file;
                }
                $note_response = $this->call($url_note, $this->access_token, 'PUT', $note_name);
            } else {
                $url_note = $url . "/Notes";
                $note_response = $this->call($url_note, $this->access_token, 'POST', $note_name);
            }
            //Upload File - POST /<module>/:id/file/:field
            if (isset($file) && !empty($file)) {//Added by BC on 21-jun-2016
                $url = $url . "/Notes/{$note_response->id}/file/filename";
                $file_abs_path = dirname(__FILE__) . '/upload/' . $file;
                move_uploaded_file($filepath, $file_abs_path);

                $target_file = basename($file);
                $file_arguments = array(
                    "format" => "sugar-html-json",
                    "delete_if_fails" => true,
                    "oauth_token" => $this->access_token,
                    'filename' => "@{$file_abs_path}",
                );

                $file_response = $this->call($url, $this->access_token, 'POST', $file_arguments, false);
                unlink("$file_abs_path");
            } else {
                $file_response = $note_response->id;
            }
        }

        return $file_response;
    }

    //Create Document with attachment and other details
    function createDocument($doc_name, $file, $filepath) {
        if (isset($doc_name['id']) && $doc_name['id'] != '') {
            $isUpdate = true;
        } else {
            $isUpdate = false;
        }
        $url = $this->base_url;
        if ($this->access_token != '') {

            if ($isUpdate == true) {
                $url_note = $url . "/Documents/{$doc_name['id']}";
                if (empty($file)) {
                    $file = $_REQUEST['edit-file'];
                    $doc_name['filename'] = $file;
                }
                $doc_response = $this->call($url_note, $this->access_token, 'PUT', $doc_name);
            } else {
                $url_note = $url . "/Documents";
                $doc_response = $this->call($url_note, $this->access_token, 'POST', $doc_name);
            }
            ///////////////////////////
            //Upload File - POST /<module>/:id/file/:field
            if (isset($file) && !empty($file)) {//Added by BC on 21-jun-2016
                $url = $url . "/Documents/{$doc_response->id}/file/filename";
                $file_abs_path = dirname(__FILE__) . '/upload/' . $file;
                move_uploaded_file($filepath, $file_abs_path);

                $target_file = basename($file);
                $file_arguments = array(
                    "format" => "sugar-html-json",
                    "delete_if_fails" => true,
                    "oauth_token" => $this->access_token,
                    'filename' => "@{$file_abs_path}",
                );

                $file_response = $this->call($url, $this->access_token, 'POST', $file_arguments, false);
                unlink("$file_abs_path");
            } else {
                $file_response = $doc_response->id;
            }
        }

        return $file_response;
    }

    //get Note Attachment
    function getNoteAttachment($note_id) {//Updated by BC on 16-nov-2015
        if ($this->access_token != '') {
            $url = $this->base_url;

            $url = $this->base_url . "/Notes/" . $note_id;
            $note_response = $this->call($url, $this->access_token, 'GET');
            $url = $this->base_url . "/Notes/{$note_id}/file/filename";
            $response = $this->call($url, $this->access_token, 'GET', array(), true, true);
            echo $response;
            exit;
        }
    }

    //get Document Attachment
    function getDocumentAttachment($document_id) {
        if ($this->access_token != '') {
            $url = $this->base_url;

            $url = $this->base_url . "/Documents/" . $document_id;
            $note_response = $this->call($url, $this->access_token, 'GET');
            $url = $this->base_url . "/Documents/{$document_id}/file/filename";
            $response = $this->call($url, $this->access_token, 'GET', array(), true, true);
            echo $response;
            exit;
        }
    }

    //get Record Detail by record id
    function getRecordDetail($module, $record_id) {
        if ($this->access_token != '') {
            $url = $this->base_url . "/{$module}/{$record_id}";
            $response = $this->call($url, $this->access_token, 'GET');
            return $response;
        }
    }

    //get Module Record Detail by passing query
    function getModuleRecords($module_name, $fields = "", $limit = "", $offset = "") {
        if ($this->access_token != '') {
            $filter_arguments = array(
                "filter" => array(
                ),
                "offset" => $offset,
                "max_num" => $limit,
                "fields" => $fields,
            );
            $module = $module_name;
            $url = $this->base_url;
            $url = $url . "/{$module}/filter";
            $response = $this->call($url, $this->access_token, 'POST', $filter_arguments);
            return $response;
        }
    }

    // Portal Login by username and password
    function PortalLogin($username, $password, $login_by_email = 0) {
        if ($this->access_token != '') {

            $url = $this->base_url . '/isActive';
            $response = $this->call($url, $this->access_token, 'GET');
            if ($response->success) {
                if ($login_by_email == 1) {//Added by BC on 03-aug-2016
                    $filter_arguments = array(
                        "filter" => array(
                            array(
                                '$and' => array(
                                    array(
                                        "username_c" => array(
                                            '$equals' => "{$username}",
                                        )
                                    ),
                                    array(
                                        "email1" => array(
                                            '$equals' => "{$password}",
                                        )
                                    )
                                )
                            )
                        ),
                        "offset" => 0,
                        "fields" => "id,username_c,password_c,salutation,first_name,last_name,email1,account_id,title,phone_work,phone_mobile','phone_fax",
                    );
                } else {
                    $filter_arguments = array(
                        "filter" => array(
                            array(
                                '$and' => array(
                                    array(
                                        "username_c" => array(
                                            '$equals' => "{$username}",
                                        )
                                    ),
                                    array(
                                        "password_c" => array(
                                            '$equals' => "{$password}",
                                        ),
                                    )
                                ),
                            ),
                        ),
                        "offset" => 0,
                        "fields" => "id,username_c,password_c,salutation,first_name,last_name,email1,account_id,title,phone_work,phone_mobile','phone_fax",
                    );
                }
                $module = "Contacts";
                $url = $this->base_url;
                $url = $url . "/{$module}/filter";
                $response = $this->call($url, $this->access_token, 'POST', $filter_arguments);
                return $response;
            } else {
                return $response;
            }
        }
    }

    //Check user information by username
    function getUserInformationByUsername($username) {
        if ($this->access_token != '') {
            $filter_arguments = array(
                "filter" => array(
                    array(
                        "username_c" => array(
                            '$equals' => "{$username}",
                        ),
                    ),
                ),
                "offset" => 0,
                "fields" => "id,username_c,password_c,email1",
            );
            $url = $this->base_url . "/Contacts/filter";
            $response = $this->call($url, $this->access_token, 'GET', $filter_arguments);
        }
        $isUser = $response->records[0]->username_c;
        if ($isUser == $username) {
            return $response;
        } else {
            return false;
        }
    }

    //Check email id exists
    function getPortalEmailExists($email1, $portal_username = '') {
        if ($this->access_token != '') {
            if ($portal_username != '') {
                $filter_arguments = array(
                    "filter" => array(
                        array(
                            '$and' => array(
                                array(
                                    "username_c" => array(
                                        '$not_equals' => "{$portal_username}",
                                    )
                                ),
                                array(
                                    "email1" => array(
                                        '$equals' => "{$email1}",
                                    )
                                )
                            )
                        )
                    ),
                    "offset" => 0,
                    "fields" => "id,username_c,password_c,email1",
                );
            } else {
                $filter_arguments = array(
                    "filter" => array(
                        array(
                            "email1" => array(
                                '$equals' => "{$email1}",
                            ),
                        ),
                    ),
                    "offset" => 0,
                    "fields" => "id,username_c,password_c,email1",
                );
            }
            $url = $this->base_url . "/Contacts/filter";
            $response = $this->call($url, $this->access_token, 'GET', $filter_arguments);
        }
        $isEmail = $response->records[0]->email1;
        if ($isEmail == $email1) {
            //return $response;
            return true;
        } else {
            return false;
        }
    }

    //get User Information by user id
    function getPortalUserInformation($contact_id) {
        if ($this->access_token != '') {
            $url = $this->base_url . "/Contacts/" . $contact_id;
            $user_response = $this->call($url, $this->access_token, 'GET');
            return $user_response;
        }
    }

    //Added by BC on 05-dec-2015 for uncode string like ^module1^,^module2^...
    function unencodeMultienum($string) {
        if (is_array($string)) {
            return $string;
        }
        if (substr($string, 0, 1) == "^" && substr($string, -1) == "^") {
            $string = substr(substr($string, 1), 0, strlen($string) - 2);
        }

        return explode('^,^', $string);
    }

    //Added by BC on 08-dec-2015 for get portal user group module access
    function getPortal_accessibleModules() {
        if ($this->access_token != '') {
            $params = array(
                "contact_id" => $_SESSION['scp_user_id']
            );

            $url = $this->base_url . "/getPortal_accessibleModules";

            $module_result = $this->call($url, $this->access_token, 'GET', $params);
            return $module_result;
        }
    }

    //Added by BC on 22-jun-2016 get KB Document Attachment
    function getKBDocumentAttachment($doc_rev_id) {
        if ($this->access_token != '') {
            $url = $this->base_url . "/DocumentRevisions/" . $doc_rev_id;
            $note_response = $this->call($url, $this->access_token, 'GET');
            $url = $this->base_url . "/DocumentRevisions/{$doc_rev_id}/file/filename";
            $response = $this->call($url, $this->access_token, 'GET', array(), true, true);
            echo $response;
            exit;
        }
    }

    //Added by BC on 23-jun-2016 for get KB Document entry list with description with custom call
    function get_entry_list_custom($module_name, $fields = '', $where_condition = array(), $limit = '', $offset = '', $order_by = '') {
        //get_entry_list call starts
        $url = $this->base_url;
        if ($this->access_token != '') {
            $filter_arguments = array(
                "filter" => array(
                    $where_condition
                ),
                "offset" => $offset,
                "order_by" => $order_by,
                "max_num" => $limit,
                "fields" => $fields
            );
            if (isset($_SESSION['sugar_version']) && $_SESSION['sugar_version'] == '7.7.0.0') {//if sugar 7 version 7.7
                $url = $url . "/" . $module_name;
                $record_response = $this->call($url, $this->access_token, 'GET', $filter_arguments);
            } else {//if version 7.6 or lesser than that
                $url = $url . "/" . $module_name . "/filter";
                $record_response = $this->call($url, $this->access_token, 'GET', $filter_arguments);
                //get_entry_list call ends
                foreach ($record_response->records as $doc_key => $record) {
                    if (array_key_exists('description', $record_response->records[$doc_key])) {
                        // get Relationship with revisions
                        $relationship_name = 'revisions';
                        $filter_arguments_rel = array(
                            "filter" => array(
                            ),
                            "fields" => ''
                        );
                        $url = "$this->base_url/{$module_name}/{$record->id}/link/{$relationship_name}";
                        $link_response = $this->call($url, $this->access_token, 'GET', $filter_arguments_rel);
                        foreach ($link_response->records as $rev_key => $rev) {
                            //get entry of KBContent
                            $filter_arguments = array(
                            );
                            $url = $this->base_url . "/KBContents/" . $rev->kbcontent_id;
                            $content_response = $this->call($url, $this->access_token, 'GET', $filter_arguments);
                            if (isset($content_response->kbdocument_body)) {
                                $record_response->records[$doc_key]->description = $content_response->kbdocument_body;
                            }
                        }
                    }
                }
            }
            return $record_response;
        }
    }

    //Added by BC on 06-jul-2016 For get entry quotes & invoice
    public function get_entry_quotes($module_name, $record_id) {
        if ($this->access_token != '') {
            $url = $this->base_url . "/get_entry_quotes";
            $parameters = array(
                'module_name' => $module_name,
                'id' => $record_id,
            );
            $get_entry_result = $this->call($url, $this->access_token, 'POST', $parameters);
            return $get_entry_result;
        }
    }

    //Added by BC on 16-jul-2016 for getting options for perticulat module
    function getEnumValues($module_name, $field_name = '') {
        if ($this->access_token != '') {
            $url = "$this->base_url/{$module_name}/enum/{$field_name}";
            $link_response = $this->call($url, $this->access_token, 'GET');

            return $link_response;
        }
    }

    //Added for default group lable 
    function get_all_modules() {

        $url = $this->base_url;
        //$url = $url . "/metadata";
        $url = $url . "/lang/en_us";
        $response = $this->call($url, $this->access_token, 'GET');

//        if ( isset( $response->full_module_list) ) {
//            $response = $response->full_module_list; 
//        } 
        if (is_object($response->app_list_strings->moduleList)) {
            $response = $response->app_list_strings->moduleList;
        }
        return $response;
    }

}
