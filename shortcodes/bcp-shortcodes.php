<?php

//For Sign Up shortcode
add_shortcode('bcp-sign-up', 'bcp_sign_up');

function bcp_sign_up() {
    //20-aug-2016
    global $objSCP, $sugar_crm_version;
    if($objSCP == NULL){
        return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'> 
            <strong>Please Contact Your Administrator.</strong>
        </div>";
    }
    if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
        $sess_id = $objSCP->session_id;
    }
    if ($sugar_crm_version == 7) {
        $sess_id = $objSCP->access_token;
    }
    if ($sess_id != '') {
        ob_start();
        if (empty($_SESSION['scp_user_id'])) {
            include( TEMPLATE_PATH . 'signup.php');
            return ob_get_clean();
        } else {
            if (isset($_COOKIE['bcp_connection_error']) && $_COOKIE['bcp_connection_error'] != '') {
                $cookie_err = $_COOKIE['bcp_connection_error'];
                unset($_COOKIE['bcp_login_error']);
                return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'> 
            <strong>$cookie_err</strong>
        </div>";
            }
        }
    } else {
        if (isset($_COOKIE['bcp_connection_error']) && $_COOKIE['bcp_connection_error'] != '') {
            $cookie_err = $_COOKIE['bcp_connection_error'];
            unset($_COOKIE['bcp_login_error']);
            return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'> 
            <strong>$cookie_err</strong>
        </div>";
        }
    }
}

//For Login Form shortcode
add_shortcode('bcp-login', 'bcp_login_shortcode');

function bcp_login_shortcode() {
    //20-aug-2016
    global $objSCP, $sugar_crm_version;
    if($objSCP == NULL){
        return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'> 
            <strong>Please Contact Your Administrator.</strong>
        </div>";
    }
    if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
        $sess_id = $objSCP->session_id;
    }
    if ($sugar_crm_version == 7) {
        $sess_id = $objSCP->access_token;
    }
    if ($sess_id != '') {
        ob_start();
        if (get_option('biztech_scp_single_signin') != '' && is_user_logged_in()) {//Added by BC on 02-aug-2016
            ob_clean();
            do_action('admin_post_bcp_login');
        }
        if (empty($_SESSION['scp_user_id'])) {
            include( TEMPLATE_PATH . 'login.php');
            return ob_get_clean();
        } else {
            if (isset($_COOKIE['bcp_connection_error']) && $_COOKIE['bcp_connection_error'] != '') {
                $cookie_err = $_COOKIE['bcp_connection_error'];
                unset($_COOKIE['bcp_login_error']);
                return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'> 
            <strong>$cookie_err</strong>
        </div>";
            }
        }
    } else {
        if (isset($_COOKIE['bcp_connection_error']) && $_COOKIE['bcp_connection_error'] != '') {
            $cookie_err = $_COOKIE['bcp_connection_error'];
            unset($_COOKIE['bcp_login_error']);
            return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'> 
            <strong>$cookie_err</strong>
        </div>";
        }
    }
}

//For Profile Form shortcode
add_shortcode('bcp-profile', 'bcp_profile_shortcode');

function bcp_profile_shortcode() {

    global $objSCP, $sugar_crm_version, $getContactInfo;
    //20-aug-2016
    if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
        $sess_id = $objSCP->session_id;
    }
    if ($sugar_crm_version == 7) {
        $sess_id = $objSCP->access_token;
    }
    if ($sess_id != '') {
        if (isset($_SESSION['scp_user_id']) == true) { // check for login or not
            if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
                $getContactInfo = $objSCP->getPortalUserInformation($_SESSION['scp_user_id'])->entry_list[0]->name_value_list;
            } else {
                $getContactInfo = $objSCP->getPortalUserInformation($_SESSION['scp_user_id']);
            }
            ob_start();
            include( TEMPLATE_PATH . 'profile.php');
            return ob_get_clean();
        }
    } else {
        if (isset($_COOKIE['bcp_connection_error']) && $_COOKIE['bcp_connection_error'] != '') {
            $cookie_err = $_COOKIE['bcp_connection_error'];
            unset($_COOKIE['bcp_login_error']);
            return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'> 
            <strong>$cookie_err</strong>
        </div>";
        }
    }
}

//For Forgot Password shortcode
add_shortcode('bcp-forgot-password', 'bcp_forgot_password');

function bcp_forgot_password() {

    //20-aug-2016
    global $objSCP, $sugar_crm_version;
    if($objSCP == NULL){
        return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'> 
            <strong>Please Contact Your Administrator.</strong>
        </div>";
    }
    if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
        $sess_id = $objSCP->session_id;
    }
    if ($sugar_crm_version == 7) {
        $sess_id = $objSCP->access_token;
    }
    if ($sess_id != '') {
        ob_start();
        if (empty($_SESSION['scp_user_id'])) {
            include( TEMPLATE_PATH . 'forgotpassword.php');
            return ob_get_clean();
        } else {
            if (isset($_COOKIE['bcp_connection_error']) && $_COOKIE['bcp_connection_error'] != '') {
                $cookie_err = $_COOKIE['bcp_connection_error'];
                unset($_COOKIE['bcp_login_error']);
                return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'> 
            <strong>$cookie_err</strong>
        </div>";
            }
        }
    } else {
        if (isset($_COOKIE['bcp_connection_error']) && $_COOKIE['bcp_connection_error'] != '') {
            $cookie_err = $_COOKIE['bcp_connection_error'];
            unset($_COOKIE['bcp_login_error']);
            return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'> 
            <strong>$cookie_err</strong>
        </div>";
        }
    }
}

//For display accesible modules
add_shortcode('bcp-manage-page', 'bcp_manage_page_shortcode');

function bcp_manage_page_shortcode() {

    //20-aug-2016
    global $objSCP, $sugar_crm_version;
    if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
        $sess_id = $objSCP->session_id;
    }
    if ($sugar_crm_version == 7) {
        $sess_id = $objSCP->access_token;
    }
    if ($sess_id != '') {
        if (empty($_SESSION['module_array'])) {//check session value in module array
            $modules_array2 = $objSCP->getPortal_accessibleModules();
            $modules = $modules_array2->accessible_modules;
        } else {
            $modules = $_SESSION['module_array'];
        }
        include( TEMPLATE_PATH . 'bcp_manage_page.php');
        return ob_get_clean();
    } else {
        if (isset($_COOKIE['bcp_connection_error']) && $_COOKIE['bcp_connection_error'] != '') {
            $cookie_err = $_COOKIE['bcp_connection_error'];
            unset($_COOKIE['bcp_login_error']);
            return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'> 
            <strong>$cookie_err</strong>
        </div>";
        }
    }
}

//For dashboard gui page
add_shortcode('bcp-dashboard-gui', 'bcp_dashboard_gui_shortcode');

function bcp_dashboard_gui_shortcode() {
    //20-aug-2016
    global $objSCP, $sugar_crm_version;
    if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
        $sess_id = $objSCP->session_id;
    }
    if ($sugar_crm_version == 7) {
        $sess_id = $objSCP->access_token;
    }
    if ($sess_id != '') {
        if (empty($_SESSION['module_array'])) {//check session value in module array
            $modules_array2 = $objSCP->getPortal_accessibleModules();
            $modules = $modules_array2->accessible_modules;
        } else {
            $modules = $_SESSION['module_array'];
        }
        include( TEMPLATE_PATH . 'bcp_dashboard_gui.php');
        return ob_get_clean();
    } else {
        if (isset($_COOKIE['bcp_connection_error']) && $_COOKIE['bcp_connection_error'] != '') {
            $cookie_err = $_COOKIE['bcp_connection_error'];
            unset($_COOKIE['bcp_login_error']);
            return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'> 
            <strong>$cookie_err</strong>
        </div>";
        }
    }
}
